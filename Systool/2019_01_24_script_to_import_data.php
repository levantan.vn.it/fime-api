<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/24/2019
 * Time: 1:38 PM
 */

$servername = "192.168.181.40";
$username = "netpower";
$password = "webPower2010";
$dbname = "Custom_Fime_Import";

$path = '/var/www/html/data_dmnc/';


$files = glob($path . '*.csv', GLOB_BRACE);

$table_names = [];


$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

foreach ($files as $filepath) {
    $fileName = basename($filepath);

    $table_name = substr($fileName, 0, strripos($fileName, '_'));


    $create_table_query = 'CREATE TABLE IF NOT EXISTS ' . $table_name . ' ( `primary_key` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY';

    $file = fopen($filepath, "r");
    $columns = fgetcsv($file);
    fclose($file);

    foreach ($columns as $column) {
        $create_table_query .= ', `' . str_replace('"', "", $column) . '` TEXT NULL';
    }


    $create_table_query .= ' )';

    $conn->query($create_table_query);


    $header = null;
    $insertData = [];
    if (($handle = fopen($filepath, 'r')) !== false) {
        while (($row = fgetcsv($handle, 100000, ',')) !== false) {
            if (!$header)
                $header = $row;
            else {
                $insert = '("';

                $index = 0;
                foreach ($row as $r) {
                    if ($index === count($row) - 1) {
                        $insert .= $r . '")';
                        break;
                    }
                    $insert .= $r . '", "';
                    $index++;
                }

                $insertData[] = $insert;
            }
        }
        fclose($handle);
    }
    $sql_insert = "INSERT INTO " . $table_name . " (" . str_replace('"', "", implode(',', $header)) . ") VALUES " . implode(',', $insertData);
    $conn->query($sql_insert);
}

$conn->close();


