<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/12/2019
 * Time: 10:04 AM
 */

namespace App\Actions\Users;


use App\Actions\Action;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\TryFree\CountLastTriesByUserIdCriteria;

class GetUserProfileAction extends Action
{
    protected $userRepository;
    protected $tryRepository;
    protected $reviewRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                TryRepositoryInterface $tryRepository,
                                ReviewRepositoryInterface $reviewRepository)
    {
        $this->userRepository = $userRepository;
        $this->tryRepository = $tryRepository;
        $this->reviewRepository = $reviewRepository;
    }

    public function run()
    {
        $user_id = auth()->id();

        if($user_id) {
            $user = $this->userRepository->find($user_id, ['reg_name', 'pic', 'user_no', 'slug', 'home_addr1', 'home_tel',
                'try_click_dt', 'review_click_dt', 'cellphone', 'email', 'id', 'role_id', 'allow_review', 'allow_comment',
                'DRMNCY_AT as active','level_number']);

            $user->isOwner = $user->hasRole('admin');

            $new_tries = $this->tryRepository->getByCriteria(new CountLastTriesByUserIdCriteria($user->try_click_dt));

            if(count($new_tries) > 0) {
                $new_tries = $new_tries[0]->total_tries;
            } else {
                $new_tries = 0;
            }

            $new_reviews = $this->reviewRepository->count([
                ['writng_dt', '>', $user->review_click_dt ? $user->review_click_dt : '0000-00-00 00:00:00'],
                'delete_at' => 'N',
                'expsr_at' => 'Y',
            ]);

            $number_of_reviews = $this->reviewRepository->getByCriteria(
                new GetNumberOfReviewsByUserIdsCriteria([$user_id]))->keyBy('user_id');

            $user->new_tries = $new_tries;
            if (isset($number_of_reviews[$user_id])) {
                $user->reviews = $number_of_reviews[$user_id]->number_of_reviews;
            } else {
                $user->reviews = 0;
            }
            $user->new_reviews = $new_reviews;

            return $user;
        }
        return null;
    }
}
