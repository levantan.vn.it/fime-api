<?php

namespace App\Actions\Users;
use App\Actions\Action;

use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\Review\GetReviewsByUserId;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Criterias\Users\GetHotFimerCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;

class GetHotFimersAction extends Action {
    protected $userRepository;
    protected $reviewRepository;
    protected $followRepository;
    protected $reviewFilesRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                UserFollowRepositoryInterface $followRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                ReviewFilesRepositoryInterface $reviewFilesRepository)

    {
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->followRepository = $followRepository;
        $this->reviewFilesRepository = $reviewFilesRepository;
    }

    public function run() {
        $fimers = $this->userRepository->pushCriteria(new GetHotFimerCriteria())->paginate(6);

        $this->getReviewsOfFimer($fimers);
        $this->getCurrentUserFollowing($fimers);
       return $fimers;
    }

    private function getReviewsOfFimer($fimers)
    {
        $fimerIds = $fimers->pluck('user_no')->toArray();

        // $number_of_reviews = $this->reviewRepository->getByCriteria(new GetNumberOfReviewsByUserIdsCriteria($fimerIds))->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->reviews = $this->reviewRepository->getByCriteria(new GetReviewsByUserId($fimer->user_no, 3));
            // $review_ids = $fimer->reviews->pluck('review_no')->toArray();
            // $files = $this->reviewFilesRepository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');
            // foreach ($fimer->reviews as $review) {
            //     if (isset($files[$review->review_no])) {
            //         $review->files = $files[$review->review_no];
            //     } else {
            //         $review->files = [];
            //     }
            // }
            // $fimer->number_of_reviews = 0;
            // if(isset($number_of_reviews[$fimer->user_no])) {
            //     $fimer->number_of_reviews = $number_of_reviews[$fimer->user_no]->number_of_reviews;
            // }
        }
    }

    protected function getCurrentUserFollowing($fimers) {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->followRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        $followers =$this->followRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings =$this->followRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if(isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if(isset($followings[$fimer->user_no])) {
                $fimer->number_of_followings =  $followings[$fimer->user_no]->number_of_followings;
            }

            if(isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }

        return $fimers;
    }
}
