<?php

namespace App\Actions\SystemNotification;
use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;
use Mockery\Exception;

class DeleteSystemNotificationAction extends Action {
    protected $system_notification_repository;

    public function __construct(SystemNotificationRepositoryInterface $system_notification_repository) {
        $this->system_notification_repository = $system_notification_repository;
    }

    public function run($ids) {
        try {
            foreach ($ids as $id) {
                $this->system_notification_repository->delete($id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
