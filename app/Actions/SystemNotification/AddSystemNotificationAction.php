<?php

namespace App\Actions\SystemNotification;
use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;
use Carbon\Carbon;
use Mockery\Exception;

class AddSystemNotificationAction extends Action {
    protected $system_notification_repository;

    /**
     * AddSystemNotificationAction constructor.
     * @param SystemNotificationRepositoryInterface $system_notification_repository
     */
    public function __construct(SystemNotificationRepositoryInterface $system_notification_repository) {
        $this->system_notification_repository = $system_notification_repository;
    }

    function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid =
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
            return $uuid;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function run($data) {
        try {
            $guid = $this->getGUID();
            $data['guid'] = $guid;
            $data['posted_at'] = Carbon::now();
            $data = $this->system_notification_repository->create($data);
            return $data;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
