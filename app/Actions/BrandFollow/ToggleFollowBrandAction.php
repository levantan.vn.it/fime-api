<?php

namespace App\Actions\BrandFollow;

use App\Actions\Notification\CreateNotificationAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\BrandFollowRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use Carbon\Carbon;
use App\Models\BrandFollow;
use Mockery\Exception;

class ToggleFollowBrandAction extends CreateNotificationAction
{
    protected $brand_follow_repository;
    protected $user_repository;
    protected $notification_repository;

    public function __construct(BrandFollowRepositoryInterface $brand_follow_repository,
                                UserRepositoryInterface $user_repository,
                                NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository
)
    {
        $this->brand_follow_repository = $brand_follow_repository;
        $this->user_repository = $user_repository;
        $this->notification_repository = $notification_repository;
        parent::__construct($notification_repository, $review_repository, $try_repository, $user_point_repository, $codeRepository);
    }

    /**
     * @param $request
     * @return array|bool
     */
    public function run($request)
    {
        try {
            $data = [];
            $data['fllwr_br'] = $request['followed_brand_id'];
            $data['user_no'] = auth()->id();

            $follows = $this->brand_follow_repository->findWhere([
                'fllwr_br' => $data['fllwr_br'],
                'user_no' => $data['user_no'],
            ]);

            if (count($follows) > 0) {
                $this->brand_follow_repository->deleteWhere([
                    'fllwr_br' => $data['fllwr_br'],
                    'user_no' => $data['user_no'],
                ]);

                $followed = false;
               
            } else {
                $this->brand_follow_repository->create([
                    'fllwr_br' => $data['fllwr_br'],
                    'regist_dt' => Carbon::now(),
                    'user_no' => $data['user_no'],
                ]);

                $followed = true;
                
            }


            // $follows = $this->brand_follow_repository->getByCriteria(new GetNumberOfFollowersByUserIdsCriteria([$data['fllwr_user_no']]));

            $total = BrandFollow::where('fllwr_br','=',$data['fllwr_br'])->count();

            return ['followed' => $followed,'total'=>$total];

        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
