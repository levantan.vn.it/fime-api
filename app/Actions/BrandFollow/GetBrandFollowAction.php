<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\BrandFollow;
use App\Actions\Action;
use App\Contracts\BrandFollowRepositoryInterface;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Models\BrandFollow;

class GetBrandFollowAction extends Action {
    protected $followRepository;

    public function __construct(
                                BrandFollowRepositoryInterface $followRepository) {
        $this->followRepository = $followRepository;
    }

    public function run() {

        $fimers = BrandFollow::select('TCT_BRFL.*')->get();

        return $fimers;
        // $current_user_id = auth()->id();

        // $user_ids = $fimers->pluck('user_no')->toArray();

        // $user_followings = $this->followRepository->getByCriteria(
        //     new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        // foreach ($fimers as $fimer) {
        //     $fimer->followed = 0;

        //     if (isset($user_followings[$fimer->user_no])) {
        //         $fimer->followed = $user_followings[$fimer->user_no]->user_followings;
        //     }
        // }
        // return $fimers;
    }
}
