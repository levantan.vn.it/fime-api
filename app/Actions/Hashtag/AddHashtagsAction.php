<?php

namespace App\Actions\Hashtag;
use App\Contracts\HashtagRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class AddHashtagsAction extends Action {
    protected $hashTagRepository;

    public function __construct(HashtagRepositoryInterface $hashTagRepository) {
        $this->hashTagRepository = $hashTagRepository;
    }

    public function run($data) {
        try {
            $hashtags = $this->hashTagRepository->create([
                'hash_tag' => '#'.$data['hash_tag'],
            ]);
            return $hashtags;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
