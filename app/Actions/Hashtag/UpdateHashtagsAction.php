<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Hashtag;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use Mockery\Exception;

class UpdateHashtagsAction extends Action {
    protected $repository;

    public function __construct(HashtagRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $hashtag = $this->repository->update([
                'hash_tag' => $data['hash_tag']
            ], $data['hash_seq']);
            return $hashtag;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
