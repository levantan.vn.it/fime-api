<?php


namespace App\Actions\Hashtag;


use App\Actions\Action;
use App\Contracts\HashtagRepositoryInterface;

class GetAllHashtagsAction extends Action {
    protected $hashTagRepository;

    public function __construct(HashtagRepositoryInterface $hashTagRepository) {
        $this->hashTagRepository = $hashTagRepository;
    }

    public function run($params) {
        if(!empty($params['pageSize']) && $params['pageSize'] != "null"){
            $hashtags = $this->hashTagRepository->scopeQuery(function($query) use ($params){
                if(isset($params['name']) && $params['name'] != "null") {
                    $query = $query->where('hash_tag', 'like', '%' . $params['name'] . '%');
                }
                if(isset($params['status']) && $params['status'] != "null") {
                    $query = $query->where('status', $params['status']);
                }
                return $query->orderBy('TCT_POPHASH.status', 'desc')
                ->orderBy('TCT_POPHASH.hash_cnt','desc')
                ->groupBy('hash_tag');
            })->paginate($params['pageSize']);
        }else{
            $hashtags = $this->hashTagRepository->scopeQuery(function ($query) {
                $query = $query->orderBy('TCT_POPHASH.hash_seq', 'desc');
                return $query;
            })->all();
        }
        
        return $hashtags;
    }
}
