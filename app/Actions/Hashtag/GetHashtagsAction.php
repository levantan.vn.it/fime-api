<?php

namespace App\Actions\Hashtag;
use App\Actions\Action;
use App\Contracts\HashtagRepositoryInterface;
use App\Criterias\Hashtag\GetTopHashtagsCriteria;

class GetHashtagsAction extends Action {
    protected $hashtag_repository;

    public function __construct(HashtagRepositoryInterface $hashtag_repository) {
        $this->hashtag_repository = $hashtag_repository;
    }

    public function run() {
        $hashtags = $this->hashtag_repository->getByCriteria(new GetTopHashtagsCriteria());
       return $hashtags;
    }
}
