<?php

namespace App\Actions\Hashtag;
use App\Actions\Action;
use App\Repositories\HashtagRepository;
use App\Contracts\HashtagRepositoryInterface;
use Mockery\Exception;

class ToggleHashtagsAction extends Action {
    protected $hashtag_repository;

    public function __construct(HashtagRepositoryInterface $hashtag_repository) {
        $this->hashtag_repository = $hashtag_repository;
    }

    public function run($ids, $toggle) {
        try {
            $hashtags = $this->hashtag_repository->findWhereIn('hash_seq', $ids);

            foreach ($hashtags as $hashtag) {
                if($toggle == false) {
                    $this->hashtag_repository->update([
                        'status' => 0
                    ], $hashtag->hash_seq);
                } else {
                    $this->hashtag_repository->update([
                        'status' => 1
                    ], $hashtag->hash_seq);
                }
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
