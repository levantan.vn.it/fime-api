<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 10:58 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;

class Login extends Action
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;
    }
    /**
     * @param $request
     * @return array
     */
    public function run($request)
    {
        $credentials = ['email' => $request['email'], 'password' => $request['password']];

        if (!$token = auth()->attempt($credentials)) {
            return ['error' => 'Unauthorized', 'errorCode' => 1];
        }
        if(empty(auth()->user()->verification)){
            auth()->logout();
            return ['error' => 'Inactive account', 'errorCode' => 3];
        }
        if (auth()->user()->deleted_at == null && auth()->user()->drmncy_at != 'Y') {
            $update = [
                'last_login_dt' => Carbon::now()->toDateString()
            ];
            if(empty(auth()->user()->update_pic) && !empty(auth()->user()->pic)){
                try {
                
                    $file_name = auth()->user()->pic;
                    $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = pathinfo($file_name, PATHINFO_FILENAME);
                    $new_url = 'https://api-np.fime.vn'.$file_name;
                    $file_thumb_name = $name . '_TMB' . '.' . $extension;
                    $image_thumb = \Image::make($new_url)->resize(150, null,function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $image_thumb->save((public_path().'/data/images/profile/'.$file_thumb_name));
                
                    $update['update_pic'] = '/data/images/profile/'.$file_thumb_name;
                }catch (\Intervention \ Image \ Exception \ NotReadableException $e) {
                    $update['update_pic'] = $file_name;
                }catch (\Intervention \ Image \ Exception \ NotSupportedException $e) {
                    $update['update_pic'] = $file_name;
                }
            }
            $this->userRepository->update($update, auth()->user()->user_no);
            
            return [
                'access_token' => $token,
                'user' => auth()->user(),
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } else {
            auth()->logout();
            return ['error' => 'Inactive account', 'errorCode' => 2];
        }
    }
}