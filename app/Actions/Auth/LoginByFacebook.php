<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 10:58 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;

class LoginByFacebook extends Action
{
    private $userRepository;

    /**
     * LoginByFacebook constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $request
     * @return array
     */
    public function run($request)
    {
        try {
            // Extract data from request
            $userID = $request['userID'];
            $is_existed_ID = $this->userRepository->findWhere(['sns_id' => $userID]);

            if (count($is_existed_ID) == 0) {
                // Not have account in database yet
                // Create a new one
                $user_no = 'U' . date('YmdHis');
                $fbInfo = json_decode($request['data']);
                $data = ['user_no' => $user_no, 'sns_id' => $userID, 'reg_name' => $fbInfo->name ?? '', 'home_zip' => 'NP000'];

                try {
                    $newUserIdentity = $this->userRepository->create($data);
                    return ['identity_id' => $newUserIdentity];
                } catch (\Exception $err) {
                    return [
                        'error' => 'Something went wrong.'
                    ];
                }

            } else {
                // If user have logged by FB before
                // Find user reference by user_id of userIdentity
                $userIdentity = $is_existed_ID[0];

                // If user have verify account, get user and return JWT token
                if (!empty($userIdentity->user_no) && !empty($userIdentity->email)) {
                    $user = $this->userRepository->find($userIdentity->user_no);
                    if(empty($user->verification)){
                        auth()->logout();
                        return ['error' => 'Inactive account', 'errorCode' => 3];
                    }
                    // If account has been deleted
                    if ($user->deleted_at != null) {
                        return ['error' => 'Inactive account', 'errorCode' => 2];
                    }
                    $token = auth()->fromUser($user);
                    return [
                        'access_token' => $token,
                        'user' => $user,
                        'expires_in' => auth()->factory()->getTTL() * 60
                    ];
                } else {
                    // Return identity id to redirect page to verify user info
                    return [
                        'identity_id' => $userIdentity
                    ];
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
            return [
                'error' => 'Something went wrong.'
            ];
        }
    }
}
