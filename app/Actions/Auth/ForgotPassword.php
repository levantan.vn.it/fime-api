<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/18/2019
 * Time: 10:24 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;
use App\Contracts\PasswordResetRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class ForgotPassword extends Action
{
    private $userRepository;
    private $passwordResetRepository;

    function __construct(UserRepositoryInterface $userRepository,
                         PasswordResetRepositoryInterface $passwordResetRepository) {
        $this->userRepository = $userRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    /**
     * @param $request
     * @return array
     */
    public function run($request){
        $user = $this->userRepository->findWhere(['email' => $request['email']]);
        if (count($user)) {
            /**
             * Create a new token to be sent to the user, save it into database
             */
            $email = $request['email'];
            $token = str_random(60);
            $created_at = Carbon::now();
            $expired_at = $created_at->addDay(3);
            $data = ['email' => $email, 'token' => $token, 'created_at' => $created_at, 'expired_at' => $expired_at];
            $last_token = $this->passwordResetRepository->findWhere(['email' => $request['email']]);
            if (count($last_token) == 0) {
                $this->passwordResetRepository->create($data);
            } else {
                $last_token = $last_token[0];
                $this->passwordResetRepository->update($data, $last_token->id);
            }

            /**
             * Send email to the email above with a link to your password reset
             */
            $url = env('APP_UI_URL', 'https://web-np.fime.vn') . '/reset-password/' . $token;
            $forgot_password_url = env('APP_UI_URL', 'localhost:4200') . '/forgot-password/';
            Mail::send('resetPasswordMail', ['url' => $url, 'forgot_password_url' => $forgot_password_url], function ($message) use ($user, $email) {
                $to = $email;
                $message->to($email, $user[0]->name)->subject('fi:me reset password');
            });
            return [
              'email' => 'sent'
            ];
        }
        return [
            'error' => 'Email does not exist'
        ];
    }
}