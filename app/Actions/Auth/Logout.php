<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/18/2019
 * Time: 10:23 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;

class Logout extends Action
{
    /**
     * @return array
     */
    public function run(){
        auth()->logout();
        return auth()->user();
    }
}