<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/18/2019
 * Time: 10:24 AM
 */

namespace App\Actions\Auth;


use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class Register extends AddPointsToUserAction
{
    private $userRepository;

    function __construct(UserRepositoryInterface $userRepository,
                         UserPointRepositoryInterface $user_point_repository, CodeRepositoryInterface $codeRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct($user_point_repository, $codeRepository);
    }

    /**
     * @param $request
     * @return array
     */
    public function run($request)
    {
        $is_email_existed = $this->userRepository->findWhere(['email' => $request['email']]);
        if (count($is_email_existed) > 0) {
            return [
                'error' => 1
            ];
        }
        $is_display_name_existed = $this->userRepository->findWhere(['id' => $request['displayName']]);
        if (count($is_display_name_existed) > 0) {
            return [
                'error' => 2
            ];
        }
        $phone = '';
        if(!empty($request['phone'])){
            $is_phone_existed = $this->userRepository->findWhere(['cellphone' => $request['phone']]);
            if (count($is_phone_existed) > 0) {
                return [
                    'error' => 3
                ];
            }
            $phone = $request['phone'];
        }
        
        $hashedPassword = Hash::make($request['password']);
        $user_no = !empty($request['user_no']) ? $request['user_no'] : 'U' . date('YmdHis');
        $access_token = str_random(10);
//        $data = ['user_no' => $user_no, 'email' => $request['email'], 'password' => $hashedPassword, 'reg_name' => $request['name']];
//        $this->userRepository->create($data);

        
        $this->userRepository->updateOrCreate(
            [
                'user_no' => $user_no
            ],
            [
                'email' => $request['email'],
                'password' => $hashedPassword,
                'reg_name' => $request['name'],
                'cellphone' => $phone,
                'id' => $request['displayName'],
                'slug' => $request['displayName'],
                'home_zip' => 'NP001',
                'verification'  =>  0,
                'verification_max'  =>  date('Y-m-d H:i:s'),
                'access_token'  =>  $access_token
            ]
        );

        // Add point
        $this->joined($user_no);

        if(!empty($request['phone'])){
            $to_name = $request['name'];
            $to_email = $request['email'];
            $data = array(
                'user_no'   =>  $user_no,
                'access_token'  =>  $access_token,
                'url'   =>  'http://fime.vn/verify/'.$user_no.'/'.$access_token
            );
            \Mail::send('emails.verification', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject("Email Xác thực(Verify Email)");
            });
            return true;
            
        }else{
            // Login user
            $credentials = ['email' => $request['email'], 'password' => $request['password'], 'deleted_at' => null];

            if (!$token = auth()->attempt($credentials)) {
                return ['error' => 'Unauthorized'];
            }

            return [
                'access_token' => $token,
                'user' => auth()->user(),
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        }
        
    }
}
