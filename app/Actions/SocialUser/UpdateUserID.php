<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 10:58 AM
 */

namespace App\Actions\SocialUser;


use App\Actions\Action;
use App\Contracts\SocialUserRepositoryInterface;

class UpdateUserID extends Action
{
    private $socialUserRepository;

    /**
     * UpdateUserID constructor.
     * @param SocialUserRepositoryInterface $socialUserRepository
     */
    public function __construct(SocialUserRepositoryInterface $socialUserRepository)
    {
        $this->socialUserRepository = $socialUserRepository;
    }

    /**
     * @param $request
     * @return array
     */
    public function run($request)
    {
        try {
            $identity_id = $request['identity_id'];
            $user_id = $request['user_id'];
            $userIdentity = $this->socialUserRepository->findWhere(['id' => $identity_id]);
            if (count($userIdentity) > 0) {
                if ($userIdentity[0]->user_id == null) {
                    $this->socialUserRepository->update(['user_id' => $user_id], $userIdentity[0]->id);
                    return [
                        'message' => 'done'
                    ];
                } else {
                    return [
                        'error' => 'Invalid identity id'
                    ];
                }
            } else {
                return [
                    'error' => 'Invalid identity id'
                ];
            }
        } catch (\Exception $e) {
            \Log::error($e);
            return [
                'error' => 'Invalid identity id'
            ];
        }

    }
}