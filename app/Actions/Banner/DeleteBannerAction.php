<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Contracts\BannerRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class DeleteBannerAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run($ids) {
        try {
            foreach ($ids as $id) {
                $this->banner_repository->delete($id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
