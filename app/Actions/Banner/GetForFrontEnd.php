<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/5/2019
 * Time: 10:53 AM
 */

namespace App\Actions\Banner;


use App\Actions\Action;
use App\Contracts\BannerRepositoryInterface;
use Carbon\Carbon;

class GetForFrontEnd extends Action
{
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository)
    {
        $this->banner_repository = $banner_repository;
    }

    public function run()
    {
        $banner = $this->banner_repository->findWhere([
            'is_disabled' => 0,
            ['period_from', '<=', Carbon::now()],
            ['period_to', '>', Carbon::now()]
        ]);
        return $banner;
    }
}

