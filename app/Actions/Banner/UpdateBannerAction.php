<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Actions\Action;
use App\Contracts\BannerRepositoryInterface;
use Mockery\Exception;

class UpdateBannerAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run($data) {
        try {
            $banner = $this->banner_repository->update([
                'name' => trim($data['name']),
                'description' => isset($data['description']) ? $data['description'] : null,
                'resource_type' => isset($data['resource_type']) ? $data['resource_type'] : null,
                'url' => isset($data['url']) ? $data['url'] : null,
                'target_url' => isset($data['target_url']) ? $data['target_url'] : null,
                'target_type' => isset($data['target_type']) ? $data['target_type'] : null,
                'button_text' => isset($data['button_text']) ? $data['button_text'] : null,
                'period_from' => isset($data['period_from']) ? $data['period_from'] : null,
                'period_to' => isset($data['period_to']) ? $data['period_to'] : null,
                'order' => isset($data['order']) ? $data['order'] : 0,
            ], $data['id']);
            return $banner;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
