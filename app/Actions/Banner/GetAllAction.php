<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Contracts\BannerRepositoryInterface;
use App\Actions\Action;
use Carbon\Carbon;
class GetAllAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run($params) {
    	$banners = $this->banner_repository->scopeQuery(function($query) use ($params){
            if(isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('name', 'like', '%' . $params['name'] . '%');
            }

            if(isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('period_from', '>=', Carbon::parse($params['from']));
            }

            if(isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('period_to', '<', Carbon::parse($params['to'])->addDay(1));
            }
            return $query;
        });
        if(!empty($params['pageSize']) && $params['pageSize'] != "null"){
        	$banners = $banners->paginate($params['pageSize']);
        }else{
        	$banners = $banners->all();
        }
       	return $banners;
    }
}
