<?php

namespace App\Actions\TextColor;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class GetAllAction extends Action
{
    private $codeRepository;

    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function run()
    {
        try {
            return $this->codeRepository->findByField('code_group', 400);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
