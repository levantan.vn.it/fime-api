<?php

namespace App\Actions\TextColor;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class CreateAction extends Action
{
    private $codeRepository;

    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function run($data)
    {
        try {
            $color = $data['color'];
            $data = [
                'code' => '400003',
                'code_nm' => $color,
                'code_group' => 400,
                'REGISTER_ESNTL_NO' => 'MB00000000',
                'UPDUSR_ESNTL_NO' => 'MB00000000'
            ];
            $this->codeRepository->updateOrCreate($data);
            return $this->codeRepository->findByField('code_group', 400);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
