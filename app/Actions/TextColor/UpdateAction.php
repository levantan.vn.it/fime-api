<?php

namespace App\Actions\TextColor;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class UpdateAction extends Action
{
    private $codeRepository;

    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function run($data)
    {
        try {
            $colors = $data['colors'];
            foreach ($colors as $color) {
                $this->codeRepository->update([
                    'code_nm' => $color['code_nm']
                ], $color['code']);
            }
            return $this->codeRepository->findByField('code_group', 400);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
