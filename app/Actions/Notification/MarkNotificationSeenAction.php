<?php

namespace App\Actions\Notification;

use App\Actions\Action;
use App\Contracts\NotificationRepositoryInterface;

class MarkNotificationSeenAction extends Action
{
    protected $notification_repository;

    public function __construct(NotificationRepositoryInterface $notification_repository)
    {
        $this->notification_repository = $notification_repository;
    }

    function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid =
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
            return $uuid;
        }
    }

    protected function cloneSystemNotification($notification) {
        $data = [];
        $data['title'] = $notification->title;
        $data['content'] = $notification->content;
        $data['url'] = $notification->url;
        $data['is_disabled'] = $notification->is_disabled;
        $data['is_system'] = $notification->is_system;
        $data['type'] = $notification->type;
        $data['user_id'] = auth()->id();
        $data['object_id'] = $notification->object_id;
        $data['object_type'] = $notification->object_type;
        $data['reference_user_id'] = $notification->reference_user_id;
        $data['guid'] = $notification->guid;
        $data['is_seen'] = 1;
        $data['posted_at'] = $notification->posted_at;
        $this->notification_repository->create($data);
    }

    public function run($ids)
    {
        try {
            $notifications = $this->notification_repository->findWhereIn('id', $ids);
            foreach ($notifications as $notification) {
                if ($notification->is_system == 0 && $notification->is_seen == 0) {
                    $this->notification_repository->update([
                        'is_seen' => 1
                    ], $notification->id);
                } else {
                    if ($notification->user_id == 0) {
                        $this->cloneSystemNotification($notification);
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
