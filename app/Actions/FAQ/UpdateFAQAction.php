<?php

namespace App\Actions\FAQ;
use App\Actions\Action;
use App\Contracts\FAQRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use Mockery\Exception;

class UpdateFAQAction extends Action {
    protected $faq_repository;
    protected $try_ext_repository;

    public function __construct(FAQRepositoryInterface $faq_repository, TryExtRepositoryInterface $try_ext_repository) {
        $this->faq_repository = $faq_repository;
        $this->try_ext_repository = $try_ext_repository;
    }

    public function run($data) {
        try {
            $faq = $this->faq_repository->update([
                'sj' => isset($data['sj']) ? $data['sj'] : null,
                'cn' => isset($data['cn']) ? $data['cn'] : null,
                'faq_se_code' => isset($data['faq_se_code']) ? $data['faq_se_code'] : null,
            ], $data['cntnts_no']);

            $this->try_ext_repository->update([
                'sj' => isset($data['sj']) ? $data['sj'] : null,
            ], $data['cntnts_no']);

            return $faq;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
