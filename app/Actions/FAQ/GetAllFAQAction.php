<?php

namespace App\Actions\FAQ;
use App\Actions\Action;
use App\Contracts\FAQRepositoryInterface;
use App\Criterias\FAQ\GetFAQCategoryAndFAQCriteria;

class GetAllFAQAction extends Action {
    protected $faq_repository;

    public function __construct(FAQRepositoryInterface $faq_repository) {
        $this->faq_repository = $faq_repository;
    }

    public function run() {
        $faqs = $this->faq_repository->getByCriteria(new GetFAQCategoryAndFAQCriteria());
        return $faqs;
    }
}
