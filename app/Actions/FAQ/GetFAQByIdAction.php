<?php

namespace App\Actions\FAQ;
use App\Actions\Action;
use App\Contracts\FAQRepositoryInterface;
use App\Criterias\FAQ\GetFAQCategoryAndFAQByIdCriteria;

class GetFAQByIdAction extends Action {
    protected $faq_repository;

    public function __construct(FAQRepositoryInterface $faq_repository) {
        $this->faq_repository = $faq_repository;
    }

    public function run($code) {
        try {
            $faq = $this->faq_repository->pushCriteria(new GetFAQCategoryAndFAQByIdCriteria($code))->first();
            return $faq;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
