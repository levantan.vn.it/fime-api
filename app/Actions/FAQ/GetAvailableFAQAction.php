<?php

namespace App\Actions\FAQ;
use App\Actions\Action;
use App\Contracts\FAQRepositoryInterface;
use App\Criterias\FAQ\GetAvailableFAQByCategoryCriteria;

class GetAvailableFAQAction extends Action {
    protected $faq_repository;

    public function __construct(FAQRepositoryInterface $faq_repository) {
        $this->faq_repository = $faq_repository;
    }

    public function run($faq_se_code) {
        $faqs = $this->faq_repository->getByCriteria(new GetAvailableFAQByCategoryCriteria($faq_se_code));
        return $faqs;
    }
}
