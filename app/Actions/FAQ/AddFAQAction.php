<?php

namespace App\Actions\FAQ;

use App\Actions\Action;
use App\Contracts\FAQRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use Mockery\Exception;

class AddFAQAction extends Action
{
    protected $repository;
    protected $try_ext_repository;

    public function __construct(FAQRepositoryInterface $repository, TryExtRepositoryInterface $try_ext_repository)
    {
        $this->repository = $repository;
        $this->try_ext_repository = $try_ext_repository;
    }

    public function run($data)
    {
        try {
            $last = $this->try_ext_repository->scopeQuery(function($query) {
                return $query->orderBy('cntnts_no','desc')
                    ->take(1);
            })->all()->first();

            $cntnts_no = $last->cntnts_no + 1;

            $query = [
                'cntnts_no' => $cntnts_no,
                'sj' => isset($data['sj']) ? $data['sj'] : null,
                'cn' => isset($data['cn']) ? $data['cn'] : null,
                'faq_se_code' => isset($data['faq_se_code']) ? $data['faq_se_code'] : null,
            ];

            $result = $this->repository->create($query);

            $this->try_ext_repository->create([
                'cntnts_no' => $cntnts_no,
                'sj' => isset($data['sj']) ? $data['sj'] : null,
                'delete_at' => 'N'
            ]);

            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
