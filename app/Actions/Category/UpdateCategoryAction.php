<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Category;

use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class UpdateCategoryAction extends Action
{
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $code = substr( $data['code'], -3);
            $category = $this->repository->update([
                'code' => !empty($data['code_group'].$code) ? $data['code_group'].$code + 1 : $data['code_group'].$code,
                'code_nm' => $data['code_nm'],
                'code_group' => $data['code_group'],
                'additional' => $data['additional'],
                'additional_inactive' => $data['additional_inactive']
            ], $data['code']);
            return $category;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
