<?php
namespace App\Actions\Category;

use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class DeleteCategoryAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
                $this->repository->update([
                    'use_at' => 'N'
                ], $id);
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
