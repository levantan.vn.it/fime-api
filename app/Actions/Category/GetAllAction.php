<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Category;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\CodeRepositoryInterface;

class GetAllAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($params) {
      $params['code_group']= Constant::$CATEGORY_GROUP_CODE;
        if(!empty($params['pageSize'])){
            $categories = $this->repository->scopeQuery(function($query) use ($params){
                $query = $query->where('code_nm','!=', "SKIN");
                $query = $query->where(function ($model) use ($params){
                    $model = $model->where('code_group', $params['code_group']);
                    $model = $model->orWhere('code_group', '>', '411');
                    return $model;
                });
                $query = $query->where('use_at', '=', 'Y');
                if(isset($params['name']) && $params['name'] != "null") {
                    $query = $query->where('code_nm', 'like', '%' . $params['name'] . '%');
                }
                if(isset($params['category_id']) && $params['category_id'] != "null") {
                    $query = $query->where('code_group', $params['category_id']);
                }
                return $query;
            })->paginate($params['pageSize'],['code','code_group','code_nm','sort_ordr','use_at','slug','additional_inactive','additional']);
        }else{
          $categories = $this->repository->scopeQuery(function($query) use ($params){
              $query = $query->where('code_nm','!=', "SKIN");
              $query = $query->where(function ($model) use ($params){
                  $model = $model->where('code_group', $params['code_group']);
                  $model = $model->orWhere('code_group', '>', '411');
                  return $model;
              });
              $query = $query->where('use_at', '=', 'Y');
              if(isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('code_nm', 'like', '%' . $params['name'] . '%');
            }
            if(isset($params['category_id']) && $params['category_id'] != "null") {
                $query = $query->where('code_group', $params['category_id']);
            }
              return $query;
          })->get(['code','code_group','code_nm','sort_ordr','use_at','slug','additional_inactive','additional']);
        }
        
       return $categories;
    }
}
