<?php

namespace App\Actions\Setting;

use App\Actions\Action;
use App\Contracts\SettingRepositoryInterface;

class UpdateSettingAction extends Action
{
    protected $setting_repository;

    public function __construct(SettingRepositoryInterface $setting_repository)
    {
        $this->setting_repository = $setting_repository;
    }

    public function run($settings)
    {
        if (!empty($setting['id'])) {
            foreach ($settings['data'] as $key => $setting) {
                $this->setting_repository->update([
                    'value' => $setting['value']
                ], $setting['id']);
            }
        } else{
            foreach ($settings['data'] as $key => $setting) {
                $id = $this->setting_repository->findByField(['key' => $key])->first();
                $this->setting_repository->update([
                    'value' => $setting['value']
                ], $id->id);
            }
        }


        return $this->setting_repository->all();
    }
}
