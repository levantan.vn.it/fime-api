<?php

namespace App\Actions\Setting;
use App\Actions\Action;
use App\Contracts\SettingRepositoryInterface;

class GetByGroupAction extends Action {
    protected $setting_repository;

    public function __construct(SettingRepositoryInterface $setting_repository) {
        $this->setting_repository = $setting_repository;
    }

    public function run($group) {
        $settings = $this->setting_repository->findWhere([
            'group' => $group
        ]);
       return $settings;
    }
}
