<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\FAQCategory;

use App\Actions\Action;
use App\Contracts\CategoryRepositoryInterface;

class GetByAction extends Action {
    protected $repository;

    public function __construct(CategoryRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        $category = $this->repository->find($id);
       return $category;
    }
}
