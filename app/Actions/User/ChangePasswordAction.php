<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 2:01 PM
 */

namespace App\Actions\User;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;

class ChangePasswordAction extends Action
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run($data) {
        try {
            $old_password = $data['old_password'];
            $new_password = $data['new_password'];
            $user = Auth::user();
            if (Hash::check($old_password, $user->password)) {
                if (Hash::check($new_password, $user->password)) {
                    return ['error' => true, 'message' => 'New password can not same old'];
                } else {
                    $this->userRepository->update([
                        'password' => Hash::make($new_password)
                    ], $user->user_no);
                    return ['error' => false, 'message' => 'Update password successfully'];
                }
            } else {
                return ['error' => true, 'message' => 'Old password is not correct'];
            }

        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
