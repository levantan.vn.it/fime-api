<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/6/2019
 * Time: 2:18 PM
 */

namespace App\Actions\TryFree;


use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\TryFree\GetTriesOfUserCriteria;
use App\Criterias\TryFree\SearchTriesCriteria;

class SearchTries extends GetTriesAction
{
    protected $repository;
    public function __construct(
        TryRepositoryInterface $tryRepository,
        UserTryRepositoryInterface $userTriesRepository,
        UserRepositoryInterface $userRepository,
        UserLikeRepositoryInterface $userLikeRepository,
        CommentRepositoryInterface $commentRepository,
        UserFollowRepositoryInterface $followRepository
    )
    {
        $this->repository = $tryRepository;
        parent::__construct($tryRepository, $commentRepository,
            $userLikeRepository, $userTriesRepository, $userRepository, $followRepository);
    }

    public function run($request)
    {
        $tries = $this->repository->pushCriteria(new SearchTriesCriteria($request['searchValue']))->paginate(10);

        $this->decorateData($tries);

        return $tries;
    }

}
