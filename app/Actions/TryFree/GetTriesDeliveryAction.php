<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use App\Criterias\UserTry\GetNumberOfDeliveredByTriesIdsCriteria;
use App\Criterias\TryFree\GetTriesOfUserCriteria;
use App\Criterias\UserTry\GetAllCriteria;

use Carbon\Carbon;

class GetTriesDeliveryAction extends Action {
    protected $repository;
    protected $code_repository;
    protected $user_try_repository;
    protected $user_like_repository;

    public function __construct(TryRepositoryInterface $repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                CodeRepositoryInterface $code_repository,
                                UserTryRepositoryInterface $user_try_repository) {
        $this->repository = $repository;
        $this->code_repository = $code_repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_try_repository = $user_try_repository;
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->repository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    // protected function getNumberLikesOfTry($tries) {
    //     $try_ids = $tries->pluck('cntnts_no')->toArray();

    //     $try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($try_ids));
    //     $try_likes = collect($try_likes)->keyBy('cntnts_no');

    //     foreach ($tries as $try) {
    //         if(isset($try_likes[$try->cntnts_no])) {
    //             $try->likes = $try_likes[$try->cntnts_no]->like_number;
    //         } else {
    //             $try->likes = 0;
    //         }
    //     }
    //     return $tries;
    // }

    protected function getTotalApply($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->total_apply = 0;
            if (isset($number_of_tries[$try->cntnts_no])) {
                $try->total_apply = $number_of_tries[$try->cntnts_no]->number_of_tries;
            }
        }
        return $tries;
    }

    protected function getTotalSelect($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $number_of_delivered = $this->user_try_repository->getByCriteria(new GetNumberOfDeliveredByTriesIdsCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->total_delivered = 0;
            if (isset($number_of_delivered[$try->cntnts_no])) {
                $try->total_delivered = $number_of_delivered[$try->cntnts_no]->number_of_delivered;
            }
        }
        return $tries;
    }

    

    protected function getStatus($tries) {
        $now = Carbon::now('UTC');
        foreach ($tries as $try) {
            $startTime = $try->event_bgnde;
            $endTime = $try->event_endde;
            if ($endTime > $now) {
                // Coming soon
                $try->count_down_type = 'Stand by';
            } else if ($startTime <= $now && $endTime > $now) {
                // On air
                $try->count_down_type = 'On air';
            } else {
                $try->count_down_type = 'Expired';
            }
        }
        return $tries;
    }

    public function run($params) {
        $tries = $this->user_try_repository->scopeQuery(function($query) use ($params){
            $query = $query->join('TCT_GOODS', 'TCT_DRWT.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no');
            $query = $query->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code');

            $query = $query->where('TOM_CNTNTS_WDTB.delete_at', 'N');
            $query = $query->where('TOM_CNTNTS_WDTB.expsr_at', 'Y');
            $query = $query->where('TCT_DRWT.slctn_at', 'Y');
            $query = $query->where('event_endde', '<', Carbon::yesterday());

            if(isset($params['is_disabled']) && $params['is_disabled'] != "null") {
                if ($params['is_disabled'] == 'Y') {
                    $query = $query->where('event_bgnde', '<', Carbon::now());
                    $query = $query->where('event_endde', '>', Carbon::now());
                } 
                else {
                    $query = $query->where('event_endde', '<', Carbon::now());
                }
            }

            if(isset($params['brand_id']) && $params['brand_id'] != "null") {
                $query = $query->where('brnd_code', $params['brand_id']);
            }

            if(isset($params['category_id']) && $params['category_id'] != "null") {
                $query = $query->where('goods_cl_code', $params['category_id']);
            }

            if(isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('sj', 'like', '%' . $params['name'] . '%');
            }

            if(isset($params['type']) && $params['type'] != "null") {
                $query = $query->where('event_knd_code', '=', $params['type']);
            }

            if(isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('dlvy_dt', '>=', Carbon::parse($params['from']));
            }

            if(isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('dlvy_dt', '<', Carbon::parse($params['to'])->addDay(1));
            }

            if(isset($params['is_event']) && $params['is_event'] != "null") {
                if ($params['is_event'] == 1) {
                    $query = $query->where('is_try_event', '=', 1);
                }
            } else {
                $query = $query->where('is_try_event', '=', 0);
            }

            $query = $query->select('TCT_GOODS.cntnts_no',
                'TCT_GOODS.link_url',
                'TCT_GOODS.modl_nombr',
                'TCT_GOODS.is_try_event',
                'TCT_GOODS.try_event_type',
                'TCT_GOODS.brnd_nm',
                'TCT_GOODS.brnd_code',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.event_bgnde',
                'TCT_GOODS.event_endde',
                'TCT_GOODS.dlvy_bgnde',
                'TCT_GOODS.dlvy_endde',
                'TCT_GOODS.event_trgter_co',
                'TCT_GOODS.event_pc',
                'TCT_GOODS.time_color_code',
                'TCT_GOODS.slctn_compt_at',
                'TCT_GOODS.goods_cl_code',
                'TCT_GOODS.goods_txt_code',
                'TCT_GOODS.goods_txt',
                'TCT_GOODS.p_cnt',
                'TCT_GOODS.m_cnt',
                'TCT_GOODS.quantity_to_qualify',
                'TCT_GOODS.resource_type',
                'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.regist_dt AS created_at',
                'TOM_CNTNTS_WDTB.expsr_at AS is_disabled', 'TOM_CNTNTS_WDTB.slug','TCT_DRWT.dlvy_dt','TCT_DRWT.invc_no','TCT_DRWT.slctn_at','TSM_CODE.code_nm AS category_name');
                 
                 $query = $query->orderBy('TCT_GOODS.event_endde','desc');
                 $query = $query->orderBy('TCT_DRWT.dlvy_dt','desc');
                 // ->orderBy('TOM_CNTNTS_WDTB.regist_dt', 'desc');
                 $query = $query->groupBy('TCT_GOODS.cntnts_no');
            return $query->orderBy($params['column'], $params['sort']);
            
        })->paginate($params['pageSize']);

        $brand_ids = $tries->pluck('brnd_code');

        $brands = $this->code_repository->findWhereIn('code', $brand_ids->toArray())->keyBy('code');

        foreach ($tries as $item) {
            if(isset($brands[$item->brnd_code])) {
                $item->brand = $brands[$item->brnd_code];
            }
        }
        
        
        
        $tries = $this->getFiles($tries);
        // $tries = $this->getNumberLikesOfTry($tries);
        $tries = $this->getTotalApply($tries);
        $tries = $this->getStatus($tries);
        $tries = $this->getTotalSelect($tries);
       return $tries;
    }
}
