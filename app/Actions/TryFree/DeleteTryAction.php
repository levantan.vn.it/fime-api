<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryFilesRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use Mockery\Exception;

class DeleteTryAction extends Action {
    protected $repository;
    protected $user_like_repository;
    protected $user_try_repository;
    protected $comment_repository;
    protected $try_files_repository;

    public function __construct(TryExtRepositoryInterface $repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                CommentRepositoryInterface $comment_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                TryFilesRepositoryInterface $try_files_repository) {
        $this->repository = $repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_try_repository = $user_try_repository;
        $this->comment_repository = $comment_repository;
        $this->try_files_repository = $try_files_repository;

    }

    public function run($ids) {
        try {
            foreach ($ids as $id) {
                $this->repository->update([
                   'delete_at' => 'Y'
                ], $id);

                $this->user_like_repository->deleteWhere([
                    'cntnts_no' => $id
                ]);
//
//                $this->user_try_repository->deleteWhere([
//                    'cntnts_no' => $id
//                ]);
//
                $this->comment_repository->deleteWhere([
                    'cntnts_no' => $id,
                ]);

                $this->try_files_repository->deleteWhere([
                    'cntnts_no' => $id,
                ]);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
