<?php

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\TryFree\GetAvailableTriesCriteria;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetLikesOfCurrentUserCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\UserTry\GetApplyOfCurrentUserCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;

class GetAvailableTryAction extends Action
{
    protected $try_repository;
    private $user_like_repository;
    private $user_repository;
    private $user_try_repository;
    protected $followRepository;

    public function __construct(TryRepositoryInterface $try_repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                UserRepositoryInterface $user_repository,
                                UserFollowRepositoryInterface $followRepository = null)
    {
        $this->try_repository = $try_repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_repository = $user_repository;
        $this->user_try_repository = $user_try_repository;
        $this->followRepository = $followRepository;
    }

    protected function getNumberLikesOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($try_ids));
        $try_likes = collect($try_likes)->keyBy('cntnts_no');

        foreach ($tries as $try) {
            if (isset($try_likes[$try->cntnts_no])) {
                $try->likes = $try_likes[$try->cntnts_no]->like_number;
            } else {
                $try->likes = 0;
            }
        }
        return $tries;
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->followRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->followRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->followRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }

    protected function getListUserLike($tries)
    {
        foreach ($tries as $try) {
            $user_liked = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($try->cntnts_no));
            $fimers = $this->getCurrentUserFollowing($user_liked);
            $try->users_liked = $fimers;
        }
        return $tries;
    }

    protected function checkJoinTry($tries)
    {
        $current_user_id = auth()->id();
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $user_applies = $this->user_try_repository->getByCriteria(new GetApplyOfCurrentUserCriteria($try_ids, $current_user_id));
        $user_applies = $user_applies->keyBy('cntnts_no');

        foreach ($tries as $item) {
            if (isset($user_applies[$item->cntnts_no])) {
                $item->is_joined = 1;
            } else {
                $item->is_joined = 0;
            }
        }
        return $tries;
    }

    protected function checkCurrentUserLiked($tries)
    {
        $current_user_id = auth()->id();
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $user_liked = $this->user_like_repository->getByCriteria(new GetLikesOfCurrentUserCriteria($try_ids, $current_user_id));

        $user_liked = $user_liked->keyBy('cntnts_no');

        foreach ($tries as $item) {
            if (isset($user_liked[$item->cntnts_no])) {
                $item->is_liked = 1;
            } else {
                $item->is_liked = 0;
            }
        }

        return $tries;
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->try_repository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    public function run()
    {
        $tries = $this->try_repository->getByCriteria(new GetAvailableTriesCriteria());
        // $try_ids = $tries->pluck('cntnts_no')->toArray();

        // $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

        // foreach ($tries as $try) {
        //     $try->total_apply = 0;
        //     if (isset($number_of_tries[$try->cntnts_no])) {
        //         $try->total_apply = $number_of_tries[$try->cntnts_no]->number_of_tries;
        //     }
        // }

        // Decorate return data
        // $this->getNumberLikesOfTry($tries);
        $this->getListUserLike($tries);
        // $this->checkJoinTry($tries);
        // $this->checkCurrentUserLiked($tries);
        // $this->getFiles($tries);

        return $tries;
    }
}
