<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipsMappingRepositoryInterface;
use App\Models\TipsMapping;

class GetByAction extends Action {
    protected $repository;
    protected $tipsMappingRepository;

    public function __construct(TipsRepositoryInterface $repository,
    TipsMappingRepositoryInterface $tipsMappingRepository) {
        $this->repository = $repository;
        $this->tipsMappingRepository = $tipsMappingRepository;
    }

    public function run($id) {
        $result = $this->repository->scopeQuery(function($query) use ($id){
            $query = $query->select('TCT_TIPS.*')
            ->leftJoin('TCT_TIPS_MAPPING','TCT_TIPS_MAPPING.ti_id','=','TCT_TIPS.ti_id')
            ->leftJoin('TCT_REVIEW','TCT_REVIEW.review_no','=','TCT_TIPS_MAPPING.content_id');
            return $query->where('TCT_TIPS.ti_id', $id);
        })->first();
        
        $result->tipsReviews = $this->getIdReviewsTipMapping($id);
        $result->tipsTries = $this->getIdTryTipMapping($id);
        
       return $result;
    }

    protected function getIdReviewsTipMapping($tip) {
        $result = TipsMapping::where('ti_id','=', $tip)
            ->where('content_gb','=', 'R')->pluck('content_id')->toArray();
        
        return $result;
    }
    
    protected function getIdTryTipMapping($tip) {
        $result = TipsMapping::where('ti_id','=', $tip)
            ->where('content_gb','=', 'T')->pluck('content_id')->toArray();
        // dd($result);
        return $result;
    }
}
