<?php
namespace App\Actions\Tip;

use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use Mockery\Exception;

class ToggleTipAction extends Action {
    protected $repository;

    public function __construct(TipsRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id, $toggle) {
        try {
            $tips = $this->repository->findWhereIn('ti_id', $id);

            foreach ($tips as $tip) {
                if($toggle == false) {
                    $this->repository->update([
                        'display_yn' => 'N'
                    ], $tip->ti_id);
                } else {
                    $this->repository->update([
                        'display_yn' => 'Y'
                    ], $tip->ti_id);
                }
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
