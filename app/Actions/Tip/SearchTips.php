<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/6/2019
 * Time: 2:18 PM
 */

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Tip\SearchTipsCriterial;

class SearchTips extends Action
{
    protected $repository;
    public function __construct(
        TipsRepositoryInterface $tipRepository,
        UserRepositoryInterface $userRepository,
        UserLikeRepositoryInterface $userLikeRepository,
        CommentRepositoryInterface $commentRepository,
        UserFollowRepositoryInterface $followRepository
    )
    {
        $this->repository = $tipRepository;
        $this->commentRepository = $commentRepository;
        $this->userLikeRepository = $userLikeRepository;
        $this->userRepository = $userRepository;
        $this->followRepository = $followRepository;
    }

    public function run($request)
    {
        try {
            $tips = $this->repository->pushCriteria(new SearchTipsCriterial(356, $request['searchValue']))->paginate(10);
            return $tips;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

}
