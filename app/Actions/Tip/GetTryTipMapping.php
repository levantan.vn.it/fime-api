<?php

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\TryRepositoryInterface;
use Carbon\Carbon;

class GetTryTipMapping extends Action
{
    protected $repository;
    private $review_files_repository;

    public function __construct(TryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function run($id) {
        $result = $this->repository->scopeQuery(function($query) use ($id){
            $query = $query->select(
            'TCT_TIPS_MAPPING.content_id',
            'TCT_GOODS.cntnts_no',
            'TCT_GOODS.event_bgnde',
            'TCT_GOODS.event_endde',
            'TOM_CNTNTS_FILE_CMMN.*',
            'TOM_CNTNTS_WDTB.sj')
            ->leftJoin('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->leftJoin('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->leftJoin('TCT_TIPS_MAPPING','TCT_TIPS_MAPPING.content_id','=','TCT_GOODS.cntnts_no')
            ->leftJoin('TCT_TIPS','TCT_TIPS.ti_id','=','TCT_TIPS_MAPPING.ti_id');
            return $query->where('TCT_TIPS_MAPPING.content_gb','=', 'T')
            ->where('TOM_CNTNTS_FILE_CMMN.se_code', '=', Constant::$MAIN_SE_CODE)
            ->where('TCT_TIPS.ti_id', $id);
        })->get();

       return $result;
    }
}

