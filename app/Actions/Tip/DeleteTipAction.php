<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;
use App\Contracts\TipsRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class DeleteTipAction extends Action {
    protected $repository;

    public function __construct(TipsRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($ids) {
        try {
            foreach ($ids as $id) {
                $this->repository->delete($id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
