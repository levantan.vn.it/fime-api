<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/5/2019
 * Time: 9:28 AM
 */

namespace App\Actions\Tip;


use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipViewsRepositoryInterface;

class GetTopViewAction extends Action
{
    protected $tipsRepository;
    protected $tipViewsRepository;

    public function __construct(
        TipsRepositoryInterface $tipsRepository,
        TipViewsRepositoryInterface $tipViewsRepository
    )
    {
        $this->tipViewsRepository = $tipViewsRepository;
        $this->tipsRepository = $tipsRepository;
    }

    public function run()
    {
        $tips = $this->tipsRepository->scopeQuery(function ($query) {
            $query = $query->select('TCT_TIPS.ti_id',
            'TCT_TIPS.subject',
            'TCT_TIPS.img_url',
            'TCT_TIPS.cover_img1',
            'TCT_TIPS.created_at',
            'TCT_TIPS.view_cnt',
            // 'TCT_TIPS.tips_desc_head',
            // 'TCT_TIPS.tips_desc_body',
            'TCT_TIPS.updated_at');
            $query = $query->where('display_yn', 'Y');
            $query = $query->where('display_yn', 'Y');
            $query = $query->where('display_yn', 'Y');
            $query = $query->orderBy('TCT_TIPS.view_cnt', 'desc');
            return $query->take(8);
        })->all();
        return $tips;
    }
}
