<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipsMappingRepositoryInterface;
use Carbon\Carbon;
use Mockery\Exception;

class AddTipAction extends Action
{
    public $NowTime;
    protected $tipsMappingRepository;
    protected $repository;

    public function __construct(TipsRepositoryInterface $repository,
                                TipsMappingRepositoryInterface $tipsMappingRepository
    )
    {
        $this->repository = $repository;
        $this->tipsMappingRepository = $tipsMappingRepository;
    }

    public function run($data)
    {
        $this->NowTime = Carbon::now();
        $Now = $this->NowTime;
        try {
            $last = $this->repository->scopeQuery(function($query) {
                return $query->orderBy('ti_id','desc')
                    ->take(1);
            })->all()->first();

            $ti_id = $last->ti_id + 1;
            $data['ti_id'] = $ti_id;
            $data['subject'] = isset($data['subject']) ? $data['subject'] : null;
            $data['display_yn'] = isset($data['display_yn']) ? $data['display_yn'] : 'Y';
            $data['push_send_yn'] = isset($data['push_send_yn']) ? $data['push_send_yn'] : 'Y';
            $data['cont1_type'] = isset($data['cont1_type']) ? $data['cont1_type'] : 'P';
            $data['cont2_type'] = isset($data['cont2_type']) ? $data['cont2_type'] : 'P';
            $data['cover_img1'] = isset($data['cover_img1']) ? $data['cover_img1'] : null;
            $data['cover_img2'] = isset($data['cover_img2']) ? $data['cover_img2'] : null;
            $data['cover_img3'] = isset($data['cover_img3']) ? $data['cover_img3'] : null;
            $data['cover_img4'] = isset($data['cover_img4']) ? $data['cover_img4'] : null;
            $data['cont1_photo'] = isset($data['cont1_photo']) ? $data['cont1_photo'] : null;
            $data['cont2_photo'] = isset($data['cont2_photo']) ? $data['cont2_photo'] : null;
            $data['cont1_link_url'] = isset($data['cont1_link_url']) ? $data['cont1_link_url'] : null;
            $data['cont2_link_url'] = isset($data['cont2_link_url']) ? $data['cont2_link_url'] : null;
            $data['tips_desc_head'] = isset($data['tips_desc_head']) ? $data['tips_desc_head'] : null;
            $data['tips_desc_body'] = isset($data['tips_desc_body']) ? $data['tips_desc_body'] : null;
            $data['caption_text'] = isset($data['caption_text']) ? $data['caption_text'] : null;
            $data['source_site_name'] = isset($data['source_site_name']) ? $data['source_site_name'] : null;
            $data['source_site_url'] = isset($data['source_site_url']) ? $data['source_site_url'] : null;
            $data['cont1_head'] = isset($data['cont1_head']) ? $data['cont1_head'] : null;
            $data['cont1_body'] = isset($data['cont1_body']) ? $data['cont1_body'] : null;
            $data['code_brand'] = isset($data['code_brand']) ? $data['code_brand'] : null;
            $data['cont2_head'] = isset($data['cont2_head']) ? $data['cont2_head'] : null;
            $data['caption_text2'] = isset($data['caption_text2']) ? $data['caption_text2'] : null;
            $data['source_site_name2'] = isset($data['source_site_name2']) ? $data['source_site_name2'] : null;
            $data['source_site_url2'] = isset($data['source_site_url2']) ? $data['source_site_url2'] : null;
            $data['cont2_body'] = isset($data['cont2_body']) ? $data['cont2_body'] : null;
            $data['tips_code_group'] = 413;
            $data['tips_cl_code'] = isset($data['tips_cl_code']) ? $data['tips_cl_code'] : 0;
            $data['img_url'] = "/data/images/tips";
            $data['mov_url'] = "/data/videos/tips";
            $data['created_at'] = $Now->toDateTimeString('Y-m-d H:i:s');
            $tip = $this->repository->create($data);

            // $contents = $data['content_id'];
            if(is_array($data['content_id'])){
                $no = 1;
                foreach($data['content_id'] as $content) {
                    $this->tipsMappingRepository->create([
                        'ti_id' => $ti_id,
                        'content_gb' => 'R',
                        'tm_no' => $no,
                        'content_id' => $content
                    ]);
                    $no++;
                }
            }
            if(is_array($data['content_tries_id'])){
                $no = 1;
                foreach($data['content_tries_id'] as $content) {
                    $this->tipsMappingRepository->create([
                        'ti_id' => $ti_id,
                        'content_gb' => 'T',
                        'tm_no' => $no,
                        'content_id' => $content
                    ]);
                    $no++;
                }
            }
            return $tip;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
