<?php

namespace App\Actions\Tip;
use App\Actions\Action;
use App\Contracts\TryRepositoryInterface;



class GetTryByIdAction extends Action {
    protected $repository;
    public function __construct(TryRepositoryInterface $repository) {
        $this->repository = $repository;

    }

    public function run($id) {
        try {
            $try = $this->repository
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->find($id,['TCT_GOODS.cntnts_no','TOM_CNTNTS_WDTB.sj','TCT_GOODS.event_bgnde','TCT_GOODS.event_endde']);

            return $try;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
