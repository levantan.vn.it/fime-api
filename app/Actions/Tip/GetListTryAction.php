<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Tip;

use App\Actions\Action;
use App\Contracts\TryRepositoryInterface;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use Carbon\Carbon;

class GetListTryAction extends Action {
    protected $repository;

    public function __construct(TryRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->repository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    protected function getStatus($tries) {
        $now = Carbon::now('UTC');
        foreach ($tries as $try) {
            $startTime = $try->event_bgnde;
            $endTime = $try->event_endde;
            if ($startTime > $now) {
                // Coming soon
                $try->count_down_type = 'Stand by';
            } else if ($startTime <= $now && $endTime > $now) {
                // On air
                $try->count_down_type = 'On air';
            } else {
                $try->count_down_type = 'Expired';
            }
        }
        return $tries;
    }

    public function run($params) {
        
        $tries = $this->repository->scopeQuery(function($query) use ($params){
            $query = $query->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no');
            $query = $query->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code');

            $query = $query->where('TOM_CNTNTS_WDTB.delete_at', 'N');
            $query = $query->where('TCT_GOODS.event_endde', '>', Carbon::now());


            if(isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('sj', 'like', '%' . $params['name'] . '%');
            }


            if(isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('event_bgnde', '>=', Carbon::parse($params['from']));
            }

            if(isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('event_bgnde', '<', Carbon::parse($params['to'])->addDay(1));
            }


            $query = $query->select('TCT_GOODS.cntnts_no',
            'TCT_GOODS.event_bgnde',
            'TCT_GOODS.event_endde',
            'TCT_GOODS.resource_type', 'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.regist_dt AS created_at',
            'TSM_CODE.code_nm AS category_name', 'TOM_CNTNTS_WDTB.expsr_at AS is_disabled');
            return $query->orderBy('TCT_GOODS.cntnts_no', 'desc');

            
        })->paginate($params['pageSize']);
        // dd($tries);
        $tries = $this->getFiles($tries);
        $tries = $this->getStatus($tries);
       return $tries;
    }
}
