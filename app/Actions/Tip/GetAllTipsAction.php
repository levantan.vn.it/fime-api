<?php

/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:37 PM
 */

namespace App\Actions\Tip;


use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use App\Criterias\Tip\GetAllTipsByCriteria;
use App\Models\Tips;

class GetAllTipsAction extends Action
{
    protected $tipsRepository;

    public function __construct(TipsRepositoryInterface $tipsRepository)
    {
        $this->tipsRepository = $tipsRepository;
    }

    public function run($params)
    {
        $period = isset($params['period']) ? $params['period'] : 0;
        $pageSize = isset($params['pageSize']) ? $params['pageSize'] : 0;
        if ($pageSize != 0) {
            // select tip in admin
            $model = Tips::select('TCT_TIPS.ti_id',
                'TCT_TIPS.subject',
                'TCT_TIPS.img_url',
                'TCT_TIPS.cover_img1',
                'TCT_TIPS.cover_img2',
                'TCT_TIPS.cover_img3',
                'TCT_TIPS.cover_img4',
                'TCT_TIPS.created_at',
                'TCT_TIPS.tips_desc_head',
                'TCT_TIPS.tips_desc_body',
                'TCT_TIPS.updated_at',
                'TSM_CODE.code')
                ->leftJoin('TSM_CODE','TSM_CODE.code','=','TCT_TIPS.tips_cl_code')
                ->orderBy('ti_id', 'desc')
                ->where('display_yn', 'Y')
                ->skip((\Request::input('page') - 1) * 10)
                ->limit(10)->get();
                if (isset($params['slug']) && $params['slug'] != 'all') {
                    $model = Tips::select('TCT_TIPS.ti_id',
                    'TCT_TIPS.subject',
                    'TCT_TIPS.img_url',
                    'TCT_TIPS.cover_img1',
                    'TCT_TIPS.cover_img2',
                    'TCT_TIPS.cover_img3',
                    'TCT_TIPS.cover_img4',
                    'TCT_TIPS.created_at',
                    'TCT_TIPS.tips_desc_head',
                    'TCT_TIPS.tips_desc_body',
                    'TCT_TIPS.updated_at',
                    'TSM_CODE.code')
                    ->leftJoin('TSM_CODE','TSM_CODE.code','=','TCT_TIPS.tips_cl_code')
                    ->orderBy('ti_id', 'desc')
                    ->where('display_yn', 'Y')
                    ->where('TSM_CODE.slug', '=', $params['slug'])
                    ->paginate(10);
                }
        } else {
            // select tip in home page
            $model = Tips::select('TCT_TIPS.subject','TCT_TIPS.ti_id','TCT_TIPS.cont1_body','TCT_TIPS.cont1_photo','TCT_TIPS.img_url','cover_img1','tips_desc_body')
                ->orderBy('ti_id', 'desc')
                ->where('display_yn', 'Y')->limit(1)->get();
        }
        return $model;
    }
}
