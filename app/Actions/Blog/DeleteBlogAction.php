<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Blog;
use App\Contracts\BlogRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class DeleteBlogAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($ids) {
        try {
            foreach ($ids as $id) {
                $this->repository->delete($id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
