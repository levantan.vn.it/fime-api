<?php

namespace App\Actions\Blog;
use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use App\Criterias\Blog\GetTopViewBlogCriteria;

class GetTopViewAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($request) {
        try {
            $blogs = $this->repository->pushCriteria(new GetTopViewBlogCriteria())->paginate(3);
            return $blogs;
        } catch (\Exception $e) {
            \Log::error($e);
            return [];
        }
    }
}
