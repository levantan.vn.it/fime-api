<?php

namespace App\Actions\Blog;
use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use Mockery\Exception;

class GetByAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
            $blog = $this->repository->find($id);
            return $blog;
        } catch (Exception $e) {
            return $e;
        }
    }
}
