<?php

namespace App\Actions\Ad;
use App\Actions\Action;
use App\Contracts\AdsRepositoryInterface;
use Mockery\Exception;

class UpdateAdAction extends Action {
    protected $ad_repository;

    public function __construct(AdsRepositoryInterface $ad_repository) {
        $this->ad_repository = $ad_repository;
    }

    public function run($data) {
        try {
            $ad = $this->ad_repository->update([
                'name' => $data['name'],
                'description' => $data['description'],
                'resource_type' => $data['resource_type'],
                'show_on_frontend' => $data['show_on_frontend'],
                'url' => $data['url'],
                'target_url' => $data['target_url'],
                'target_type' => $data['target_type'],
                'order' => $data['order']
            ], $data['id']);
            return $ad;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
