<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;

class GetAllAdsAction extends Action {
    protected $ads_repository;

    public function __construct(AdsRepositoryInterface $ads_repository) {
        $this->ads_repository = $ads_repository;
    }

    public function run($params) {
    	if(!empty($params['pageSize'])){
    		$ads = $this->ads_repository->paginate($params['pageSize']);
    	}else{
    		$ads = $this->ads_repository->all(['description','name','order','target_type','resource_type','show_on_frontend','target_url','url','id','is_disabled']);
    	}
        
        return $ads;
    }
}
