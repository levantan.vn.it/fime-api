<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class ToggleAdAction extends Action {
    protected $ad_repository;

    public function __construct(AdsRepositoryInterface $ad_repository) {
        $this->ad_repository = $ad_repository;
    }

    public function run($id) {
        try {
            $ad = $this->ad_repository->find($id);

            $result = $this->ad_repository->update([
                'is_disabled' => !$ad->is_disabled
            ], $id);

            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
