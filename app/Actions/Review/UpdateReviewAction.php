<?php

namespace App\Actions\Review;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\FilesRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Criterias\Files\GetReviewFilesCriteria;
use Carbon\Carbon;
use Mockery\Exception;

class UpdateReviewAction extends Action
{
    protected $repository;
    protected $hashtag_repository;
    protected $files_repository;
    protected $review_files_repository;
    protected $reviewHashTagsRepository;

    public function __construct(ReviewRepositoryInterface $repository,
                                HashtagRepositoryInterface $hashtag_repository,
                                FilesRepositoryInterface $files_repository,
                                ReviewFilesRepositoryInterface $review_files_repository,
                                ReviewHashTagsRepositoryInterface $reviewHashTagsRepository
    )
    {
        $this->repository = $repository;
        $this->hashtag_repository = $hashtag_repository;
        $this->files_repository = $files_repository;
        $this->review_files_repository = $review_files_repository;
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
    }

    public function run($data)
    {
        try {
            $review = $this->repository->find($data['review_no']);
            // if ($review->user_no != auth()->id()) {
            //     return false;
            // }

            $old_hash_tags = [];
            preg_match_all('/#([A-Za-z-_!?.@%^&*$0-9]+)/', $review->review_dc, $old_hash_tags);

            $result = $this->repository->update([
                'goods_nm' => isset($data['goods_nm']) ? $data['goods_nm'] : null,
                'goods_cl_code' => isset($data['goods_cl_code']) ? $data['goods_cl_code'] : null,
                'goods_cl_code_brand' => isset($data['goods_cl_code_brand']) ? $data['goods_cl_code_brand'] : null,
                'review_dc' => isset($data['review_dc']) ? $data['review_dc'] : null,
                'review_short' => isset($data['review_short']) ? $data['review_short'] : null,
            ], $data['review_no']);

            // $hashtags = [];
            // preg_match_all('/#([A-Za-z-_!?.@%^&*$0-9]+)/', $data['review_dc'], $hashtags);
            $hashtags = [];
            if(!empty($data['hash_tag'])){
                preg_match_all('/#([A-Za-z-_!?.@%^&*$0-9]+)/', $data['hash_tag'], $hashtags);
            } else{
                preg_match_all('/#([A-Za-z-_!?.@%^&*$0-9]+)/', $review['review_dc'], $hashtags);
            }


            if (!empty($hashtags) && count($hashtags) > 1) {
                $hashtags = $hashtags[0];
            }

            if (count($old_hash_tags) > 1) {
                $hashtags = array_merge($hashtags, $old_hash_tags[0]);
            }

            $hashtags = array_unique($hashtags);


            $this->reviewHashTagsRepository->deleteWhere([
                'review_no' => $result->review_no,
            ]);

            if (count($hashtags)) {
                foreach ($hashtags as $hashtag) {
                    $hashtag = strtoupper($hashtag);

                    $existing_tag = $this->hashtag_repository->findWhere([
                        'hash_tag' => $hashtag
                    ])->first();

                    if ($existing_tag) {
                        if(!empty($data['hash_tag'])){
                            $total = $this->hashtag_repository->count([
                                ['hash_tag', 'like', '%' . $hashtag . '%']
                            ]);
                        }
                        if ($total == 0) {
                            $this->hashtag_repository->delete($existing_tag->hash_seq);
                        } else {
                            $this->hashtag_repository->update([
                                'hash_cnt' => $total
                            ], $existing_tag->hash_seq);
                            $this->updateReviewHashTagTable($result->review_no, $existing_tag->hash_seq);
                        }
                    } else {
                        $hashtagRecord = $this->hashtag_repository->create([
                            'hash_cnt' => 1,
                            'hash_tag' => $hashtag,
                            'hash_dt' => Carbon::now()
                        ]);
                        $this->updateReviewHashTagTable($result->review_no, $hashtagRecord->hash_seq);
                    }
                }
            }

            if (!empty($data['images'])) {
                $this->review_files_repository->deleteWhere([
                    'review_no' => $result->review_no
                ]);

                foreach ($data['images'] as $index => $image) {

                    $this->review_files_repository->create([
                        'file_se_code' => Constant::$FILE_SE_CODE,
                        'file_sn' => $index + 1,
                        'orginl_file_nm' => $image['name'],
                        'stre_file_nm' => $image['name'],
                        'thumb_file_nm' => isset($image['thumb_name']) ? $image['thumb_name'] : null,
                        'thumb_file_nm2' => $image['name'],
                        'file_cours' => $image['url'],
                        'se_code' => Constant::$OTHER_SE_CODE,
                        'review_no' => $result->review_no
                    ]);
                }
            } else {
                $this->review_files_repository->deleteWhere([
                    'review_no' => $result->review_no
                ]);
            }

            $images = $this->review_files_repository->getByCriteria(new GetReviewFilesCriteria($result->review_no));
            $result->files = $images;

            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function updateReviewHashTagTable($review_no, $hash_seq)
    {
        $count = $this->reviewHashTagsRepository->count([
            'review_no' => $review_no,
            'hash_seq' => $hash_seq
        ]);

        if ($count === 0) {
            $this->reviewHashTagsRepository->create([
                'review_no' => $review_no,
                'hash_seq' => $hash_seq
            ]);
        }
    }
}
