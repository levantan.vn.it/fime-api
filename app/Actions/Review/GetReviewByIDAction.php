<?php

namespace App\Actions\Review;
use App\Actions\Action;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Criterias\Files\GetReviewFilesCriteria;



class GetReviewByIDAction extends Action {
    protected $review_repository;
    private $review_files_repository;


    public function __construct(ReviewRepositoryInterface $review_repository,
    ReviewFilesRepositoryInterface $review_files_repository) {
        $this->review_repository = $review_repository;
        $this->review_files_repository = $review_files_repository;

    }

    public function run($id) {
        try {
            $review = $this->review_repository->find($id);
            $this->getFiles($review);

            return $review;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
    protected function getFiles($review) {
        try {
            $files = $this->review_files_repository->getByCriteria(new GetReviewFilesCriteria($review->review_no));
            $review->files = $files;
            return $review;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
