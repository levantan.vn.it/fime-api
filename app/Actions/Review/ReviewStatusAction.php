<?php

namespace App\Actions\Review;

use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\UserTry\GetWinnersByTryIdCriteria;
use App\Actions\Action;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class ReviewStatusAction extends Action
{
    protected $review_repository;

    public function __construct(UserRepositoryInterface $user_repository,
                                ReviewRepositoryInterface $review_repository)
    {
        $this->user_repository = $user_repository;
        $this->review_repository = $review_repository;
    }

    public function run($data)
    {
        try {
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
