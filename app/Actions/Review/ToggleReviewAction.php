<?php

namespace App\Actions\Review;
use App\Actions\Action;
use App\Repositories\ReviewRepository;
use Mockery\Exception;

class ToggleReviewAction extends Action {
    protected $review_repository;

    public function __construct(ReviewRepository $review_repository) {
        $this->review_repository = $review_repository;
    }

    public function run($ids, $toggle) {
        try {
            $reviews = $this->review_repository->findWhereIn('review_no', $ids);

            foreach ($reviews as $review) {
                if($toggle == false) {
                    $this->review_repository->update([
                        'expsr_at' => 'N'
                    ], $review->review_no);
                } else {
                    $this->review_repository->update([
                        'expsr_at' => 'Y'
                    ], $review->review_no);
                }
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
