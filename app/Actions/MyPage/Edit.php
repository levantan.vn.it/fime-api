<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/20/2019
 * Time: 3:17 PM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Edit extends Action
{
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function run($data)
    {
        $user = $data['user'];

        if ($user['pic'] !== Auth::user()->avatar) {
            $file_data = $user['pic'];

            @list($type, $file_data) = explode(';base64,', $file_data);
            if (!empty($file_data)) {
                @list(, $extension) = explode('/', $type);
                $randomName = $this->generateRandomString();
                $file_name =  $randomName. '.' . $extension;
                // save file image
                \Storage::disk('data')->put('images/profile/' . $file_name, base64_decode($file_data));
                try {
                    $new_url = public_path('/data/images/profile/'.$file_name);
                    $command = "convert ".$new_url;
                    if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                        $command .= ' -strip ';
                    }else{
                        $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                    }
                    $command .= $file_name;
                    exec($command);
                    $image = \Image::make($file_data)->resize(400, null,function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $image->save((public_path().'/data/images/profile/'.$file_name));

                    $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                    $image_thumb = \Image::make($file_data)->resize(150, null,function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $image_thumb->save((public_path().'/data/images/profile/'.$file_thumb_name));
                    $user['update_pic'] = '/data/images/profile/' . $file_thumb_name;
                }catch (\Intervention \ Image \ Exception \ NotReadableException $e) {
                    $user['update_pic'] = '/data/images/profile/' . $file_name;
                }catch (\Intervention \ Image \ Exception \ NotSupportedException $e) {
                    $user['update_pic'] = '/data/images/profile/' . $file_name;
                }
                $user['pic'] = '/data/images/profile/' . $file_name;
            }
        }

        $rs = $this->userRepository->update([
            'home_addr1' => $user['home_addr1'],
            'pic' => $user['pic'],
            'email' => $user['email'],
            'cellphone' => $user['cellphone'],
            'self_intro' => $user['self_intro'],
            'updated_at' => Carbon::now()
        ], Auth::user()->user_no);


        return $rs;
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function is_base64_encoded($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
