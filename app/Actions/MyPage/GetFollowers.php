<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:46 AM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\Users\GetUsersByIdsCriteria;
use App\Criterias\Users\GetUsersFollowCriteria;
use App\Criterias\Users\GetUsersFollowingCriteria;

class GetFollowers extends Action
{
    protected $userFollowRepository;
    protected $userRepository;

    public function __construct(UserFollowRepositoryInterface $userFollowRepository, UserRepositoryInterface $userRepository)
    {
        $this->userFollowRepository = $userFollowRepository;
        $this->userRepository = $userRepository;
    }

    public function run($request)
    {
        if (isset($request['user_no']))
            $userId = (string)$request['user_no'];
        else return [];

        $followUsers = $this->userFollowRepository->pushCriteria(new GetUsersFollowCriteria($userId))->paginate(10);

        $followIds = $followUsers->pluck('user_id')->toArray();


        $users = $this->userRepository->getByCriteria(new GetUsersByIdsCriteria($followIds));

        $user_ids = $users->pluck('user_no')->toArray();

        $followers = $this->userFollowRepository->skipCriteria()->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');


        $followings = $this->userFollowRepository->skipCriteria()->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');


        $current_user_id = auth()->id();

        if($current_user_id) {
            $current_followings = $this->userFollowRepository->skipCriteria()->getByCriteria(
                new GetUsersFollowingCriteria($current_user_id, $user_ids)
            )->keyBy('user_id');
        }

        foreach ($users as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->user_no])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if ($current_user_id && isset($current_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }


        $data = $followUsers->toArray();
        $data['data'] = $users;
        return $data;
    }
}
