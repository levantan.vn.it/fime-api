<?php

namespace App\Actions\Comment;
use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;

use Mockery\Exception;

class ToggleCommentAction extends Action {
    protected $comment_repository;
    protected $review_comment_repository;

    public function __construct(CommentRepositoryInterface $comment_repository,
                                ReviewCommentRepositoryInterface $review_comment_repository) {
        $this->comment_repository = $comment_repository;
        $this->review_comment_repository = $review_comment_repository;
    }

    public function run($id, $type) {
        try {
            if($type == 'try') {
                $comment = $this->comment_repository->find($id);

                $result = $this->comment_repository->update([
                    'expsr_at' => $comment->expsr_at == 'Y' ? 'N' : 'Y'
                ], $id);
            } else {
                $comment = $this->review_comment_repository->find($id);

                $result = $this->review_comment_repository->update([
                    'expsr_at' => $comment->expsr_at == 'Y' ? 'N' : 'Y'
                ], $id);
            }

            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
