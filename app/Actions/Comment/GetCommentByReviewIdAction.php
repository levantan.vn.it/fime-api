<?php

namespace App\Actions\Comment;
use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Criterias\Comment\GetAllCommentsByReviewIdCriteria;
use App\Criterias\Comment\GetAllCommentsByTryIdCriteria;

class GetCommentByReviewIdAction extends Action {
    protected $comment_repository;
    protected $review_comment_repository;

    public function __construct(CommentRepositoryInterface $comment_repository,
                                ReviewCommentRepositoryInterface $review_comment_repository) {
        $this->comment_repository = $comment_repository;
        $this->review_comment_repository = $review_comment_repository;
    }

    public function run($id, $type) {
        if($type == 'try') {
            $comments = $this->comment_repository->pushCriteria(new GetAllCommentsByTryIdCriteria($id))->paginate(10);
        } else {
            $comments = $this->review_comment_repository->pushCriteria(new GetAllCommentsByReviewIdCriteria($id))->paginate(10);
        }
        return $comments;
    }
}
