<?php

namespace App\Actions\Comment;
use App\Contracts\BannerRepositoryInterface;
use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;

class GetCommentByIdAction extends Action {
    protected $comment_repository;

    public function __construct(CommentRepositoryInterface $comment_repository) {
        $this->comment_repository = $comment_repository;
    }

    public function run($id) {
        $comment = $this->comment_repository->find($id);
        return $comment;
    }
}
