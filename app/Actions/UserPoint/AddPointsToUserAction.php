<?php

namespace App\Actions\UserPoint;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use Mockery\Exception;

class AddPointsToUserAction extends Action
{
    private $user_point_repository;
    private $codeRepository;

    /*
     * Joined
     */
    protected function joined($user_no)
    {
        try {
            $data = [];
            $data['accml_dt'] = date('Y-m-d');
            $data['user_no'] = $user_no;
            $data['accml_pntt'] = $this->getPoint(Constant::$JOIN_CODE);
            $data['pntt_accml_code'] = Constant::$JOIN_CODE;
            return $this->user_point_repository->create($data);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /*
     * Try Applied
     */
    protected function tryApplied($user_no, $cntnts_no)
    {
        try {
            $data = [];
            $data['accml_dt'] = date('Y-m-d');
            $data['user_no'] = $user_no;
            $data['cntnts_no'] = $cntnts_no;
            $data['accml_pntt'] = $this->getPoint(Constant::$TRY_APPLIED_CODE);
            $data['pntt_accml_code'] = Constant::$TRY_APPLIED_CODE;
            return $this->user_point_repository->create($data);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /*
     * Try winner
     */

    protected function tryWinner($user_no, $cntnts_no, $selected)
    {
        try {
            $data = [];
            $data['accml_dt'] = date('Y-m-d');
            $data['user_no'] = $user_no;
            $data['cntnts_no'] = $cntnts_no;
            $data['accml_pntt'] = $selected ? $this->getPoint(Constant::$TRY_WINNER_CODE) : -1 * ($this->getPoint(Constant::$TRY_WINNER_CODE));
            $data['pntt_accml_code'] = Constant::$TRY_WINNER_CODE;
            return $this->user_point_repository->create($data);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /*
     * Try review written
     */

    protected function tryReviewWinner($user_no, $cntnts_no, $review_no, $deleted)
    {
        try {
            $data = [];
            $data['accml_dt'] = date('Y-m-d');
            $data['user_no'] = $user_no;
            $data['cntnts_no'] = $cntnts_no;
            $data['review_no'] = $review_no;
            $data['accml_pntt'] = !$deleted ? $this->getPoint(Constant::$TRY_REVIEW_WINNER_CODE) : -1 * ($this->getPoint(Constant::$TRY_REVIEW_WINNER_CODE));
            $data['pntt_accml_code'] = Constant::$TRY_REVIEW_WINNER_CODE;
            return $this->user_point_repository->create($data);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /*
     * Review written
     */
    protected function writeReview($user_no, $review_no, $deleted)
    {
        try {
            $data = [];
            $data['accml_dt'] = date('Y-m-d');
            $data['user_no'] = $user_no;
            $data['review_no'] = $review_no;
            $data['accml_pntt'] = !$deleted ? $this->getPoint(Constant::$WRITE_REVIEW_CODE) : -1 * ($this->getPoint(Constant::$WRITE_REVIEW_CODE));
            $data['pntt_accml_code'] = Constant::$WRITE_REVIEW_CODE;
            return $this->user_point_repository->create($data);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /*
     * Reply written
     */
    public function comment($type, $id, $user_no, $delete)
    {
        $data = [];
        $data['accml_dt'] = date('Y-m-d');
        $data['user_no'] = $user_no;
        switch ($type) {
            case 'review':
                $point = $this->getPoint(Constant::$COMMENT_REVIEW_CODE);
                $data['review_no'] = $id;
                $data['accml_pntt'] = $delete ? -1 * abs($point) : $point;
                $data['pntt_accml_code'] = Constant::$COMMENT_REVIEW_CODE;
                break;
            case 'try':
                $point = $this->getPoint(Constant::$COMMENT_REVIEW_CODE);
                $data['cntnts_no'] = $id;
                $data['accml_pntt'] = $delete ? -1 * abs($point) : $point;
                $data['pntt_accml_code'] = Constant::$COMMENT_REVIEW_CODE;
                break;
        }
        return $this->user_point_repository->create($data);
    }

    /*
     * Like to try
     */
    protected function likeTry($user_no, $cntnts_no, $is_liking)
    {
        try {
            return $this->like('try', $cntnts_no, $user_no, $is_liking);
        } catch (Exception $e) {
            \Log::error($e);
            return false;
        }
    }

    /*
     * Like to review
     */
    protected function likeReview($user_no, $review_no, $is_liking)
    {
        try {
            return $this->like('review', $review_no, $user_no, $is_liking);
        } catch (Exception $e) {
            \Log::error($e);
            return false;
        }
    }


    protected function like($type, $id, $user_no, $is_liking)
    {
        $data = [];
        $data['accml_dt'] = date('Y-m-d');
        $data['user_no'] = $user_no;
        switch ($type) {
            case 'review':
                $point = $this->getPoint(Constant::$LIKE_REVIEW_CODE);
                $data['review_no'] = $id;
                $data['accml_pntt'] = $is_liking ? $point : -1 * abs($point);
                $data['pntt_accml_code'] = Constant::$LIKE_REVIEW_CODE;
                break;
            case 'try':
                $point = $this->getPoint(Constant::$LIKE_TRY_CODE);
                $data['cntnts_no'] = $id;
                $data['accml_pntt'] = $is_liking ? $point : -1 * abs($point);
                $data['pntt_accml_code'] = Constant::$LIKE_TRY_CODE;
                break;
        }
        return $this->user_point_repository->create($data);
    }


    public function __construct(
        UserPointRepositoryInterface $user_point_repository,
        CodeRepositoryInterface $codeRepository)
    {
        $this->user_point_repository = $user_point_repository;
        $this->codeRepository = $codeRepository;
    }


    /**
     * @param $code
     * @return int
     */
    private function getPoint($code)
    {
        $point = 0;

        try {
            $points = $this->codeRepository->findWhere([
                'code_group' => Constant::$POINT_GROUP_CODE,
                'code' => $code
            ], ['refrn_info'])->toArray();

            if (!empty($points)) {
                $point = $points[0]['refrn_info'];
            }

            return $point;
        } catch (Exception $e) {
            return 0;
        }
    }

}
