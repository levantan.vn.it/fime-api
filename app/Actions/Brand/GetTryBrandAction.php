<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;

use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Comment\GetCommentNumberByTryIdsCriteria;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\TryFree\GetTriesCriteria;
use App\Criterias\TryFree\GetWinnersInfoCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetLikesOfCurrentUserCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\UserTry\GetApplyOfCurrentUserCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use Carbon\Carbon;
use App\Models\TryFree;


class GetTryBrandAction extends Action
{
    protected $repository;
    private $user_like_repository;
    private $comment_repository;
    private $user_repository;
    protected $user_try_repository;
    protected $followRepository;

    public function __construct(TryRepositoryInterface $repository,
                                CommentRepositoryInterface $comment_repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                UserRepositoryInterface $user_repository,
                                UserFollowRepositoryInterface $followRepository)
    {
        $this->repository = $repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_repository = $user_repository;
        $this->comment_repository = $comment_repository;
        $this->user_try_repository = $user_try_repository;
        $this->followRepository = $followRepository;
    }

    public function run($id)
    {
        $current_user_id = auth()->id();

        if ($current_user_id) {
            $this->user_repository->update([
                'try_click_dt' => Carbon::now()
            ], $current_user_id);
        }
        $tries_brand = TryFree::select('TCT_GOODS.cntnts_no',
        'TCT_GOODS.link_url',
        'TCT_GOODS.modl_nombr',
        'TCT_GOODS.is_try_event',
        'TCT_GOODS.try_event_type',
        'TCT_GOODS.view_cnt',
        'TCT_GOODS.brnd_nm',
        'TCT_GOODS.brnd_code',
        'TCT_GOODS.event_knd_code',
        'TCT_GOODS.event_bgnde',
        'TCT_GOODS.event_endde',
        'TCT_GOODS.dlvy_bgnde',
        'TCT_GOODS.dlvy_endde',
        'TCT_GOODS.event_trgter_co',
        'TCT_GOODS.event_pc',
        'TCT_GOODS.time_color_code',
        'TCT_GOODS.slctn_compt_at',
        'TCT_GOODS.border_at',
        'TCT_GOODS.hash_tag',
        'TCT_GOODS.goods_cl_code',
        'TCT_GOODS.goods_txt_code',
        'TCT_GOODS.goods_txt',
        'TCT_GOODS.p_cnt',
        'TCT_GOODS.m_cnt',
        'TCT_GOODS.short_desc',
        'TCT_GOODS.quantity_to_qualify',
        'TCT_GOODS.resource_type',
        'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug')
        ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
        ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
        ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
        ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
        ->where('brnd_code', $id)->where('event_knd_code', '=', '398002')
        ->orderBy('event_endde','desc')
        ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
        ->skip((\Request::input('page') - 1) * 9)->limit(9)->get();

        $this->decorateData($tries_brand);

        return $tries_brand;
    }

    public function decorateData($tries)
    {
        $this->getTotalApply($tries);
        $this->getNumberLikesOfTry($tries);
        $this->getNumberCommentsOfTry($tries);
        $this->getListUserLike($tries);
        $this->checkCurrentUserLiked($tries);
        $this->checkJoinTry($tries);
        $this->checkWinner($tries);
        $this->getFiles($tries);
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->repository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    protected function getTotalApply($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->total_apply = 0;
            if (isset($number_of_tries[$try->cntnts_no])) {
                $try->total_apply = $number_of_tries[$try->cntnts_no]->number_of_tries;
            }
        }
        return $tries;
    }

    protected function getNumberLikesOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($try_ids));

        $list_try_likes = collect($list_try_likes)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_likes[$try->cntnts_no])) {
                $tries[$index]->likes = $list_try_likes[$try->cntnts_no]->like_number;
            } else {
                $tries[$index]->likes = 0;
            }
        }

        return $tries;
    }

    protected function getNumberCommentsOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_comments = $this->comment_repository->getByCriteria(new GetCommentNumberByTryIdsCriteria($try_ids));

        $list_try_comments = collect($list_try_comments)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_comments[$try->cntnts_no])) {
                $tries[$index]->comments = $list_try_comments[$try->cntnts_no]->comment_number;
            } else {
                $tries[$index]->comments = 0;
            }
        }

        return $tries;
    }

    protected function getListUserLike($tries)
    {
        foreach ($tries as $index => $try) {
            $list_user_liked = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($try->cntnts_no));
//            $fimers = $this->getCurrentUserFollowing($list_user_liked);
            $tries[$index]->users_liked = $list_user_liked;
        }

        return $tries;
    }

    protected function checkCurrentUserLiked($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_liked = $this->user_like_repository->getByCriteria(new GetLikesOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_liked = $list_user_liked->keyBy('cntnts_no');

        foreach ($tries as $item) {
            if (isset($list_user_liked[$item->cntnts_no])) {
                $item->is_liked = 1;
            } else {
                $item->is_liked = 0;
            }
        }
        return $tries;
    }

    protected function checkJoinTry($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_apply = $this->user_try_repository->getByCriteria(new GetApplyOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_apply = $list_user_apply->keyBy('cntnts_no');

        foreach ($tries as $index => $item) {
            if (isset($list_user_apply[$item->cntnts_no])) {
                $tries[$index]->is_joined = 1;
            } else {
                $tries[$index]->is_joined = 0;
            }
        }
        return $tries;
    }

    protected function checkWinner($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id) {
            foreach ($tries as $index => $item) {
                $tries[$index]->slctn_dt = null;
                $tries[$index]->dlvy_dt = null;
            }
            return null;
        }

        $list_winners = $this->user_try_repository->getByCriteria(new GetWinnersInfoCriteria($list_try_id, $current_user_id));
        $list_winners = $list_winners->keyBy('cntnts_no');

        foreach ($tries as $index => $item) {
            if (isset($list_winners[$item->cntnts_no])) {
                $tries[$index]->slctn_dt = $list_winners[$item->cntnts_no]->slctn_dt;
                $tries[$index]->dlvy_dt = $list_winners[$item->cntnts_no]->dlvy_dt;
            } else {
                $tries[$index]->slctn_dt = null;
                $tries[$index]->dlvy_dt = null;
            }
        }
        return $tries;
    }
}
