<?php

namespace App\Actions\Brand;

use App\Actions\Action;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Criterias\Comment\GetNumberOfCommentsByReviewIdsCriteria;
use App\Criterias\Files\GetReviewFilesCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\UserLike\GetReviewLikesByUserIdCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;

use App\Models\Review;

class GetReviewBrandAction extends Action
{
    protected $review_repository;
    private $user_follow_repository;
    private $user_like_repository;
    private $comment_repository;
    private $review_files_repository;

    /**
     * GetReviewByTypeAction constructor.
     * @param ReviewRepositoryInterface $review_repository
     * @param UserFollowRepositoryInterface $user_follow_repository
     * @param ReviewLikeRepositoryInterface $user_like_repository
     * @param ReviewCommentRepositoryInterface $comment_repository
     * @param ReviewFilesRepositoryInterface $review_files_repository
     */
    public function __construct(
        ReviewRepositoryInterface $review_repository,
        UserFollowRepositoryInterface $user_follow_repository,
        ReviewLikeRepositoryInterface $user_like_repository,
        ReviewCommentRepositoryInterface $comment_repository,
        ReviewFilesRepositoryInterface $review_files_repository
    ) {
        $this->review_repository = $review_repository;
        $this->user_follow_repository = $user_follow_repository;
        $this->user_like_repository = $user_like_repository;
        $this->comment_repository = $comment_repository;
        $this->review_files_repository = $review_files_repository;
    }

    /**
     * @param $reviews
     * @return mixed
     */


    public function run($id)
    {
        $reviews_brand = Review::select(
            'TCT_REVIEW.cntnts_no',
            'TCT_REVIEW.updt_dt',
            'TCT_REVIEW.delete_at',
            'TCT_REVIEW.expsr_at',
            'TCT_REVIEW.pblonsip_at',
            'TCT_REVIEW.pblonsip_snts',
            'TCT_REVIEW.pblonsip_time',
            'TCT_REVIEW.review_no',
            'TCT_REVIEW.user_no',
            'TCT_REVIEW.goods_cl_code',
            'TCT_REVIEW.goods_nm',
            'TCT_REVIEW.m_cnt',
            'TCT_REVIEW.p_cnt',
            'TCT_REVIEW.slug',
            'TCT_REVIEW.review_short',
            'TCT_REVIEW.writng_dt',
            'TDM_USER.reg_name as author_name',
            'TDM_USER.id as author_ds',
            'TDM_USER.user_no as author_id',
            'TDM_USER.pic as author_avatar',
            'TDM_USER.slug as author_slug'
        )
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('goods_cl_code_brand', $id)
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc')
            ->skip((\Request::input('page') - 1) * 9)->limit(9)
            ->get();
        $this->decorateData($reviews_brand);

        return $reviews_brand;
    }

    protected function decorateData($reviews)
    {
        $this->getFollowInfo($reviews);
        $this->checkCurrentUserFollowingReviewAuthor($reviews);
        $this->getLikesNumber($reviews);
        $this->checkCurrentUserLikedReview($reviews);
        $this->getCommentNumberOfReview($reviews);
        $this->getFiles($reviews);
        return $reviews;
    }


    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->user_follow_repository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id)
        )->keyBy('followed_user_id');


        $followers = $this->user_follow_repository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->user_follow_repository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getFollowInfo($reviews)
    {
        try {
            $user_ids = $reviews->pluck('user_no')->toArray();

            $list_number_follow = $this->user_follow_repository->getByCriteria(new GetNumberOfFollowersByUserIdsCriteria($user_ids));

            $list_number_follow = collect($list_number_follow)->keyBy('user_id');

            foreach ($reviews as $index => $review) {
                if (isset($list_number_follow[$review->user_no])) {
                    $review->author_follows = $list_number_follow[$review->user_no]->number_of_followers;
                } else {
                    $review->author_follows = 0;
                }
            }

            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function checkCurrentUserFollowingReviewAuthor($reviews)
    {
        $current_user_id = auth()->id();

        $user_ids = $reviews->pluck('user_no')->toArray();

        $user_followings = $this->user_follow_repository->getByCriteria(new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        foreach ($reviews as $index => $review) {
            $review->followed = 0;
            if (isset($user_followings[$review->user_no])) {
                $review->followed = 1;
            }
        }
        return $reviews;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getLikesNumber($reviews)
    {
        try {
            $review_ids = $reviews->pluck('review_no')->toArray();

            $list_like_number = $this->user_like_repository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria($review_ids));

            $list_number_follow = collect($list_like_number)->keyBy('object_id');

            foreach ($reviews as $review) {
                if (isset($list_number_follow[$review->review_no])) {
                    $review->like_number = $list_number_follow[$review->review_no]->like_number;
                } else {
                    $review->like_number = 0;
                }
            }
            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function checkCurrentUserLikedReview($reviews)
    {
        $current_user_id = auth()->id();

        $review_ids = $reviews->pluck('review_no')->toArray();

        $list_user_liked = $this->user_like_repository->getByCriteria(new GetReviewLikesByUserIdCriteria($review_ids, $current_user_id));

        $list_user_liked = $list_user_liked->keyBy('review_no');

        foreach ($reviews as $review) {
            if (isset($list_user_liked[$review->review_no])) {
                $review->is_liked = 1;
            } else {
                $review->is_liked = 0;
            }
        }

        return $reviews;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getCommentNumberOfReview($reviews)
    {
        try {
            $review_ids = $reviews->pluck('review_no')->toArray();

            $list_comment_number = $this->comment_repository->getByCriteria(new GetNumberOfCommentsByReviewIdsCriteria($review_ids));

            $list_comment_number = collect($list_comment_number)->keyBy('review_no');

            foreach ($reviews as $review) {
                if (isset($list_comment_number[$review->review_no])) {
                    $review->comment_number = $list_comment_number[$review->review_no]->comment_number;
                } else {
                    $review->comment_number = 0;
                }
            }
            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getFiles($reviews)
    {

        $review_ids = $reviews->pluck('review_no')->toArray();
        $reviewsMainImage = $this->review_files_repository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');
        foreach ($reviews as $review) {

            if (isset($reviewsMainImage[$review->review_no]))
                $review->main_image = $reviewsMainImage[$review->review_no];
        }
        return $reviews;
    }
}
