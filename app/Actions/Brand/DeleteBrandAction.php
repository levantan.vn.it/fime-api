<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;
use App\Contracts\BrandRepositoryInterface;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class DeleteBrandAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
            $brand = $this->repository->delete($id);
            return $brand;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
