<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Categories;

use App\Actions\Action;
use App\Contracts\CodeGroupRepositoryInterface;
use Mockery\Exception;



class UpdateCategoriesAction extends Action
{
    protected $repository;

    public function __construct(CodeGroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $category = $this->repository->update([
                'code_group_eng_nm' => $data['code_group_eng_nm']
            ], $data['code_group']);
            return $category;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
