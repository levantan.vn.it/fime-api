<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Categories;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\CodeRepositoryInterface;

class GetAllCategoriesAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($params) {
      $params['code_group']= Constant::$FASHION_GROUP_CODE;
        if(!empty($params['pageSize'])){
            
            $categories = $this->repository->scopeQuery(function($query) use ($params){
                $query = $query->where('code_group', $params['code_group']);
                $query = $query->where('use_at', '=', 'Y');
                return $query;
            })->paginate($params['pageSize'],['code','code_group','code_nm','sort_ordr','use_at','slug','additional_inactive','additional']);
        }else{
          $categories = $this->repository->scopeQuery(function($query) use ($params){
              $query = $query->where('code_group', $params['code_group']);
              $query = $query->where('use_at', '=', 'Y');
              return $query;
          })->get(['code','code_group','code_nm','sort_ordr','use_at','slug','additional_inactive','additional']);
        }
        
       return $categories;
    }
}
