<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 1:11 PM
 */

namespace App\Export;


use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class Winner implements FromArray, WithEvents, WithTitle, WithCustomStartCell
{
    use Exportable;

    /** @var array */
    public $users;

    public $try;

    /** @var int */
    public $startRow = 7;

    public $length = 0;

    /** @var string */
    public $title;

    /**
     * Winner constructor.
     * @param $users
     * @param $try
     * @param string $title
     */
    public function __construct($users, $try, string $title)
    {
        $this->users = $users;
        $this->try = $try;
        $this->length = count($users);
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        // TODO: Implement array() method.
        return $this->users;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // Set heading
                $sheet = $event->sheet->getDelegate();
                $sheet->getCell('A2')->setValue('Product name: ' . $this->try->sj)->getStyle()->getFont()->setSize(14)->setBold(true);
                $sheet->getCell('A3')->setValue('Max-Participants: ' . $this->try->event_trgter_co)->getStyle()->getFont()->setSize(11)->setBold(true)->setItalic(true);
                $sheet->getCell('A4')->setValue('Period: ' . Carbon::parse($this->try->event_bgnde)
                        ->format('Y-m-d') . ' to ' . Carbon::parse($this->try->event_endde)->format('Y-m-d'))->getStyle()->getFont()->setSize(11)->setBold(true)->setItalic(true);

                $sheet->getCell('A' . ($this->startRow - 1))->setValue('ID');
                $sheet->getCell('B' . ($this->startRow - 1))->setValue('Name');
                $sheet->getCell('C' . ($this->startRow - 1))->setValue('Email');
                $sheet->getCell('D' . ($this->startRow - 1))->setValue('Phone');
                $sheet->getCell('E' . ($this->startRow - 1))->setValue('Address');
                $sheet->getCell('F' . ($this->startRow - 1))->setValue('Registerd Date');
                $sheet->getCell('G' . ($this->startRow - 1))->setValue('Apply Date');
                $sheet->getCell('H' . ($this->startRow - 1))->setValue('Number Of Selected');
                $sheet->getCell('I' . ($this->startRow - 1))->setValue('Number Of Likes');
                $sheet->getCell('J' . ($this->startRow - 1))->setValue('Number Of Follower');
                $sheet->getCell('K' . ($this->startRow - 1))->setValue('Number Of Reviews');
                $sheet->getCell('L' . ($this->startRow - 1))->setValue('Total point');
                $sheet->getCell('M' . ($this->startRow - 1))->setValue('Is Select');
                $sheet->getCell('N' . ($this->startRow - 1))->setValue('Is Delivery');
                $sheet->getCell('O' . ($this->startRow - 1))->setValue('Delivery Date');


                $sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(20);
                $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(20);
                $sheet->getColumnDimension('F')->setAutoSize(true);
                $sheet->getColumnDimension('G')->setAutoSize(true);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);
                $sheet->getColumnDimension('J')->setAutoSize(true);
                $sheet->getColumnDimension('K')->setAutoSize(true);
                $sheet->getColumnDimension('L')->setAutoSize(true);
                $sheet->getColumnDimension('M')->setAutoSize(true);
                $sheet->getColumnDimension('N')->setAutoSize(true);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'N' . ($this->startRow - 1))->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'N' . ($this->startRow - 1))->getFill()->setFillType('solid')->getStartColor()->setARGB('366092');

                $horizontal_left = array(
                    'alignment' => array(
                        'horizontal' => 'left',
                    )
                );
                $horizontal_center = array(
                    'alignment' => array(
                        'horizontal' => 'center',
                    )
                );

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'N' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_left);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'N' . ($this->length + ($this->startRow - 1)))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $sheet->getStyle('F' . ($this->startRow - 1) . ':' . 'N' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'A' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
            },
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        // TODO: Implement title() method.
        return $this->title;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        // TODO: Implement startCell() method.
        return 'A' . ($this->startRow);
    }

}
