<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 1:48 PM
 */

namespace App\Contracts;

interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * @param int $limit
     * @param int $offset
     * @param int $roleId
     * @param null $searchData
     * @return mixed
     */
    public function findUsers(int $limit = 10, int $offset = 0, int $roleId = 0, $searchData = null);
}
