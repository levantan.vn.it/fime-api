<?php

namespace App\Contracts;

/**
 * Interface ReviewCommentRepositoryInterface.
 *
 */
interface ReviewCommentRepositoryInterface extends RepositoryInterface
{
}