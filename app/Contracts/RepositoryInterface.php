<?php

namespace App\Contracts;

/**
 * Interface RepositoryInterface.
 *
 */
interface RepositoryInterface
{
    public function count(array $where = [], $columns = '*');
}