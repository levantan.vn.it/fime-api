<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 3:08 PM
 */

namespace App\Repositories;


use App\Contracts\TryFilesRepositoryInterface;
use App\Models\TryFiles;

class TryFilesRepository extends Repository implements TryFilesRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return TryFiles::class;
    }
}
