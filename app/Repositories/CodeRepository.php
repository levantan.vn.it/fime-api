<?php

namespace App\Repositories;

use App\Contracts\CodeRepositoryInterface;
use App\Models\Code;

/**
 * Class CodeRepository.
 *
 */
class CodeRepository extends Repository implements CodeRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Code::class;
    }
}
