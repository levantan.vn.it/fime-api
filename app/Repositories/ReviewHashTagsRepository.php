<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/22/2019
 * Time: 11:09 AM
 */

namespace App\Repositories;


use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Models\ReviewHashTags;

class ReviewHashTagsRepository extends Repository implements ReviewHashTagsRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return ReviewHashTags::class;
    }
}
