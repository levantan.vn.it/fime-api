<?php

namespace App\Repositories;

use App\Contracts\BlogRepositoryInterface;
use App\Models\Blog;

/**
 * Class BlogRepository.
 *
 */
class BlogRepository extends Repository implements BlogRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }
}
