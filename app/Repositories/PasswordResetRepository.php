<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 1:50 PM
 */

namespace App\Repositories;


use App\Contracts\PasswordResetRepositoryInterface;
use App\Models\PasswordReset;

class PasswordResetRepository extends Repository implements PasswordResetRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PasswordReset::class;
    }
}