<?php

namespace App\Repositories;

use App\Contracts\TryRepositoryInterface;
use App\Models\TryFree;

/**
 * Class TryRepository.
 *
 */
class TryRepository extends Repository implements TryRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return TryFree::class;
    }
}
