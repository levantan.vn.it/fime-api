<?php

namespace App\Repositories;

use App\Contracts\CodeGroupRepositoryInterface;
use App\Models\CodeGroup;

/**
 * Class CodeRepository.
 *
 */
class CodeGroupRepository extends Repository implements CodeGroupRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return CodeGroup::class;
    }
}
