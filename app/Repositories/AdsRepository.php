<?php

namespace App\Repositories;

use App\Contracts\AdsRepositoryInterface;
use App\Models\Ads;

/**
 * Class BannerRepository.
 *
 */
class AdsRepository extends Repository implements AdsRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Ads::class;
    }
}
