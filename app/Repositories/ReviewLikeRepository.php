<?php

namespace App\Repositories;

use App\Contracts\ReviewLikeRepositoryInterface;
use App\Models\ReviewLike;

/**
 * Class ReviewLikeRepository.
 *
 */
class ReviewLikeRepository extends Repository implements ReviewLikeRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return ReviewLike::class;
    }
}
