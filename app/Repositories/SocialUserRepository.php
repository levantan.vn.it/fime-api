<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 1:50 PM
 */

namespace App\Repositories;


use App\Contracts\SocialUserRepositoryInterface;
use App\Models\SocialUser;

class SocialUserRepository extends Repository implements SocialUserRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SocialUser::class;
    }

    /**
     * @param int $id
     * @return SocialUser
     */
    public function getUserById(int $id): SocialUser
    {
        // TODO: Implement getUserById() method.
        return SocialUser::find($id);
    }
}