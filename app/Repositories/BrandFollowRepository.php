<?php

namespace App\Repositories;

use App\Contracts\BrandFollowRepositoryInterface;
use App\Models\BrandFollow;

/**
 * Class UserFollowRepository.
 *
 */
class BrandFollowRepository extends Repository implements BrandFollowRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return BrandFollow::class;
    }
}
