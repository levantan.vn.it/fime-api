<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateFollowingOfUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateFollowingOfUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $followings = DB::table('TCT_FLLW')->select('fllwr_user_no',DB::raw('count(fllwr_user_no) as number_of_following'))
        ->groupBy('fllwr_user_no')->limit(10)->get();
        foreach($followings as $following){
            DB::table('TDM_USER')->where('fllwr_user_no',$following->number_of_following)->update([
                'following' => $following->number_of_following
            ]);
        }
    }
}
