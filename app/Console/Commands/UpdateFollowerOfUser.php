<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateFollowerOfUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateFollowerOfUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $followers = DB::table('TCT_FLLW')->select('user_no', DB::raw('count(user_no) as number_of_followers'))
        ->groupBy('user_no')->orderBy('hot_fimer_1year','desc')->limit(10)->get();

        foreach($followers as $follower) {
            DB::table('TDM_USER')->where('user_no',$follower->user_no)->update([
                'follower' => $follower->number_of_followers
            ]);
        }
    }
}
