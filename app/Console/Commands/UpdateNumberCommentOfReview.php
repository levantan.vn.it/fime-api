<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateNumberCommentOfReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateNumberCommentOfReview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $comments_number = DB::table('TCT_REVIEW_ANWR')->select('review_no', DB::raw('count(review_no) as number_of_comment'))
        ->groupBy('review_no')->limit(100)->get();

        foreach ($comments_number as $comment){
            DB::table('TCT_REVIEW')->where('review_no', $comment->review_no)->update([
                'comments' => $comment->number_of_comment
            ]);
        }
    }
}
