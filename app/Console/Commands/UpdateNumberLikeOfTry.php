<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateNumberLikeOfTry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateNumberLikeOfTry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $like_number = DB::table('TCT_GOODS_RECM')->select('cntnts_no', DB::raw('count(cntnts_no) as number_of_likes'))->groupBy('cntnts_no')->limit(100)->get();

        foreach ($like_number as $like){
            DB::table('TCT_GOODS')->where('cntnts_no', $like->cntnts_no)->update([
                'likes' => $like->number_of_likes
            ]);
        }
    }
}
