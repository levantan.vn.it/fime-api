<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateNumberCommentOfTry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateNumberCommentOfTry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $comment_number = DB::table('TCT_GOODS_ANWR')->select('cntnts_no', DB::raw('count(cntnts_no) as number_of_comments'))
        ->groupBy('cntnts_no')->orderBy('cntnts_no', 'desc')->limit(100)->get();

        foreach ($comment_number as $comment){
            DB::table('TCT_GOODS')->where('cntnts_no', $comment->cntnts_no)->update([
                'comments' => $comment->number_of_comments
            ]);
        }
    }
}
