<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateQueryTry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:try';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tries = DB::table('TCT_GOODS')->select('cntnts_no','likes','comments','tries_apply')->where('likes',0)->where('comments',0)->where('tries_apply',0)->orderBy('cntnts_no','desc')->limit(100)->get();
        foreach($tries as $try){
            $total_apply = DB::table('TCT_DRWT')->select('cntnts_no')->where('cntnts_no',$try->cntnts_no)->count();
            $likes = DB::table('TCT_GOODS_RECM')->select('cntnts_no')->where('cntnts_no',$try->cntnts_no)->count();
            $comments = DB::table('TCT_GOODS_ANWR')->select('cntnts_no')->where('cntnts_no',$try->cntnts_no)->count();
            $try->tries_apply = $total_apply;
            $try->likes = $likes;
            $try->comments = $comments;
            $try->save();
        }
    }
}
