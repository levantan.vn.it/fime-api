<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\User;
class UpdatePicUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdatePicUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::select('TDM_USER.user_no', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.pic','TDM_USER.slug')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TDM_USER.update_pic',NULL)->where('pic','!=',null)->orderBy('TDM_USER.created_at', 'desc')->limit(50)->get();
        if(!empty($users) && count($users)){
            foreach ($users as $user) {
                $file_name =  $user->pic;
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $name = pathinfo($file_name, PATHINFO_FILENAME  );
                $new_url = 'https://api-np.fime.vn'.$file_name;
                try {
                   $command = "convert ".$new_url;
                   if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                       $command .= ' -strip ';
                   }else{
                       $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                   }
                   $command .= $file_name;
                   exec($command);
                   
                   $file_thumb_name = $name . '_TMB' . '.' . $extension;
                   $image_thumb = \Image::make($new_url)->resize(150, null,function ($constraint) {
                       $constraint->aspectRatio();
                       $constraint->upsize();
                   });
                   $image_thumb->save((public_path().'/data/images/profile/'.$file_thumb_name));
                   $user->update_pic = '/data/images/profile/'.$file_thumb_name;
                   $user->save(); 
                } catch (\Intervention \ Image \ Exception \ NotReadableException $e) {
                    $user->update_pic = 'N';
                    $user->save(); 
                    break;
                }
               
            }
        }
    }
}
