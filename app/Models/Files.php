<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:29 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'files';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'url'];
}
