<?php

namespace App\Models;

class ReviewAd extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'review_ads';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'url', 'is_youtube', 'is_disabled', 'created_by', 'updated_by', 'deleted_by'];
}
