<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/22/2019
 * Time: 11:10 AM
 */

namespace App\Models;


class ReviewHashTags extends BaseModel
{
    public $connection = 'aws';
    /**
     * @var string
     */
    public $table = 'review_hashtag';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['review_no', 'hash_seq'];

    /**
     * @var string
     */
    protected  $primaryKey = 'id';
}
