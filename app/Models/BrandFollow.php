<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class BrandFollow extends BaseModel
{
    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_BRFL';

    public $primaryKey = ['user_no', 'fllwr_br'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_no', 'fllwr_br', 'regist_dt'];
}
