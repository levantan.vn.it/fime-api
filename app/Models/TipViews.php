<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 3:58 PM
 */

namespace App\Models;


class TipViews extends BaseModel
{
    protected $connection = 'aws';

    protected $table = 'tip_views';

    protected  $primaryKey = 'ti_id';

    public $timestamps = false;

    protected $fillable = ['ti_id', 'views'];
}
