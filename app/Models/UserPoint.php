<?php

namespace App\Models;

class UserPoint extends BaseModel
{
    protected $connection = 'aws';

    protected  $primaryKey = 'sn';

    protected $table = 'TCT_PNTT';

    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_no', 'accml_dt', 'accml_pntt', 'cntnts_no', 'pntt_accml_code', 'pntt_use_code',
        'user_pntt', 'use_dt', 'rm', 'review_no'];
}
