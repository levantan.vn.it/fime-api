<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReviewFiles extends Model
{
    protected $table = 'TCT_REVIEW_FILE';

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['file_se_code', 'file_sn', 'stre_file_nm', 'file_size', 'orginl_file_nm', 'file_cours',
        'se_code', 'image_dc', 'thumb_file_nm', 'register_esntl_no', 'regist_dt', 'dwld_co', 'thumb_file_nm2',
        'review_no'];
}
