<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodeGroup extends Model
{
    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TSM_CODE_GROUP';

    protected $primaryKey = 'code_group';

    public $incrementing = false;

    public  $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code_group','svc_relm_code', 'code_group_nm', 'code_group_eng_nm', 'code_group_dc', 'use_at', 'register_esntl_no', 'regist_dt', 'updusr_esntl_no', 'updt_dt', 'sort_ordr'];
}
