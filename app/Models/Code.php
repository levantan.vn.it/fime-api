<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Code extends BaseModel
{
    use HasSlug;

    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TSM_CODE';

    protected $primaryKey = 'code';

    public $incrementing = false;

    public  $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'upper_code', 'code_group', 'code_nm', 'code_eng_nm', 'code_dc', 'sort_ordr', 'use_at', 'refrn_info', 'register_esntl_no', 'regist_dt', 'updusr_esntl_no', 'updt_dt', 'rm', 'slug', 'additional', 'additional_inactive','file'];

    /**
     * @return SlugOptions
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('code_nm')
            ->saveSlugsTo('slug');
    }
}
