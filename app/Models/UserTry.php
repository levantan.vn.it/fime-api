<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class UserTry extends BaseModel
{
    public $connection = 'aws';

    public $primaryKey = ['cntnts_no', 'user_no'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_DRWT';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cntnts_no', 'user_no', 'reqst_dt', 'slctn_dt', 'slctn_at', 'dlvy_at', 'dlvy_dt',
        'dlvy_compt_at', 'dlvy_compt_dt', 'dlvy_addr', 'dlvy_cmpny_code', 'dlvy_mth_code', 'invc_no', 'memo'];

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

}
