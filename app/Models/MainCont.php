<?php

namespace App\Models;

class MainCont extends BaseModel
{
    protected $connection = 'aws';

    protected  $primaryKey = 'conts_seq';

    protected $table = 'TCT_MAIN_CONTS';

    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ref_no', 'cont_type', 'cont_ord', 'cont_std', 'cont_kind'];

}
