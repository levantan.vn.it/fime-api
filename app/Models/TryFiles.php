<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:29 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TryFiles extends Model
{
    protected $table = 'TOM_CNTNTS_FILE_CMMN';

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['cntnts_no', 'file_se_code', 'file_sn', 'orginl_file_nm', 'stre_file_nm', 'file_size', 'file_cours',
        'etc_attrb_1', 'etc_attrb_2', 'se_code', 'image_dc', 'thumb_file_nm', 'mrc_file_nm', 'register_esntl_no', 'regist_dt',
        'updusr_esntl_no', 'updt_dt', 'dwld_co', 'thumb_file_nm2'];
}
