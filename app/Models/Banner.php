<?php

namespace App\Models;

class Banner extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'url', 'resource_type', 'target_url', 'period_from', 'period_to', 'order',
        'target_type', 'button_text', 'is_disabled', 'created_by', 'updated_by', 'deleted_by','view_cnt'];
}
