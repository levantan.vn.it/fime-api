<?php

namespace App\Models;

class SystemNotification extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'url', 'is_disabled', 'is_system', 'guid', 'posted_at', 'created_by', 'updated_by', 'deleted_by'];
}
