<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Actions\Notification\CreateNotificationAction;
class LikeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $type;
    protected $user_id;
    protected $object_id;
    protected $is_liking;
    protected $object_type;
    protected $author_id;
    protected $disable_noti;
    public function __construct($type,$user_id,$object_id,$is_liking,$object_type, $author_id,$disable_noti)
    {
        $this->type = $type;
        $this->user_id = $user_id;
        $this->object_id = $object_id;
        $this->is_liking = $is_liking;
        $this->object_type = $object_type;
        $this->author_id = $author_id;
        $this->disable_noti = $disable_noti;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $action = new CreateNotificationAction();
        if($this->type == 'try'){
            $action->createTryAppliedNotification($this->object_id, $this->disable_noti);
            $action->likeTry($this->user_id, $this->object_id, $this->is_liking);
        }else{
            $this->createLikeNotification($this->object_id, $this->object_type, $this->author_id, false, $this->disable_noti);
            $action->likeReview($this->user_id, $this->object_id, $this->is_liking);
        }
    }
}
