<?php
namespace App\Criterias\Tip;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllTipsByCriteria implements CriteriaInterface
{
    /**
     * GetRecentlyReviewCriteria constructor.
     */
    public $period;
    public $pageSize;
    public function __construct($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $pageSize = $this->pageSize;
        if ($pageSize != 0) {
        $model = $model
            ->select('TCT_TIPS.*')
            ->orderBy('ti_id', 'desc')
            ->where('display_yn', 'Y')
            ->skip((\Request::input('page') - 1) * 10)
            ->limit(10);
        } else {
            $model = $model
            ->select('TCT_TIPS.*')
            ->orderBy('ti_id', 'desc')
            ->where('display_yn', 'Y')->from('TCT_TIPS')->get();
        }
        return $model;
    }
}