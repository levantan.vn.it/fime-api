<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/4/2019
 * Time: 11:08 AM
 */

namespace App\Criterias\Tip;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTipsByIdsCriteria implements CriteriaInterface
{
    public $ids;
    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.

        $model = $model
            ->select('TCT_TIPS.*')
            ->whereIn('TCT_TIPS.ti_id', $this->ids);
        return $model;
    }
}
