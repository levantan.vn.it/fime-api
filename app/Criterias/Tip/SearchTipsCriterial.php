<?php

namespace App\Criterias\Tip;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class SearchTipsCriterial implements CriteriaInterface
{
    private $tip_category_slug;
    private $searchValue;
    private $tipIds;

    public function __construct($tip_category_slug = null, $searchValue = null, $tipIds = null)
    {
        $this->tip_category_slug = $tip_category_slug;
        $this->searchValue = $searchValue;
        $this->tipIds = $tipIds;
    }
     /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_TIPS.ti_id',
            'TCT_TIPS.subject',
            'TCT_TIPS.img_url',
            'TCT_TIPS.cover_img1',
            'TCT_TIPS.cover_img2',
            'TCT_TIPS.cover_img3',
            'TCT_TIPS.cover_img4',
            'TCT_TIPS.created_at',
            'TCT_TIPS.tips_desc_head',
            'TCT_TIPS.tips_desc_body',
            'TCT_TIPS.updated_at')
            ->orderBy('TCT_TIPS.ti_id', 'desc');
            
        if ($this->searchValue != null) {
            $model = $model->where(function ($query) {
                $query->where('TCT_TIPS.subject', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.tips_desc_head', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.tips_desc_body', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.caption_text', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.cont1_head', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.cont1_body', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.caption_text2', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.cont2_head', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_TIPS.cont2_body', 'LIKE', '%' . $this->searchValue . '%');
                
                return $query;
            });
            $model = $model->orderByRaw(
                 "CASE
                    WHEN subject LIKE '". $this->searchValue ."' THEN 1
                    WHEN subject LIKE '". $this->searchValue ."%' THEN 2
                    WHEN subject LIKE '%". $this->searchValue ."' THEN 4
                    WHEN tips_desc_head LIKE '". $this->searchValue ."' THEN 1
                    WHEN tips_desc_head LIKE '". $this->searchValue ."%' THEN 2
                    WHEN tips_desc_head LIKE '%". $this->searchValue ."' THEN 4
                    WHEN tips_desc_body LIKE '". $this->searchValue ."' THEN 1
                    WHEN tips_desc_body LIKE '". $this->searchValue ."%' THEN 2
                    WHEN tips_desc_body LIKE '%". $this->searchValue ."' THEN 4
                    WHEN caption_text LIKE '". $this->searchValue ."' THEN 1
                    WHEN caption_text LIKE '". $this->searchValue ."%' THEN 2
                    WHEN caption_text LIKE '%". $this->searchValue ."' THEN 4
                    WHEN cont1_head LIKE '". $this->searchValue ."' THEN 1
                    WHEN cont1_head LIKE '". $this->searchValue ."%' THEN 2
                    WHEN cont1_head LIKE '%". $this->searchValue ."' THEN 4
                    WHEN cont1_body LIKE '". $this->searchValue ."' THEN 1
                    WHEN cont1_body LIKE '". $this->searchValue ."%' THEN 2
                    WHEN cont1_body LIKE '%". $this->searchValue ."' THEN 4
                     WHEN caption_text2 LIKE '". $this->searchValue ."' THEN 1
                    WHEN caption_text2 LIKE '". $this->searchValue ."%' THEN 2
                    WHEN caption_text2 LIKE '%". $this->searchValue ."' THEN 4
                     WHEN cont2_head LIKE '". $this->searchValue ."' THEN 1
                    WHEN cont2_head LIKE '". $this->searchValue ."%' THEN 2
                    WHEN cont2_head LIKE '%". $this->searchValue ."' THEN 4
                     WHEN cont2_body LIKE '". $this->searchValue ."' THEN 1
                    WHEN cont2_body LIKE '". $this->searchValue ."%' THEN 2
                    WHEN cont2_body LIKE '%". $this->searchValue ."' THEN 4
                    ELSE 3
                  END asc"
            );
        }
        return $model;
    }
}
