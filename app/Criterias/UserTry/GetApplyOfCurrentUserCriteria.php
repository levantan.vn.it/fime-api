<?php
namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetApplyOfCurrentUserCriteria implements CriteriaInterface
{
    private $list_try_id;
    private $user_id;
    public function __construct($list_try_id, $user_id)
    {
        $this->list_try_id = $list_try_id;
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('cntnts_no', 'user_no')
            ->where('user_no', $this->user_id)
            ->whereIn('cntnts_no', $this->list_try_id);
        return $model;
    }
}