<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:48 AM
 */

namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfTriesByUserIdsCriteria implements CriteriaInterface
{
    /** @var int */
    private $userIds;

    public function __construct($userIds)
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_tries'), 'TDM_USER.user_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_DRWT.user_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_DRWT.cntnts_no')
            ->join('TCT_GOODS', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code')
            ->whereIn('TCT_DRWT.user_no', $this->userIds)
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->groupBy('TCT_DRWT.user_no');

        return $model;
    }
}
