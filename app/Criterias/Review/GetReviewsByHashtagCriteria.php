<?php

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsByHashtagCriteria implements CriteriaInterface
{
    private $hashtag;

    public function __construct($hashtag)
    {
        $this->hashtag = $hashtag;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.*', 'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->where('TCT_REVIEW.review_dc', 'like', '%' . $this->hashtag . '%')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc');
        return $model;
    }
}
