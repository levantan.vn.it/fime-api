<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 2:55 PM
 */

namespace App\Criterias\Brand;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllProductCriteria implements CriteriaInterface
{
    /** @var int */
    private $brandId;

    /**
     * GetUsersFollowCriteria constructor.
     * @param int $brandId
     */
    public function __construct($brandId)
    {
        $this->brandId = $brandId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $model = $model
            ->select('TCT_GOODS.brnd_nm','TCT_GOODS.cntnts_no','TSM_CODE.*')
            ->leftJoin('TCT_GOODS', 'TSM_CODE.code', '=', 'TCT_GOODS.brnd_code')
            ->where('TSM_CODE.code', $this->brandId)
            ->orderBy('TCT_GOODS.cntnts_no','desc');
        return $model;
    }
}
