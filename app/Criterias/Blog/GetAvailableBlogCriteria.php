<?php
namespace App\Criterias\Blog;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAvailableBlogCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('blogs.*')
            ->where('blogs.show_on_main', '=', 1)
            ->where('blogs.is_disabled', '=', 0)
            ->orderBy('blogs.id', 'desc');
        return $model;
    }
}