<?php
namespace App\Criterias\Blog;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTopViewBlogCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('blogs.*')
            ->where('blogs.is_disabled', '=', 0)
            ->orderBy('blogs.views', 'desc');
        return $model;
    }
}