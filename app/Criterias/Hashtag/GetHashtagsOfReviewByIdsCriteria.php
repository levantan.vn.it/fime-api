<?php
namespace App\Criterias\Comment;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetHashtagsOfReviewByIdsCriteria implements CriteriaInterface
{
    private $review_ids;

    public function __construct($review_ids)
    {
        $this->review_ids = $review_ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('tct_pophash.*')
            ->join('tct_pophash', 'tct_pophash.hash_seq', '=', 'review_hashtag.hash_seq')
            ->whereIn('review_no', $this->review_ids)
            ->groupBy('review_no');

        return $model;
    }
}