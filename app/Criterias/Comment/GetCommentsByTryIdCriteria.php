<?php
namespace App\Criterias\Comment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetCommentsByTryIdCriteria implements CriteriaInterface
{
    private $object_id;
    private $parent_id;
    private $last_id;
    private $page;

    public function __construct($object_id, $page = 0, $last_id = INF, $parent_id = null)
    {
        $this->object_id = $object_id;
        $this->parent_id = $parent_id;
        $this->page = $page;
        $this->last_id = $last_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_GOODS_ANWR.id','TCT_GOODS_ANWR.resource_type','TCT_GOODS_ANWR.file', 'anwr_cn as content', 'anwr_writng_dt as created_at', 'TDM_USER.reg_name as author',
                'TDM_USER.pic as author_avatar', 'TDM_USER.id as author_ds', 'TCT_GOODS_ANWR.user_no',
                'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_ANWR.user_no')
            ->where('TCT_GOODS_ANWR.cntnts_no', '=', $this->object_id)
            ->where('TCT_GOODS_ANWR.parent_id', '=', $this->parent_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_GOODS_ANWR.delete_at', 'N')
            ->where('TCT_GOODS_ANWR.expsr_at', 'Y')
            ->where('TCT_GOODS_ANWR.id', '<', $this->last_id)
            ->orderBy('TCT_GOODS_ANWR.id', 'desc')
            ->limit(5);

        return $model;
    }
}
