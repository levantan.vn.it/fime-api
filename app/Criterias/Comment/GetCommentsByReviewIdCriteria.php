<?php
namespace App\Criterias\Comment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetCommentsByReviewIdCriteria implements CriteriaInterface
{
    private $object_id;
    private $parent_id;
    private $last_id;
    private $page;

    public function __construct($object_id, $page = 0, $last_id = INF, $parent_id = null)
    {
        $this->object_id = $object_id;
        $this->parent_id = $parent_id;
        $this->page = $page;
        $this->last_id = $last_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW_ANWR.id','TCT_REVIEW_ANWR.resource_type','TCT_REVIEW_ANWR.file', 'anwr_cn as content', 'anwr_writng_dt as created_at',
                'TDM_USER.reg_name as author', 'TDM_USER.id as author_ds', 'TDM_USER.pic as author_avatar',
                'TCT_REVIEW_ANWR.user_no', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW_ANWR.user_no')
            ->where('TCT_REVIEW_ANWR.review_no', '=', $this->object_id)
            ->where('TCT_REVIEW_ANWR.parent_id', '=', $this->parent_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW_ANWR.delete_at', 'N')
            ->where('TCT_REVIEW_ANWR.expsr_at', 'Y')
            ->where('TCT_REVIEW_ANWR.id', '<', $this->last_id)
            ->orderBy('TCT_REVIEW_ANWR.anwr_writng_dt', 'desc')
            ->limit(5);

        return $model;
    }
}
