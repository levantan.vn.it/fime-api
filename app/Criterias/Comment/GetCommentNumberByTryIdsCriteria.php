<?php
namespace App\Criterias\Comment;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetCommentNumberByTryIdsCriteria implements CriteriaInterface
{
    private $try_ids;

    public function __construct($try_ids)
    {
        $this->try_ids = $try_ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as comment_number'), 'cntnts_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_ANWR.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_GOODS_ANWR.expsr_at', 'Y')
            ->where('TCT_GOODS_ANWR.delete_at', 'N')
            ->whereIn('cntnts_no', $this->try_ids)
            ->groupBy('cntnts_no');
        return $model;
    }
}