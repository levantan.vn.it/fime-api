<?php
namespace App\Criterias\Comment;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfCommentsByReviewIdsCriteria implements CriteriaInterface
{
    private $review_ids;

    public function __construct($review_ids)
    {
        $this->review_ids = $review_ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as comment_number'), 'review_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW_ANWR.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW_ANWR.expsr_at', 'Y')
            ->where('TCT_REVIEW_ANWR.delete_at', 'N')
            ->whereIn('review_no', $this->review_ids)
            ->groupBy('review_no');

        return $model;
    }
}