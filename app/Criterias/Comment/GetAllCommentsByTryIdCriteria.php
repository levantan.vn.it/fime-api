<?php
namespace App\Criterias\Comment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllCommentsByTryIdCriteria implements CriteriaInterface
{
    private $try_id;

    public function __construct($try_id)
    {
        $this->try_id = $try_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_GOODS_ANWR.id', 'TCT_GOODS_ANWR.expsr_at', 'anwr_cn as content', 'anwr_writng_dt as created_at',
                'TDM_USER.reg_name as author', 'TDM_USER.id as author_ds','TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_ANWR.user_no')
            ->where('TCT_GOODS_ANWR.cntnts_no', '=', $this->try_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_GOODS_ANWR.delete_at', 'N')
            ->orderBy('TCT_GOODS_ANWR.anwr_writng_dt', 'asc');
        return $model;
    }
}