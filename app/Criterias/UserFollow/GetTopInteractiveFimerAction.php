<?php

namespace App\Actions\UserFollow;
use App\Actions\Action;
use App\Contracts\SettingRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\User\GetHotFimersCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetUsersByIdsCriteria;
use Mockery\Exception;

class GetTopInteractiveFimerAction extends Action {
    protected $user_follow_repository;
    protected $setting_repository;
    protected $user_repository;

    /**
     * GetTopInteractiveFimerAction constructor.
     * @param UserFollowRepositoryInterface $user_follow_repository
     * @param SettingRepositoryInterface $setting_repository
     * @param UserRepositoryInterface $user_repository
     */
    public function __construct(UserFollowRepositoryInterface $user_follow_repository,
                                SettingRepositoryInterface $setting_repository,
                                UserRepositoryInterface $user_repository) {
        $this->user_follow_repository = $user_follow_repository;
        $this->user_repository = $user_repository;
        $this->setting_repository = $setting_repository;
    }

    protected function checkCurrentUserFollowingReviewAuthor($top_fimer) {
        $current_user_id = auth()->id();
        $list_following_id = $top_fimer->pluck('user_no')->toArray();

        $user_followings = $this->user_follow_repository->getByCriteria(new GetFollowingOfCurrentUserCriteria($list_following_id, $current_user_id))->keyBy('followed_user_id');
        // check current user follow or not follow fimer
        // $followers =$this->user_follow_repository->getByCriteria(
        //     new GetNumberOfFollowersByUserIdsCriteria($list_following_id)
        // )->keyBy('user_id');
        
        foreach ($top_fimer as $index => $fimer) {
            $fimer->followed = 0;
            // $fimer->follows = 0;
            if(isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }

            // add number number_of_followers to fimer
            // if(isset($followers[$fimer->user_no])) {
            //     $fimer->follows = $followers[$fimer->user_no]->number_of_followers;
            // }
        }
    }

    /**
     * @return mixed
     */
    public function run() {

        try {
            $settings = $this->setting_repository->findWhere([
                'group' => 'review_fimer'
            ])->keyBy('key');

            if(isset($settings['config_manually']) && $settings['config_manually']->value == 'true') {
                $users = isset($settings['fimer']) ? json_decode($settings['fimer']->value): [];

                if(count($users) == 0) {
                    return [];
                }
                $users = collect($users)->keyBy('userId');
                $userIds = collect($users)->pluck('userId')->toArray();

                $top_fimers = $this->user_repository->getByCriteria(new GetUsersByIdsCriteria($userIds));
                foreach ($top_fimers as $top_fimer) {
                    $top_fimer->position = 0;
                    if(isset($users[$top_fimer->user_no])) {
                        $top_fimer->position = $users[$top_fimer->user_no]->position;
                    }
                }

                $top_fimers = $top_fimers->sortBy('position')->values();

            } else {
                $top_fimers = $this->user_repository->pushCriteria(new GetHotFimersCriteria(90))->paginate(3);
                $top_fimers = collect($top_fimers->items());
            }

            $this->checkCurrentUserFollowingReviewAuthor($top_fimers);
            return $top_fimers;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
