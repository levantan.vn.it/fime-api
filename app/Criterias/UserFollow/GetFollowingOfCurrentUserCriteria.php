<?php
namespace App\Criterias\UserFollow;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetFollowingOfCurrentUserCriteria implements CriteriaInterface
{
    private $list_following_id;
    private $current_user_id;

    /**
     * GetTopInteractiveFimerCriteria constructor.
     * @param $list_following_id
     * @param $current_user_id
     */
    public function __construct($list_following_id, $current_user_id)
    {
        $this->list_following_id = $list_following_id;
        $this->current_user_id = $current_user_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as follow_number'), 'TDM_USER.user_no as followed_user_id')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_FLLW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->whereIn('TCT_FLLW.user_no', $this->list_following_id)
            ->where('TCT_FLLW.fllwr_user_no', $this->current_user_id)
            ->groupBy('TCT_FLLW.user_no');
        return $model;
    }
}