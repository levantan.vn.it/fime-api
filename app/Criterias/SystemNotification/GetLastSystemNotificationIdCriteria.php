<?php
namespace App\Criterias\SystemNotification;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetLastSystemNotificationIdCriteria implements CriteriaInterface
{
    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('notifications.system_notification_id')
            ->orderBy('notifications.system_notification_id', 'desc')
            ->withTrashed()
            ->limit(1);
        return $model;
    }
}