<?php
namespace App\Criterias\UserLike;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetLikesOfCurrentUserCriteria implements CriteriaInterface
{
    private $object_ids;
    private $user_id;

    public function __construct($object_ids, $user_id)
    {
        $this->object_ids = $object_ids;
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->whereIn('cntnts_no', $this->object_ids)
            ->where('user_no', $this->user_id);

        return $model;
    }
}