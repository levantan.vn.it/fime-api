<?php
namespace App\Criterias\UserLike;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetLikeNumberOfTryByListIdCriteria implements CriteriaInterface
{
    private $list_try_id;
    public function __construct($list_try_id)
    {
        $this->list_try_id = $list_try_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as like_number'), 'cntnts_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_RECM.user_no')
            ->whereIn('cntnts_no', $this->list_try_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->groupBy('cntnts_no');
        return $model;
    }
}