<?php
namespace App\Criterias\Notification;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationsCriteria implements CriteriaInterface
{
    private $list_id;

    public function __construct($list_id)
    {
        $this->list_id = $list_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('notifications.*')
            ->where('notifications.is_disabled', '=', 0)
            ->whereIn('notifications.id', $this->list_id)
            ->orderBy('notifications.posted_at', 'desc');
        return $model;
    }
}