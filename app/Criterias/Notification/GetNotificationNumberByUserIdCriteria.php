<?php

namespace App\Criterias\Notification;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationNumberByUserIdCriteria implements CriteriaInterface
{
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $now = Carbon::now();
        $model = $model
            ->select(DB::raw('count(user_id) as count'))->where('notifications.is_seen', '=', 0)->where('notifications.user_id', '=', $this->user_id)->where('notifications.is_disabled',0)->whereNull('notifications.deleted_at');
        return $model;
    }
}