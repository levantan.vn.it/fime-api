<?php
namespace App\Criterias\Notification;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationUserCriteria implements CriteriaInterface
{
    private $list_notification_id;

    public function __construct($list_notification_id)
    {
        $this->list_notification_id = $list_notification_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('notifications.id', 'TDM_USER.reg_name as user_name', 'TDM_USER.pic as user_avatar', 'TDM_USER.slug as user_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'notifications.reference_user_id')
            ->whereIn('notifications.id', $this->list_notification_id);
        return $model;
    }
}