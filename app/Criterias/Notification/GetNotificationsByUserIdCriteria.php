<?php
namespace App\Criterias\Notification;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationsByUserIdCriteria implements CriteriaInterface
{
    private $user_id;
    private $page;

    public function __construct($user_id, $page)
    {
        $this->user_id = $user_id;
        $this->page = $page - 1;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('notifications.guid', DB::raw('max(id) as id'))
            ->where(function($query) {
                $query->where('notifications.user_id', '=', $this->user_id);
                $query->orWhereNull('notifications.user_id');
            })
            ->where('notifications.is_disabled', '=', 0)
            ->where('notifications.posted_at', '<=', Carbon::now())
            ->groupBy('notifications.guid')
            ->limit(10)
            ->skip($this->page * 10)
            ->orderBy('posted_at', 'desc');
        return $model;
    }
}