<?php
namespace App\Criterias\Users;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetHotFimerCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $select = ['TDM_USER.id',
            'TDM_USER.slug',
            'TDM_USER.pic',
            'TDM_USER.hot_fimer',
            'TDM_USER.hot_fimer_1year',
            'TDM_USER.hot_fimer_7days',
            'TDM_USER.hot_fimer_30days',
            'TDM_USER.hot_fimer_90days',
            'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as number_of_reviews")];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS update_pic");
        $model = $model
            ->select($select)
            ->join('TCT_REVIEW', 'TCT_REVIEW.USER_NO', 'TDM_USER.USER_NO')
            ->orderBy('TDM_USER.hot_fimer_7days', 'desc')
            ->where('TDM_USER.hot_fimer','1')
            ->distinct()
            ->groupBy('TDM_USER.user_no')
            ->limit(4);

        return $model;
    }
}
