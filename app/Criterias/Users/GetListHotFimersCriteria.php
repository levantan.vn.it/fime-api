<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/27/2019
 * Time: 3:13 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetListHotFimersCriteria implements CriteriaInterface
{
    /** @var string */
    private $searchType;
    /** @var string|null */
    private $searchValue;
    /** @var int */
    private $allowComment;
    /** @var int */
    private $allowReview;
    /** @var string */
    private $isActive;
    /** @var string */
    private $isDelete;
    /** @var int */
    private $roleId;


    /**
     * GetListUsersCriteria constructor.
     * @param string $searchType
     * @param string|null $searchValue
     * @param int $allowComment
     * @param int $allowReview
     * @param string $isActive
     * @param string $isDelete
     * @param int $roleId
     */
    public function __construct(string $searchType, ?string $searchValue, int $allowComment, int $allowReview, string $isActive, string $isDelete, int $roleId)
    {
        $this->searchType = $searchType;
        $this->searchValue = $searchValue;
        $this->allowComment = $allowComment;
        $this->allowReview = $allowReview;
        $this->isActive = $isActive;
        $this->isDelete = $isDelete;
        $this->roleId = $roleId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->searchType && $this->searchValue) {
            $model = $model
                ->select('user_no',
                'reg_name',
                'id',
                'cellphone',
                'email',
                'home_addr1',
                'last_login_dt',
                'reviews',
                'updated_at',
                'allow_comment',
                'allow_review',
                'verification',
                'drmncy_at',
                'delete_at',
                'deleted_at',
                'slug',
                'role_id',
                'hot_fimer',
                'hot_fimer_7days')
                ->where($this->searchType, 'like', '%' . $this->searchValue . '%')
                ->where('allow_review', $this->allowReview)
                ->where('allow_comment', $this->allowComment)
                ->where('role_id', $this->roleId)
                ->where('drmncy_at', $this->isActive)
                ->where('delete_at', $this->isDelete)
                ->orderBy('hot_fimer_7days', 'desc');
        } else {
            $model = $model
                ->select('user_no',
                'reg_name',
                'id',
                'cellphone',
                'email',
                'home_addr1',
                'last_login_dt',
                'reviews',
                'updated_at',
                'allow_comment',
                'allow_review',
                'verification',
                'drmncy_at',
                'delete_at',
                'deleted_at',
                'slug',
                'role_id',
                'hot_fimer',
                'hot_fimer_7days')
                ->where('allow_review', $this->allowReview)
                ->where('allow_comment', $this->allowComment)
                ->where('role_id', $this->roleId)
                ->where('drmncy_at', $this->isActive)
                ->where('delete_at', $this->isDelete)
                ->orderBy('hot_fimer_7days', 'desc');
        }

        return $model;
    }
}
