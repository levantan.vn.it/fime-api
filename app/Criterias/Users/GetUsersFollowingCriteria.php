<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 1:43 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUsersFollowingCriteria implements CriteriaInterface
{

    /** @var string */
    private $userId;
    private $userIds;

    /**
     * GetUsersFollowCriteria constructor.
     * @param string $userId
     * @param array $userIds
     */
    public function __construct(string $userId, $userIds = [])
    {
        $this->userId = $userId;
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_FLLW.user_no as user_id')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_FLLW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('fllwr_user_no', $this->userId)
            ->orderBy('TCT_FLLW.regist_dt', 'desc');

        if(count($this->userIds) > 0){
            $model = $model->whereIn('TCT_FLLW.user_no', $this->userIds);
        }

        return $model;
    }
}
