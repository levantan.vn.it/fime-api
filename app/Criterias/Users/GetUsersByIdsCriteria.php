<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 2:08 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUsersByIdsCriteria implements CriteriaInterface
{
    /** @var array */
    private $userIds;

    public function __construct(array $userIds = [])
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $select = ['TDM_USER.user_no', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.pic','TDM_USER.slug'];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS update_pic");
        $model = $model
            ->select($select)
            ->whereIn('user_no', $this->userIds)
            // ->where('drmncy_at', 'N')
            // ->where('delete_at', 'N')
            ->orderBy('user_no', 'desc');
        return $model;
    }
}
