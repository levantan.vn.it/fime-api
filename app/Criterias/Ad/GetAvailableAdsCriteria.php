<?php
namespace App\Criterias\Ad;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAvailableAdsCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('description','name','order','target_type','resource_type','show_on_frontend','target_url','url','id','is_disabled','view_cnt')
            ->where('ads.show_on_frontend', '=', 1)
            ->orderBy('ads.order', 'asc')
            ->limit(8);
        return $model;
    }
}