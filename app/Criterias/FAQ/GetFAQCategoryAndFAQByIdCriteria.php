<?php
namespace App\Criterias\FAQ;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetFAQCategoryAndFAQByIdCriteria implements CriteriaInterface
{
    protected $code;

    /**
     * GetFAQCategoryAndFAQByIdCriteria constructor.
     * @param $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_FAQ.CNTNTS_NO as cntnts_no' ,'TCT_FAQ.SJ as sj', 'TCT_FAQ.CN as cn',
                'TSM_CODE.CODE_NM as category_name',
                'TSM_CODE.CODE as faq_se_code')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.CNTNTS_NO', '=', 'TCT_FAQ.CNTNTS_NO')
            ->join('TSM_CODE', 'TSM_CODE.CODE', '=', 'TCT_FAQ.FAQ_SE_CODE')
            ->where('TOM_CNTNTS_WDTB.DELETE_AT', '=', 'N')
            ->where('TCT_FAQ.CNTNTS_NO', '=', $this->code)
            ->orderBy('TCT_FAQ.CNTNTS_NO');
        return $model;
    }
}