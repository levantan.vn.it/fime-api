<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 2:55 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Actions\Constant;
class GetTriesOfUserCriteria implements CriteriaInterface
{
    /** @var int */
    private $userId;

    /**
     * GetUsersFollowCriteria constructor.
     * @param int $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $current_user_id = auth()->id();
        // TODO: Implement apply() method.
        //select 'TCT_GOODS.cntnts_no', 'TCT_GOODS.modl_nombr', 'TCT_GOODS.brnd_nm', 'TCT_GOODS.event_knd_code', 'TCT_GOODS.event_bgnde', 'TCT_GOODS.event_endde', 'TCT_GOODS.dlvy_bgnde', 'TCT_GOODS.dlvy_endde', 'TCT_GOODS.time_color_code', 'TCT_GOODS.event_trgter_co', 'TCT_GOODS.goods_pc', 'TCT_GOODS.slctn_compt_at', 'TCT_GOODS.link_url', 'TCT_GOODS.brnd_code', 'TCT_GOODS.goods_cl_code', 'TCT_GOODS.goods_txt_code', 'TCT_GOODS.goods_txt', 'TCT_GOODS.resource_type', 'TCT_GOODS.is_try_event', 'TCT_GOODS.try_event_type', 'TCT_GOODS.quantity_to_qualify', 'TCT_GOODS.tries_apply as total_apply', 'TCT_GOODS.likes', 'TCT_GOODS.comments', 'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug','TOM_CNTNTS_FILE_CMMN.file_cours', 'TOM_CNTNTS_FILE_CMMN.orginl_file_nm', 'TOM_CNTNTS_FILE_CMMN.stre_file_nm' From TCT_GOODS
        $select = ['TCT_GOODS.cntnts_no',
                    'TCT_GOODS.modl_nombr',
                    'TCT_GOODS.brnd_nm',
                    'TCT_GOODS.event_knd_code',
                    'TCT_GOODS.event_bgnde',
                    'TCT_GOODS.event_endde',
                    'TCT_GOODS.dlvy_bgnde',
                    'TCT_GOODS.dlvy_endde',
                    'TCT_GOODS.time_color_code',
                    'TCT_GOODS.event_trgter_co',
                    'TCT_GOODS.goods_pc',
                    'TCT_GOODS.slctn_compt_at',
                    'TCT_GOODS.link_url',
                    'TCT_GOODS.brnd_code',
                    'TCT_GOODS.goods_cl_code',
                    'TCT_GOODS.goods_txt_code',
                    'TCT_GOODS.goods_code_group',
                    'TCT_GOODS.goods_txt',
                    'TCT_GOODS.resource_type',
                    'TCT_GOODS.is_try_event',
                    'TCT_GOODS.try_event_type',
                    'TCT_GOODS.quantity_to_qualify',
                    'TCT_GOODS.tries_apply as total_apply',
                    'TCT_GOODS.likes',
                    'TCT_GOODS.comments',
                    'TCT_GOODS.level_apply',
                    'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug',
                    'TOM_CNTNTS_FILE_CMMN.file_cours',
                    'TOM_CNTNTS_FILE_CMMN.orginl_file_nm',
                    'TOM_CNTNTS_FILE_CMMN.stre_file_nm'
        ];
        // $current_user_id = 'U20190626111347';
        // check user login
        if(!empty($current_user_id)){
            // select is_liked = 1 if user like try or is_liked = null if user not like try
            // SELECT count(user_no) FROM TCT_GOODS_RECM WHERE TCT_GOODS_RECM.cntnts_no = TCT_GOODS.cntnts_no AND TCT_GOODS_RECM.user_no = '".$current_user_id."' GROUP BY TCT_GOODS_RECM.cntnts_no) as is_liked
            $select[] = \DB::raw("(SELECT count(user_no) FROM TCT_GOODS_RECM WHERE TCT_GOODS_RECM.cntnts_no = TCT_GOODS.cntnts_no AND TCT_GOODS_RECM.user_no = '".$current_user_id."' GROUP BY TCT_GOODS_RECM.cntnts_no) as is_liked");
            // select is_joined = 1 if user apply try or is_joined = null if user not apply try
            // SELECT count(*) FROM TCT_DRWT WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' GROUP BY TCT_DRWT.cntnts_no) as is_joined
            $select[] = \DB::raw("(SELECT count(*) FROM TCT_DRWT WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' GROUP BY TCT_DRWT.cntnts_no) as is_joined");
            // select slctn_dt if user wwiner try or slctn_dt = null if user not winner try
            // (SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as slctn_dt
            $select[] = \DB::raw("(SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as slctn_dt");
            // select dlvy_dt if user wwiner try or dlvy_dt = null if user not winner try
            // (SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as dlvy_dt
            $select[] = \DB::raw("(SELECT TCT_DRWT.dlvy_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as dlvy_dt");
        }

        $model = $model->select($select);
        $model = $model->where('TOM_CNTNTS_FILE_CMMN.se_code', '=', Constant::$MAIN_SE_CODE);
        $model = $model->join('TCT_GOODS', 'TCT_DRWT.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->where('TCT_DRWT.user_no', $this->userId)
            ->groupBy('TCT_GOODS.cntnts_no')
            ->orderBy('reqst_dt', 'desc');
        return $model;
    }
}
