<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTryNameByIdsCriteria implements CriteriaInterface
{
    private $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $model = $model
            ->select('TOM_CNTNTS_WDTB.sj', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->whereIn('TCT_GOODS.cntnts_no', $this->ids);

        return $model;
    }
}
