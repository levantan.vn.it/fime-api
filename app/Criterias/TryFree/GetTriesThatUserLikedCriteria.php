<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 10:13 AM
 */

namespace App\Criterias\TryFree;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTriesThatUserLikedCriteria implements CriteriaInterface
{
    /** @var int */
    private $tryIds;

    public function __construct(array $tryIds = [])
    {
        $this->tryIds = $tryIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_GOODS.cntnts_no',
                'TCT_GOODS.modl_nombr',
                'TCT_GOODS.brnd_nm',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.event_bgnde',
                'TCT_GOODS.event_endde',
                'TCT_GOODS.dlvy_bgnde',
                'TCT_GOODS.dlvy_endde',
                'TCT_GOODS.time_color_code',
                'TCT_GOODS.event_trgter_co',
                'TCT_GOODS.goods_pc',
                'TCT_GOODS.slctn_compt_at',
                'TCT_GOODS.link_url',
                'TCT_GOODS.brnd_code',
                'TCT_GOODS.goods_cl_code',
                'TCT_GOODS.goods_txt_code',
                'TCT_GOODS.goods_code_group',
                'TCT_GOODS.goods_txt',
                'TCT_GOODS.resource_type',
                'TCT_GOODS.is_try_event',
                'TCT_GOODS.level_apply',
                'TCT_GOODS.try_event_type',
                'TCT_GOODS.quantity_to_qualify',
                'TOM_CNTNTS_WDTB.sj as goods_nm', 'TOM_CNTNTS_WDTB.regist_dt as created_at', 'TOM_CNTNTS_WDTB.slug')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->whereIn('TCT_GOODS.cntnts_no', $this->tryIds)
            ->orderBy('created_at', 'desc');
        return $model;
    }
}
