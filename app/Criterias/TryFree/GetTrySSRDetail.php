<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTrySSRDetail implements CriteriaInterface
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_GOODS.short_desc')
            ->where('TCT_GOODS.cntnts_no', '=', $this->id);

        return $model;
    }
}
