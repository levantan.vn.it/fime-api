<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use App\Actions\Constant;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetFilesOfTryCriteria implements CriteriaInterface
{
    private $cntnts_no;

    public function __construct($cntnts_no)
    {
        $this->cntnts_no = $cntnts_no;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {   
        // select 'TOM_CNTNTS_FILE_CMMN.orginl_file_nm','TOM_CNTNTS_FILE_CMMN.cntnts_no','TOM_CNTNTS_FILE_CMMN.stre_file_nm','TOM_CNTNTS_FILE_CMMN.file_size','TOM_CNTNTS_FILE_CMMN.file_cours','TOM_CNTNTS_FILE_CMMN.se_code' from TCT_GOODS 
        //join TOM_CNTNTS_FILE_CMMN on TOM_CNTNTS_FILE_CMMN.cntnts_no = TCT_GOODS.cntnts_no 
        // where TCT_GOODS.cntnts_no $this->cntnts_no and TOM_CNTNTS_FILE_CMMN != Constant::$DESC_SE_CODE and TOM_CNTNTS_FILE_CMMN.se_code != Constant::$MAIN_SE_CODE
        //order by TOM_CNTNTS_FILE_CMMN.se_code desc
        $model = $model
            ->select('TOM_CNTNTS_FILE_CMMN.orginl_file_nm','TOM_CNTNTS_FILE_CMMN.cntnts_no','TOM_CNTNTS_FILE_CMMN.stre_file_nm','TOM_CNTNTS_FILE_CMMN.file_size','TOM_CNTNTS_FILE_CMMN.file_cours','TOM_CNTNTS_FILE_CMMN.se_code')
            ->join('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TCT_GOODS.cntnts_no', '=', $this->cntnts_no)
            ->where('TOM_CNTNTS_FILE_CMMN.se_code', '!=', Constant::$DESC_SE_CODE)
            ->where('TOM_CNTNTS_FILE_CMMN.se_code', '!=', Constant::$MAIN_SE_CODE)
            ->orderBy('TOM_CNTNTS_FILE_CMMN.se_code', 'desc');
        return $model;
    }
}
