<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Actions\Constant;

class GetTriesCriteria implements CriteriaInterface
{
    public $period;
    public $category;
    public function __construct($period,$category)
    {
        $this->period = $period;
        $this->category = $category;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $period = $this->period;
        $category = $this->category;
        $current_user_id = auth()->id();
        //select 'TCT_GOODS.cntnts_no', 'TCT_GOODS.modl_nombr', 'TCT_GOODS.brnd_nm', 'TCT_GOODS.event_knd_code', 'TCT_GOODS.event_bgnde', 'TCT_GOODS.event_endde', 'TCT_GOODS.dlvy_bgnde', 'TCT_GOODS.dlvy_endde', 'TCT_GOODS.time_color_code', 'TCT_GOODS.event_trgter_co', 'TCT_GOODS.goods_pc', 'TCT_GOODS.slctn_compt_at', 'TCT_GOODS.link_url', 'TCT_GOODS.brnd_code', 'TCT_GOODS.goods_cl_code', 'TCT_GOODS.goods_txt_code', 'TCT_GOODS.goods_txt', 'TCT_GOODS.resource_type', 'TCT_GOODS.is_try_event', 'TCT_GOODS.try_event_type', 'TCT_GOODS.quantity_to_qualify', 'TCT_GOODS.tries_apply as total_apply', 'TCT_GOODS.likes', 'TCT_GOODS.comments', 'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug','TOM_CNTNTS_FILE_CMMN.file_cours', 'TOM_CNTNTS_FILE_CMMN.orginl_file_nm', 'TOM_CNTNTS_FILE_CMMN.stre_file_nm' From TCT_GOODS
        $select = ['TCT_GOODS.cntnts_no',
                    'TCT_GOODS.modl_nombr',
                    'TCT_GOODS.brnd_nm',
                    'TCT_GOODS.event_knd_code',
                    'TCT_GOODS.event_bgnde',
                    'TCT_GOODS.event_endde',
                    'TCT_GOODS.dlvy_bgnde',
                    'TCT_GOODS.dlvy_endde',
                    'TCT_GOODS.time_color_code',
                    'TCT_GOODS.event_trgter_co',
                    'TCT_GOODS.goods_pc',
                    'TCT_GOODS.slctn_compt_at',
                    'TCT_GOODS.link_url',
                    'TCT_GOODS.brnd_code',
                    'TCT_GOODS.goods_cl_code',
                    'TCT_GOODS.goods_txt_code',
                    'TCT_GOODS.goods_code_group',
                    'TCT_GOODS.goods_txt',
                    'TCT_GOODS.resource_type',
                    'TCT_GOODS.is_try_event',
                    'TCT_GOODS.try_event_type',
                    'TCT_GOODS.quantity_to_qualify',
                    'TCT_GOODS.tries_apply as total_apply',
                    'TCT_GOODS.likes',
                    'TCT_GOODS.comments',
                    'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug',
                    'TOM_CNTNTS_FILE_CMMN.file_cours',
                    'TOM_CNTNTS_FILE_CMMN.orginl_file_nm',
                    'TOM_CNTNTS_FILE_CMMN.stre_file_nm',
                    'TCT_GOODS.level_apply'
        ];
        $select[] = \DB::raw(" CASE  WHEN event_bgnde >= '" . $now . "' THEN 0 ELSE tries_apply END AS total_apply");
        // $current_user_id = 'U20190626111347';
        // check user login
        if(!empty($current_user_id)){
            // select is_liked = 1 if user like try or is_liked = null if user not like try
            // SELECT count(user_no) FROM TCT_GOODS_RECM WHERE TCT_GOODS_RECM.cntnts_no = TCT_GOODS.cntnts_no AND TCT_GOODS_RECM.user_no = '".$current_user_id."' GROUP BY TCT_GOODS_RECM.cntnts_no) as is_liked
            $select[] = \DB::raw("(SELECT count(user_no) FROM TCT_GOODS_RECM WHERE TCT_GOODS_RECM.cntnts_no = TCT_GOODS.cntnts_no AND TCT_GOODS_RECM.user_no = '".$current_user_id."' GROUP BY TCT_GOODS_RECM.cntnts_no) as is_liked");
            // select is_joined = 1 if user apply try or is_joined = null if user not apply try
            // SELECT count(*) FROM TCT_DRWT WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' GROUP BY TCT_DRWT.cntnts_no) as is_joined
            $select[] = \DB::raw("(SELECT count(*) FROM TCT_DRWT WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' GROUP BY TCT_DRWT.cntnts_no) as is_joined");
            // select slctn_dt if user wwiner try or slctn_dt = null if user not winner try
            // (SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as slctn_dt
            $select[] = \DB::raw("(SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as slctn_dt");
            // select dlvy_dt if user wwiner try or dlvy_dt = null if user not winner try
            // (SELECT TCT_DRWT.slctn_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as dlvy_dt
            $select[] = \DB::raw("(SELECT TCT_DRWT.dlvy_dt FROM TCT_DRWT JOIN TOM_CNTNTS_WDTB on TCT_DRWT.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no WHERE TCT_DRWT.cntnts_no = TCT_GOODS.cntnts_no AND TCT_DRWT.user_no = '".$current_user_id."' AND TOM_CNTNTS_WDTB.delete_at = 'N' AND TOM_CNTNTS_WDTB.expsr_at = 'Y' ) as dlvy_dt");
        }
        //join TOM_CNTNTS_WDTB on TOM_CNTNTS_WDTB.cntnts_no = TCT_GOODS.cntnts_no
        $model = $model->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no');
        $model = $model->join('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no');
        if(!empty($category)){
            $model = $model->where('TCT_GOODS.goods_code_group', $category);
            $model = $model->join('TSM_CODE_GROUP', 'TSM_CODE_GROUP.code_group', '=', 'TCT_GOODS.goods_code_group');
            $select[] = 'TSM_CODE_GROUP.code_group_eng_nm';
        }
        $model = $model->select($select);
        // where TOM_CNTNTS_WDTB.delete_at = 'N' and TOM_CNTNTS_FILE_CMMN.se_code = MAIN_SE_CODE
        $model = $model->where('TOM_CNTNTS_WDTB.delete_at', 'N')->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')->where('TOM_CNTNTS_FILE_CMMN.se_code', '=', Constant::$MAIN_SE_CODE);
        $now = Carbon::now();
        // get all tries
        if ($period == 0) {
            // order WHEN event_endde < '" . $now . "' THEN 3 WHEN event_bgnde >= '" . $now . "' and event_endde >= '" . $now . "' THEN 2 ELSE 1 END asc
            $model = $model->orderByRaw("CASE
                    WHEN TCT_GOODS.event_endde < '" . $now . "' THEN 3
                    WHEN TCT_GOODS.event_bgnde >= '" . $now . "' and TCT_GOODS.event_endde >= '" . $now . "' THEN 2
                    ELSE 1
                  END asc");
        } 
        // get tries đang diễn ra (in process)
        else if($period == 1) {
            // wwhere event_bgnde < now and event_endde >= now order event_endde asc
            $model = $model->where('TCT_GOODS.event_bgnde', '<', $now)
            ->where('TCT_GOODS.event_endde', '>=', $now)->orderBy('TCT_GOODS.event_endde','asc');
        } 
        // get tries sắp diễn ra(comming soon)
        else if($period == 2) {
            // wwhere event_bgnde >= now and event_endde >= now order event_endde asc
            $model = $model->where('TCT_GOODS.event_bgnde', '>=', $now)
                ->where('TCT_GOODS.event_endde', '>=', $now)->orderBy('TCT_GOODS.event_endde','asc');
        } 
        // get tries đã kết thúc(end)
        else if($period == 3) {
            // wwhere event_bgnde >= now and event_endde >= now order event_endde desc
            $model = $model->where('TCT_GOODS.event_endde', '<', $now)->orderBy('TCT_GOODS.event_endde', 'asc')->groupBy('TCT_GOODS.cntnts_no');
        }

        // order by event_endde asc skip (current page - 1) * 20 limit 20
        $model = $model->offset((\Request::input('page') - 1) * 20)->limit(20);
        // Ignore try items that not belong to any category.
        // join TSM_CODE on TSM_CODE.code = TCT_GOODS.goods_cl_code
        $model = $model->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code');
        return $model;
    }
}
