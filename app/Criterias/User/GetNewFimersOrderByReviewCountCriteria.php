<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetNewFimersCriteria.
 *
 * @package namespace App\Criterias;
 */
class GetNewFimersOrderByReviewCountCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // Select TDM_USER.pic', 'TDM_USER.slug', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as number_of_reviews, update_pic
        // join TCT_REVIEW on TCT_REVIEW.USER_NO = TDM_USER.USER_NO
        // where TDM_USER.drmncy_at = Y and TDM_USER.delete_at = N and TCT_REVIEW.delete_at = N and TCT_REVIEW.expsr_at = Y
        // group by TDM_USER.USER_NO
        // order by hot fimer desc and number_of_reviews desc
        $select = ['TDM_USER.pic', 'TDM_USER.level_number','TDM_USER.slug', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as number_of_reviews")];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS update_pic");
        $model = $model
            ->select($select)
            ->join('TCT_REVIEW', 'TCT_REVIEW.USER_NO', 'TDM_USER.USER_NO')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->groupBy('TDM_USER.USER_NO')
            ->orderBy('TDM_USER.created_at', 'desc')
            ->orderBy('number_of_reviews', 'desc');
        return $model;
    }
}
