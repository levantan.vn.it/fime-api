<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/13/2019
 * Time: 3:00 PM
 */

namespace App\Criterias\User;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetHotFimersOrderByReviewCountCriteria implements CriteriaInterface
{
    /** @var int */
    private $days;

    /** @var string */
    private $searchValue;

    /**
     * GetHotFimersCriteria constructor.
     * @param int $days
     * @param null $searchValue
     */
    public function __construct(int $days = 30, $searchValue = null)
    {
        $this->days = $days;
        $this->searchValue = $searchValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(!in_array($this->days, [7, 30, 90])){
            $query = 'hot_fimer_1year';
        }else{
            $query = 'hot_fimer_' . $this->days . 'days';
        }
        // Select TDM_USER.pic', 'TDM_USER.slug', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as number_of_reviews, update_pic
        // join TCT_REVIEW on TCT_REVIEW.USER_NO = TDM_USER.USER_NO
        // where TDM_USER.drmncy_at = Y and TDM_USER.delete_at = N and TCT_REVIEW.delete_at = N and TCT_REVIEW.expsr_at = Y
        // group by TDM_USER.USER_NO
        // order by hot fimer desc and number_of_reviews desc
        $select = ['TDM_USER.pic','TDM_USER.level_number', 'TDM_USER.slug', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as number_of_reviews")];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS update_pic");
        $model = $model
            ->select($select)
            ->join('TCT_REVIEW', 'TCT_REVIEW.USER_NO', 'TDM_USER.USER_NO')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->groupBy('TDM_USER.USER_NO')
            ->orderBy($query, 'desc')
            ->orderBy('number_of_reviews', 'desc');

        // check search value
        // where name or id or slug
        if ($this->searchValue != null) {
            $model = $model->where(function ($query) {
                $query->where('reg_name', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('id', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('slug', 'LIKE', '%' . $this->searchValue . '%');
            });
        }


        return $model;
    }
}
