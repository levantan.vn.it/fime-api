<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/13/2019
 * Time: 3:00 PM
 */

namespace App\Criterias\User;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetHotFimersCriteria implements CriteriaInterface
{
    /** @var int */
    private $days;

    /** @var string */
    private $searchValue;

    /**
     * GetHotFimersCriteria constructor.
     * @param int $days
     * @param null $searchValue
     */
    public function __construct(int $days = 30, $searchValue = null)
    {
        $this->days = $days;
        $this->searchValue = $searchValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $current_user_id = auth()->id();
        // check hot fimer by year or day
        if(!in_array($this->days, [7, 30, 90])){
            $query = 'hot_fimer_1year';
        }else{
            $query = 'hot_fimer_' . $this->days . 'days';
        }
        //where drmncy_at = 'N' and delete_at = 'N'
        $model = $model->where('drmncy_at', 'N')->where('delete_at', 'N');
        if ($this->searchValue == null) {
            // order by hot fimer
            $model = $model->orderBy($query, 'desc')->orderBy('reg_name', 'asc');
        }
        // select 'id','slug','reg_name','pic','follower as follows'
        $select = ['id','slug','reg_name','pic','follower as follows','level_number','user_no'];
        $select[] = \DB::raw("
                        CASE 
                            WHEN TDM_USER.update_pic is null  THEN TDM_USER.pic
                            WHEN TDM_USER.update_pic = ''  THEN TDM_USER.pic                 
                            ELSE TDM_USER.update_pic
                         END AS update_pic");
        // $current_user_id = 'U20190626111347';
        if(!empty($current_user_id)){
            // select check follow author of current user
            // $select[] = \DB::raw("(SELECT count(TCT_FLLW.user_no) FROM TCT_FLLW WHERE TCT_FLLW.user_no = TCT_REVIEW.user_no AND TDM_USER.delete_at = 'N' AND TDM_USER.drmncy_at = 'N' AND TCT_FLLW.fllwr_user_no = '".$current_user_id."' GROUP BY TCT_REVIEW.user_no) as followed");
            
        }
        $model = $model->select($select);
        // $model = $model->orderBy('id', 'asc')->orderBy($query, 'desc');

        if ($this->searchValue != null) {
            // where id like %searchValue% or where slug like %searchValue% or where reg_name like %searchValue%
            $model = $model->where(function ($query) {
                $query->where('id', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('slug', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('reg_name', 'LIKE', '%' . $this->searchValue . '%');
            });
            // order by near result search
            $model = $model->orderByRaw(
                 "CASE
                    WHEN reg_name LIKE '". $this->searchValue ."' THEN 1
                    WHEN reg_name LIKE '". $this->searchValue ."%' THEN 2
                    WHEN reg_name LIKE '%". $this->searchValue ."' THEN 4
                    WHEN id LIKE '". $this->searchValue ."' THEN 1
                    WHEN id LIKE '". $this->searchValue ."%' THEN 2
                    WHEN id LIKE '%". $this->searchValue ."' THEN 4
                    WHEN slug LIKE '". $this->searchValue ."' THEN 1
                    WHEN slug LIKE '". $this->searchValue ."%' THEN 2
                    WHEN slug LIKE '%". $this->searchValue ."' THEN 4
                    ELSE 3
                  END asc"
            );
        }
        


        return $model;
    }
}
