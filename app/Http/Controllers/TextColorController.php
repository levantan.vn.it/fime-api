<?php


namespace App\Http\Controllers;


use App\Actions\TextColor\CreateAction;
use App\Actions\TextColor\GetAllAction;
use App\Actions\TextColor\UpdateAction;
use App\Http\Requests\CoreRequest;

class TextColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index']]);
    }
    public function index(GetAllAction $action){
        return $this->response($action->run());
    }

    public function update(CoreRequest $request, UpdateAction $action) {
        return $this->response($action->run($request->all()));
    }

    public function store(CoreRequest $request, CreateAction $action) {
        return $this->response($action->run($request->all()));
    }
}
