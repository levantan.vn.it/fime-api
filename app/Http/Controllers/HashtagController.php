<?php

namespace App\Http\Controllers;

use App\Actions\Ad\AddAdAction;
use App\Actions\Ad\DeleteAdAction;
use App\Actions\Ad\GetAdByIDAction;
use App\Actions\Ad\GetAllAdsAction;
use App\Actions\Ad\GetAvailableAdsAction;
use App\Actions\Ad\ToggleAdAction;
use App\Actions\Ad\UpdateAdAction;
use App\Actions\Hashtag\AddHashtagsAction;
use App\Actions\Hashtag\GetHashtagsAction;
use App\Actions\Hashtag\UpdateHashtagsAction;
use App\Actions\Hashtag\GetAllHashtagsAction;
use App\Actions\Hashtag\DeleteHashtagsAction;
use App\Actions\Hashtag\ToggleHashtagsAction;
use App\Http\Requests\Hashtag\AddHashtagRequest;
use App\Http\Requests\Hashtag\UpdateHashtagRequest;
use App\Http\Requests\Hashtag\DeleteHashtagRequest;
use App\Http\Requests\Hashtag\ToggleHashtagRequest;
use App\Http\Requests\Ad\AddAdRequest;
use App\Http\Requests\Ad\DeleteAdRequest;
use App\Http\Requests\Ad\GeAdByIDRequest;
use App\Http\Requests\Ad\ToggleAdRequest;
use App\Http\Requests\Ad\UpdateAdRequest;
use Illuminate\Http\Request;
class HashtagController extends Controller
{
    public function __construct()
    {
    }

    public function getTopHashtags(GetHashtagsAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    public function index(Request $request,GetAllHashtagsAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function store(AddHashtagRequest $request, AddHashtagsAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function update(UpdateHashtagRequest $request, UpdateHashtagsAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function delete(DeleteHashtagRequest $request, DeleteHashtagsAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function toggle(Request $request, ToggleHashtagsAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }
}
