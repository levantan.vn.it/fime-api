<?php

namespace App\Http\Controllers;

use App\Actions\Brand\AddBrandAction;
use App\Actions\Brand\DeleteBrandAction;
use App\Actions\Brand\GetAllAction;
use App\Actions\Brand\GetByAction;
use App\Actions\Brand\UpdateBrandAction;
use App\Actions\Brand\GetReviewBrandAction;
use App\Actions\Brand\GetTipBrandAction;
use App\Http\Requests\Brand\AddBrandRequest;
use App\Http\Requests\Brand\GetByRequest;
use App\Http\Requests\Brand\DeleteBrandRequest;
use App\Http\Requests\Brand\UpdateBrandRequest;
use App\Http\Requests\CoreRequest;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\CodeRepositoryInterface;

use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;


use App\Models\Review;
use App\Models\TryFree;
use App\Models\Tips;
use App\Models\Code;

use Illuminate\Http\Request;

class BrandController extends Controller
{
    protected $userRepository;
    protected $reviewRepository;
    protected $userLikeRepository;
    protected $reviewFileRepository;
    protected $userFollowRepository;

    protected   $try_repository;
    protected   $codeRepository;
    public function __construct(TryRepositoryInterface $try_repository,
                                CodeRepositoryInterface $codeRepository,
                                
                                UserRepositoryInterface $userRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                ReviewLikeRepositoryInterface $userLikeRepository,
                                ReviewFilesRepositoryInterface $reviewFileRepository,
                                UserFollowRepositoryInterface $userFollowRepository)
    {
        //        $this->middleware('jwt.auth', ['except' => ['index']]);
        $this->codeRepository = $codeRepository;
        $this->try_repository = $try_repository;

        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->userLikeRepository = $userLikeRepository;
        $this->reviewFileRepository = $reviewFileRepository;
        $this->userFollowRepository = $userFollowRepository;
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    public function index(Request $request, GetAllAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
    /**
     * @param AddBrandRequest $request
     * @param AddBrandAction $action
     * @return array
     */
    public function store(AddBrandRequest $request, AddBrandAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateBrandRequest $request
     * @param UpdateBrandAction $action
     * @return array
     */
    public function update(UpdateBrandRequest $request, UpdateBrandAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteBrandRequest $request
     * @param DeleteBrandAction $action
     * @return array
     */
    public function delete(DeleteBrandRequest $request, DeleteBrandAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
    public function getReviewsByBrand(CoreRequest $request, GetReviewBrandAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
    public function getTipsByBrand(CoreRequest $request, GetTipBrandAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function getById($id)
    {
        $brand = Code::select('code','code_nm','code_group','file')->where('code',$id)->get();
        return $this->response($brand);
    }
}
