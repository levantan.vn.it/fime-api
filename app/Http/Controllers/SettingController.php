<?php

namespace App\Http\Controllers;

use App\Actions\Setting\GetAllSettingAction;
use App\Actions\Setting\GetByGroupAction;
use App\Actions\Setting\UpdateSettingAction;
use App\Http\Requests\Setting\GetByGroupRequest;
use App\Http\Requests\Setting\UpdateSettingsRequest;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'getByGroup']]);
    }

    /**
     * @param GetAllSettingAction $action
     * @return array
     */
    public function index(GetAllSettingAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    public function getByGroup(GetByGroupRequest $request, GetByGroupAction $action) {
        $data = $action->run($request->group);
        return $this->response($data);
    }

    public function update(UpdateSettingsRequest $request, UpdateSettingAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }
}
