<?php

namespace App\Http\Controllers;

use App\Actions\Comment\CreateCommentAction;
use App\Actions\Comment\DeleteCommentAction;
use App\Actions\Comment\GetCommentByIdAction;
use App\Actions\Comment\GetCommentByObjectIdAction;
use App\Actions\Comment\GetCommentByReviewIdAction;
use App\Actions\Comment\ToggleCommentAction;
use App\Http\Requests\Comment\CreateCommentRequest;
use App\Http\Requests\Comment\DeleteCommentRequest;
use App\Http\Requests\Comment\GetCommentByIDRequest;
use App\Http\Requests\Comment\GetCommentByReviewIdRequest;
use App\Http\Requests\Comment\ToggleCommentRequest;
use App\Models\Comment;
class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => [
            'index', 'getListComments', 'getCommentsOfReview', 'getById','removeComment'
        ]]);
}

    /**
     * @param GetCommentByReviewIdRequest $request
     * @param GetCommentByReviewIdAction $action
     * @return array
     */
    public function getCommentsOfReview(GetCommentByReviewIdRequest $request, GetCommentByReviewIdAction $action) {
        $data = $action->run($request->id, $request->type);
        return $this->response($data);
    }

    /**
     * @param GetCommentByReviewIdRequest $request
     * @param GetCommentByObjectIdAction $action
     * @return array
     */
    public function getListComments(GetCommentByReviewIdRequest $request, GetCommentByObjectIdAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteCommentRequest $request
     * @param DeleteCommentAction $action
     * @return array
     */
    public function delete(DeleteCommentRequest $request, DeleteCommentAction $action) {
        $data = $action->run($request->id, $request->type);
        return $this->response($data);
    }

    /**
     * @param GetCommentByIDRequest $request
     * @param GetCommentByIdAction $action
     * @return array
     */
    public function getById(GetCommentByIDRequest $request, GetCommentByIdAction $action) {
        $data = $action->run($request->id, $request->type);
        return $this->response($data);
    }

    /**
     * @param ToggleCommentRequest $request
     * @param ToggleCommentAction $action
     * @return array
     */
    public function toggle(ToggleCommentRequest $request, ToggleCommentAction $action) {
        $data = $action->run($request->id, $request->type);
        return $this->response($data);
    }

    /**
     * @param CreateCommentRequest $request
     * @param CreateCommentAction $action
     * @return array
     */
    public function store(CreateCommentRequest $request, CreateCommentAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function removeComment(){
        $check = Comment::where('delete_at','Y')->delete();
        dd($check);
    }

}
