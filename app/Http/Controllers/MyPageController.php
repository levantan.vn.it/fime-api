<?php

namespace App\Http\Controllers;

use App\Actions\MyPage\Edit;
use App\Actions\MyPage\Follow;
use App\Actions\MyPage\GetFollowers;
use App\Actions\MyPage\GetFollowings;
use App\Actions\MyPage\GetMyInformation;
use App\Actions\MyPage\GetMyReviews;
use App\Actions\MyPage\GetMyTries;
use App\Actions\MyPage\GetReviewsUserLike;
use App\Actions\MyPage\LinkFacebook;
use App\Http\Requests\CoreRequest;

class MyPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth')->only('edit');
    }

    /**
     * @param CoreRequest $request
     * @param GetMyInformation $action
     * @return array
     */
    public function get(CoreRequest $request, GetMyInformation $action)
    {
        return $this->response($action->run($request));
    }

    public function getReviews(CoreRequest $request, GetMyReviews $action)
    {
        return $this->response($action->run($request));
    }

    public function getLikeReviews(CoreRequest $request, GetReviewsUserLike $action)
    {
        return $this->response($action->run($request));
    }

    public function getFollowers(CoreRequest $request, GetFollowers $action)
    {
        return $this->response($action->run($request));
    }

    public function getFollowings(CoreRequest $request, GetFollowings $action)
    {
        return $this->response($action->run($request));
    }

    public function getTries(CoreRequest $request, GetMyTries $action)
    {
        return $this->response($action->run($request));
    }
    public function follow(CoreRequest $request, Follow $action)
    {
        return $this->response($action->run($request->all()));
    }
    public function edit(CoreRequest $request, Edit $action)
    {
        return $this->response($action->run($request->all()));
    }

    public function linkFacebook(CoreRequest $request, LinkFacebook $action) {
        return $this->response($action->run($request->all()));
    }
}
