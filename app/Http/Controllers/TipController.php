<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:39 PM
 */

namespace App\Http\Controllers;


use App\Actions\Tip\GetAllTipsAction;
use App\Actions\Tip\GetByIDAction;
use App\Actions\Tip\GetTopViewAction;
use App\Actions\Tip\GetByAction;
use App\Actions\Tip\GetByPaginateTipsAction;
use App\Actions\Tip\AddTipAction;
use App\Actions\Tip\ToggleTipAction;
use App\Actions\Tip\DeleteTipAction;
use App\Actions\Tip\UpdateTipAction;
use App\Actions\Tip\GetListTryAction;
use App\Actions\Tip\GetListReviewAction;
use App\Actions\Tip\GetTryByIdAction;
use App\Actions\Tip\GetReviewsTipMapping;
use App\Actions\Tip\GetTryTipMapping;
use App\Http\Requests\CoreRequest;
use Illuminate\Http\Request;
use App\Models\Code;
use File;
class TipController extends Controller
{
    public function getAll(CoreRequest $request,GetAllTipsAction $action)
    {
        return $this->response($action->run($request->all()));
    }

    public function getByID(CoreRequest $coreRequest, GetByIDAction $action)
    {
        return $this->response($action->run($coreRequest->id));
    }

    public function getTopView(CoreRequest $coreRequest, GetTopViewAction $action)
    {
        return $this->response($action->run());
    }

    public function getCategory()
    {
        $categories = Code::where('code_group','=','413')
        ->where('use_at','=','Y')
        ->get();
        return $this->response($categories);
    }

    public function index(CoreRequest $request,GetByPaginateTipsAction $action)
    {
        return $this->response($action->run($request->all()));
    }

    public function getTipById(CoreRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function store(CoreRequest $request, AddTipAction $action)
    {
        $data = $action->run($request->all());

        return $this->response($data);
    }

    public function toggle(CoreRequest $request, ToggleTipAction $action)
    {
        $data = $action->run($request->id, $request->toggle);
        return $this->response($data);
    }

    public function deleteMulti(CoreRequest $request, DeleteTipAction $action)
    {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    public function getTipCategory()
    {
        $categories = Code::where('code_group','=','413')
        ->where('use_at','=','Y')
        ->get();
        return $this->response($categories);
    }

    public function update(CoreRequest $request, UpdateTipAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function getListTries(CoreRequest $request,GetListTryAction $action)
    {
        return $this->response($action->run($request->all()));
    }
    public function GetListReview(CoreRequest $request,GetListReviewAction $action)
    {
        return $this->response($action->run($request->all()));
    }

    public function GetTryById(CoreRequest $request, GetTryByIdAction $action) {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    
    public function GetReviewsTipMapping(CoreRequest $request, GetReviewsTipMapping $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
    public function GetTryTipMapping(CoreRequest $request, GetTryTipMapping $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function uploadTips(Request $request)
    {
        try {

            $now = date('Ymd');
            if ($request->hasFile('video')) {
                $video = $request->file('video');
                $file_name = $this->generateRandomString() . '.' . $video->extension();
                $video->move('/data/videos/tips', $file_name);
                return ['result' => ['name' => $file_name, 'url' => '/data/videos/tips']];
            } else if ($request->input('files')) {
                $files = $request->input('files');
                $images = [];
                foreach ($files as $key => $file) {
                    if (($file['base64'] == '' || $file['type'] == '') && $file['url'] != '') {
                        $path_names = explode('/', $file['url']);
                        $file_name = $path_names[count($path_names) - 1];
                        $file_path = '';
                        for ($i = 3; $i < count($path_names) - 1; $i++) {
                            $file_path = $file_path . '/' .$path_names[$i];
                        }
                        array_push($images, ['name' => $file_name, 'url' => $file_path, 'thumb_name' => $file_thumb_name]);
                    } else {
                        $file_data = $file['base64'];
                        @list($type, $file_data) = explode(';base64,', $file_data);
                        @list(, $extension) = explode('/', $type);
                        $randomName = $this->generateRandomString();
                        if ($extension == 'png') {
                            $extension = 'jpg';
                        }
                        $file_name = $randomName . '.' . $extension;
                        $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                        $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                        \Storage::disk('data')->put('images/tips/' . $file_original_name, base64_decode($file_data));
                        if(File::exists(public_path($file_original_name))){
                            File::delete(public_path($file_original_name));
                        }
                        $uri = 'data://application/octet-stream;base64,'  . $file_data;
                        $new_url = public_path('/data/images/tips/' .$file_original_name);
                        // $size_info = getimagesize($new_url);
                        // $width = $size_info[0];
                        // $heigth = $size_info[1];
                        $image_thumb = \Image::make($file_data)->resize(400, null,function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $image = \Image::make($file_data)->resize(800, null,function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $command = "convert ".$new_url;
                        if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                            $command .= ' -strip ';
                        }else{
                            $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                        }
                        $command .= $file_original_name;
                        exec($command);
                        if(!empty($request->type) && $request->type == 'review'){
                            $image_thumb->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                            $image->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                        }
                        
                        $image_thumb->save((public_path().'/data/images/tips/' . $file_thumb_name));
                        $image->save((public_path().'/data/images/tips/' . $file_name));
                        array_push($images, ['name' => $file_name, 'url' => '/data/images/tips/' , 'thumb_name' => $file_thumb_name]);
                    }
                }
                return ['result' => ['images' => $images]];
            } else if ($request->hasFile('images')) {
                $image = $request->file('images');
                $file_name = $this->generateRandomString() . '.' . $image->extension();
                $image->move('/data/images/tips' , $file_name);
                return ['result' => '/data/images/tips/'. $file_name];
            } else {
                $file_data = $request->input('file');
                @list($type, $file_data) = explode(';base64,', $file_data);
                @list(, $extension) = explode('/', $type);
                $randomName = $this->generateRandomString();
                $file_name = $randomName . '.' . $extension;
                $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                \Storage::disk('data')->put('images/tips/'. $file_original_name,  base64_decode($file_data));
                if(File::exists(public_path($file_original_name))){
                    File::delete(public_path($file_original_name));
                }
                $uri = 'data://application/octet-stream;base64,'  . $file_data;
                $new_url = public_path('/data/images/tips/'.$file_original_name);
                // $size_info = getimagesize($new_url);
                // $width = $size_info[0];
                // $heigth = $size_info[1];

                $image_thumb = \Image::make($file_data)->resize(400, null,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $image = \Image::make($file_data)->resize(800, null,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $command = "convert ".$new_url;
                if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                    $command .= ' -strip ';
                }else{
                    $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                }
                $command .= $file_original_name;
                exec($command);
                if(!empty($request->type) && $request->type == 'review'){
                    $image_thumb->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                    $image->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                }
                $image_thumb->save((public_path().'/data/images/tips/'. $file_thumb_name));
                $image->save((public_path().'/data/images/tips/' . $file_name));
                return ['result' => ['name' => $file_name, 'url' => '/data/images/tips/', 'thumb_name' => 'data/images/tips/' . $file_thumb_name]];
            }

        } catch (\Exception $ex) {
            \Log::error($ex);

            return ['result' => [
                'error' => true,
                'message' => $ex->getMessage()
            ]];
        }catch(Intervention\Image\Exception\NotReadableException $e){
            \Log::error($ex);

            return ['result' => [
                'error' => true,
                'message' => $ex->getMessage()
            ]];
        }
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }
}
