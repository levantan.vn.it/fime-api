<?php

namespace App\Http\Controllers;

use App\Actions\SocialUser\UpdateUserID;
use App\Http\Requests\SocialUser\UpdateUserIDRequest;

class SocialUserController extends Controller
{
    /**
     * @param UpdateUserIDRequest $request
     * @param UpdateUserID $action
     * @return array
     */
    public function updateUserID(UpdateUserIDRequest $request, UpdateUserID $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }
}
