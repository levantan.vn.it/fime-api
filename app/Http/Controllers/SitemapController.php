<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Review;
use App\Models\Tips;
use App\Models\TryFree;
use App\Models\User;
use App\Models\Code;
use Carbon\Carbon;
use App\Actions\Constant;
use File;


class SitemapController extends Controller
{
    public function __construct()
    { }

    public function sitemap()
    {
        $sitemap = $this->index();
        // $tries = $this->tries();
        // $reviews = $this->reviews();
        // $reviews1 = $this->reviews1();
        // $tips = $this->tips();
        // $fimers = $this->fimers();
        // $fimers1 = $this->fimers1();
        // $fimers2 = $this->fimers2();
        // $fimers3 = $this->fimers3();
        // $fimers4 = $this->fimers4();
        $sitemap = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $sitemap);
        // $tries = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $tries);
        // $reviews = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $reviews);
        // $reviews = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $reviews1);
        // $tips = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $tips);
        // $fimers = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $fimers);
        // $fimers1 = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $fimers1);
        // $fimers2 = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $fimers2);
        // $fimers3 = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $fimers3);
        // $fimers4 = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $fimers4);
        File::put('/web_src/browser/sitemap.xml', $sitemap);
        // File::put('/web_src/browser/tries.xml', $tries);
        // File::put('/web_src/browser/tips.xml', $tips);
        // File::put('/web_src/browser/reviews.xml', $reviews);
        // File::put('/web_src/browser/reviews1.xml', $reviews1);
        // File::put('/web_src/browser/fimers.xml', $fimers);
        // File::put('/web_src/browser/fimers1.xml', $fimers1);
        // File::put('/web_src/browser/fimers2.xml', $fimers2);
        // File::put('/web_src/browser/fimers3.xml', $fimers3);
        // File::put('/web_src/browser/fimers4.xml', $fimers4);
    }

    public function index()
    {
        $tries = $this->tries();
        $reviews = $this->reviews();
        // $reviews1 = $this->reviews1();
        $tips = $this->tips();
        $fimers = $this->fimers();
        // $fimers1 = $this->fimers1();
        // $fimers2 = $this->fimers2();
        // $fimers3 = $this->fimers3();
        // $fimers4 = $this->fimers4();
        $categories = $this->categories();

        return view('sitemap.index', [
            'tries' => $tries,
            'reviews' => $reviews,
            // 'reviews1' => $reviews1,
            'tips' => $tips,
            'fimers' => $fimers,
            // 'fimers1' => $fimers1,
            // 'fimers2' => $fimers2,
            // 'fimers3' => $fimers3,
            // 'fimers4' => $fimers4,
            'categories' => $categories,
        ])->render();
    }

    public function tries()
    {
        $tries = TryFree::select('TCT_GOODS.event_bgnde', 'TOM_CNTNTS_WDTB.slug')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->orderBy('TCT_GOODS.event_bgnde', 'desc')
            ->latest()
            ->limit(100)
            ->get();

        // return view('sitemap.tries', [
        //     'tries' => $tries,
        // ])->render();
        return $tries;
    }

    public function reviews()
    {
        $reviews = Review::select('TCT_REVIEW.slug', 'TCT_REVIEW.writng_dt')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc')
            ->limit(100)
            ->get();
        // return view('sitemap.reviews', [
        //     'reviews' => $reviews,
        // ])->render();
        return $reviews;
    }
    // public function reviews1()
    // {
    //     $reviews = Review::select('TCT_REVIEW.slug', 'TCT_REVIEW.writng_dt')
    //         ->where('TCT_REVIEW.delete_at', 'N')
    //         ->where('TCT_REVIEW.expsr_at', 'Y')
    //         ->orderBy('TCT_REVIEW.writng_dt', 'desc')
    //         ->take(30000)->skip(30000)
    //         ->get();
    //     return view('sitemap.reviews1', [
    //         'reviews' => $reviews,
    //     ])->render();
    // }

    public function categories()
    {
        $code = Constant::$CATEGORY_GROUP_CODE;
        $categories = Code::where('code_nm', '!=', "SKIN")->where('code_group', '=', $code)->limit(0)
            ->get();
        return $categories;
    }


    public function tips()
    {
        $tips = Tips::select('ti_id', 'created_at')
            ->where('display_yn', 'Y')
            ->orderBy('created_at', 'desc')
            ->limit(100)
            ->get();
        // return view('sitemap.tips', [
        //     'tips' => $tips,
        // ])->render();
        return $tips;
    }
    public function fimers()
    {
        $fimers = User::select('user_no', 'hot_fimer_1year', 'reg_dt', 'slug')
            ->where('drmncy_at', 'N')
            ->where('delete_at', 'N')
            ->orderBy('hot_fimer_1year', 'desc')
            ->limit(100)
            ->get();
        // return view('sitemap.fimers', [
        //     'fimers' => $fimers,
        // ])->render();
        return $fimers;
    }
    // public function fimers1()
    // {
    //     $fimers = User::select('user_no', 'hot_fimer_1year', 'reg_dt', 'slug')
    //         ->where('drmncy_at', 'N')
    //         ->where('delete_at', 'N')
    //         ->orderBy('hot_fimer_1year', 'desc')
    //         ->take(30000)->skip(30000)
    //         ->get();
    //     return view('sitemap.fimers1', [
    //         'fimers' => $fimers,
    //     ])->render();
    // }
    // public function fimers2()
    // {
    //     $fimers = User::select('user_no', 'hot_fimer_1year', 'reg_dt', 'slug')
    //         ->where('drmncy_at', 'N')
    //         ->where('delete_at', 'N')
    //         ->orderBy('hot_fimer_1year', 'desc')
    //         ->take(30000)->skip(60000)
    //         ->get();
    //     return view('sitemap.fimers2', [
    //         'fimers' => $fimers,
    //     ])->render();
    // }
    // public function fimers3()
    // {
    //     $fimers = User::select('user_no', 'hot_fimer_1year', 'reg_dt', 'slug')
    //         ->where('drmncy_at', 'N')
    //         ->where('delete_at', 'N')
    //         ->orderBy('hot_fimer_1year', 'desc')
    //         ->take(30000)->skip(90000)
    //         ->get();
    //     return view('sitemap.fimers3', [
    //         'fimers' => $fimers,
    //     ])->render();
    // }
    // public function fimers4()
    // {
    //     $fimers = User::select('user_no', 'hot_fimer_1year', 'reg_dt', 'slug')
    //         ->where('drmncy_at', 'N')
    //         ->where('delete_at', 'N')
    //         ->orderBy('hot_fimer_1year', 'desc')
    //         ->take(30000)->skip(120000)
    //         ->get();
    //     return view('sitemap.fimers4', [
    //         'fimers' => $fimers,
    //     ])->render();
    // }
}
