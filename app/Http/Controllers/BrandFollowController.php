<?php

namespace App\Http\Controllers;

use App\Actions\BrandFollow\GetBrandFollowAction;
use App\Actions\BrandFollow\ToggleFollowBrandAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\UserFollow\FollowUserRequest;
use App\Models\BrandFollow;
use Illuminate\Http\Request;
class BrandFollowController extends Controller
{
    /**
     * UserFollowController constructor.
     */
    public function __construct()
    {
        // $this->middleware('jwt.auth', ['except' => ['getTopInteractive']]);
    }

    public function toggle(CoreRequest $request, ToggleFollowBrandAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function getBrandFollow($followed_brand_id) {
        $total = BrandFollow::where('fllwr_br','=',$followed_brand_id)->count();
        return $this->response($total);
    }

    public function checkBrandFollow($followed_brand_id) {
        $data['user_no'] = auth()->id();
        $check = BrandFollow::where('fllwr_br','=',$followed_brand_id)->where('user_no','=',$data['user_no'])->first();
        if(!empty($check))
            return $this->response(true);
        return $this->response(false); 
    }

}
