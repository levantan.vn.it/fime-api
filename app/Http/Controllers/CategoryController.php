<?php

namespace App\Http\Controllers;

use App\Actions\Category\AddCategoryAction;
use App\Actions\Category\DeleteCategoryAction;
use App\Actions\Category\GetAllAction;
use App\Actions\Category\UpdateCategoryAction;

use App\Http\Requests\Category\AddCategoryRequest;
use App\Http\Requests\Category\DeleteCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\Code;
use App\Http\Requests\CoreRequest;


class CategoryController extends Controller
{
    public function __construct()
    {
//        $this->middleware('jwt.auth', ['except' => ['index']]);
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    public function index(Request $request,GetAllAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param AddCategoryRequest $request
     * @param AddCategoryAction $action
     * @return array
     */
    public function store(AddCategoryRequest $request, AddCategoryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param UpdateCategoryAction $action
     * @return array
     */
    public function update(UpdateCategoryRequest $request, UpdateCategoryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteCategoryRequest $request
     * @param DeleteCategoryAction $action
     * @return array
     */
    public function toggle(CoreRequest $request, DeleteCategoryAction $action)
    {
        $data = $action->run($request->id, $request->toggle);
        return $this->response($data);
    }

    public function delete(Request $request, DeleteCategoryAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function getFashion()
    {
        $categories = Code::where('code_group','=','412')
        ->where('code_nm','!=', "SKIN")
        ->where('use_at','=', 'Y')
        ->get();
        return $this->response($categories);
    }

    public function getBeauty()
    {
        $categories = Code::where('code_group','=','407')
        ->where('code_nm','!=', "SKIN")
        ->where('use_at','=', 'Y')
        ->get();
        return $this->response($categories);
    }

}
