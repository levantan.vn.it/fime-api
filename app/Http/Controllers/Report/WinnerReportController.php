<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 11:57 AM
 */

namespace App\Http\Controllers\Report;


use App\Actions\ExcelExport\WinnerAction;
use App\Export\Winner;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoreRequest;
use Carbon\Carbon;

class WinnerReportController extends Controller
{
    public function export(CoreRequest $request, WinnerAction $winnerAction)
    {
        $data = $winnerAction->run($request->all());

        $title = 'Winners';

        $filename = 'Winners_' .Carbon::now()->format('Y-m-d_His'). '.xlsx';
        (new Winner($data['user_tries'], $data['try'], $title))->store('/workspace/export/'.$filename, 'data');
        return $this->response(['path' => '/data/workspace/export/'.$filename]);
    }
}
