<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:00 PM
 */

namespace App\Http\Controllers\Report;


use App\Actions\ExcelExport\TriesAction;
use App\Export\Tries;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoreRequest;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;

class TriesReportController extends Controller
{
    /**
     * @param CoreRequest $request
     * @param TriesAction $triesAction
     * @return array|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(CoreRequest $request, TriesAction $triesAction)
    {
        $data = $triesAction->run($request->all());

        $params = $request->all();

        $title = 'Tries';

        if (isset($params['from']) && $params['from'] != "null" && isset($params['to']) && $params['to'] != "null") {
            $title .= '('. Carbon::parse($params['from'])->format('Y-m-d').'_to_'. Carbon::parse($params['to'])->format('Y-m-d') .')';
        }

        $filename = 'tries_' .Carbon::now()->format('Y-m-d_His'). '.xlsx';
        (new Tries($data, $title))->store('/workspace/export/'.$filename, 'data');
        return $this->response(['path' => '/data/workspace/export/'.$filename]);
    }
}
