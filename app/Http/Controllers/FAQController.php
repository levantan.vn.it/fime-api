<?php

namespace App\Http\Controllers;

use App\Actions\FAQ\AddFAQAction;
use App\Actions\FAQ\DeleteFAQAction;
use App\Actions\FAQ\GetAllFAQAction;
use App\Actions\FAQ\GetAvailableFAQAction;
use App\Actions\FAQ\GetFAQByIdAction;
use App\Actions\FAQ\ToggleFAQAction;
use App\Actions\FAQ\UpdateFAQAction;
use App\Http\Requests\FAQ\AddFAQRequest;
use App\Http\Requests\FAQ\DeleteFAQRequest;
use App\Http\Requests\FAQ\GetFAQByCodeRequest;
use App\Http\Requests\FAQ\GetFAQByIDRequest;
use App\Http\Requests\FAQ\ToggleFAQRequest;
use App\Http\Requests\FAQ\UpdateFAQRequest;

class FAQController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'get', 'available']]);
    }

    /**
     * @param GetAllFAQAction $action
     * @return array
     */
    public function index(GetAllFAQAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param DeleteFAQRequest $request
     * @param DeleteFAQAction $action
     * @return array
     */
    public function delete(DeleteFAQRequest $request, DeleteFAQAction $action)
    {
        $data = $action->run($request->cntnts_nos);
        return $this->response($data);
    }

    /**
     * @param GetFAQByIDRequest $request
     * @param GetFAQByIdAction $action
     * @return array
     */
    public function get(GetFAQByIDRequest $request, GetFAQByIdAction $action)
    {
        $data = $action->run($request->code);
        return $this->response($data);
    }

    /**
     * @param AddFAQRequest $request
     * @param AddFAQAction $action
     * @return array
     */
    public function store(AddFAQRequest $request, AddFAQAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateFAQRequest $request
     * @param UpdateFAQAction $action
     * @return array
     */
    public function update(UpdateFAQRequest $request, UpdateFAQAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param ToggleFAQRequest $request
     * @param ToggleFAQAction $action
     * @return array
     */
    public function toggle(ToggleFAQRequest $request, ToggleFAQAction $action)
    {
        $data = $action->run($request->cntnts_nos, $request->toggle);
        return $this->response($data);
    }

    /**
     * @param GetFAQByCodeRequest $request
     * @param GetAvailableFAQAction $action
     * @return array
     */
    public function available(GetFAQByCodeRequest $request, GetAvailableFAQAction $action)
    {
        $data = $action->run($request->code);
        return $this->response($data);
    }
}
