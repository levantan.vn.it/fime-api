<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Image::configure(array('driver' => public_path('data')));

use File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Jobs\RenderImageJob;
class UploadController extends Controller
{
    private $MAX_UPLOAD_SIZE;

    public function __construct()
    {
        set_time_limit(500);
        // $this->MAX_UPLOAD_SIZE = config('MAX_UPLOAD_SIZE', 20) * 1024 * 1024;
    }

    public function store(Request $request)
    {
        try {

            $now = date('Ymd');
            if($request->hasFile('video_review')){
                $path = $request->file('video_review')->store('review_pc','videos');

                
                $name = str_replace("review_pc/", "", $path);
                $fileUniqid = uniqid() . '_' . time();
                $media = \FFMpeg::fromDisk('videos')->open($path);
                $time = $media->getDurationInSeconds();
                if($time > 60){
                    Storage::delete($path);
                    return ['result' => ['error' => true]];
                }
                $new_path = '/data/videos/review_pc';
                return ['result' => ['name' => $name,'thumb_name'=>'', 'url' => $new_path,'path'=>$path,'fileUniqid'=>$fileUniqid]];
            }elseif ($request->hasFile('video')) {
                $video = $request->file('video');
                $file_name = $this->generateRandomString() . '.' . $video->extension();
                $video->move('/data/videos', $file_name);
                return ['result' => ['name' => $file_name, 'url' => '/data/videos']];
            } else if ($request->input('files')) {
                $files = $request->input('files');
                $images = [];
                foreach ($files as $key => $file) {
                    if (($file['base64'] == '' || $file['type'] == '') && $file['url'] != '') {
                        $path_names = explode('/', $file['url']);
                        $file_name = $path_names[count($path_names) - 1];
                        $file_path = '';
                        for ($i = 3; $i < count($path_names) - 1; $i++) {
                            $file_path = $file_path . '/' .$path_names[$i];
                        }
                        $file_thumb_name = isset($file['thumb_name']) ? $file['thumb_name'] : '';
                        array_push($images, ['name' => $file_name, 'url' => $file_path, 'thumb_name' => $file_thumb_name]);
                    } else {
                        $file_data = $file['base64'];
                        @list($type, $file_data) = explode(';base64,', $file_data);
                        @list(, $extension) = explode('/', $type);
                        $randomName = $this->generateRandomString();
                        $randomName = $randomName.'_up';
                        // if ($extension == 'png') {
                        //     $extension = 'jpg';
                        // }
                        $file_name = $randomName . '.' . $extension;
                        $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                        $file_thumb_name_2 = $randomName . '_THM' . '.' . $extension;
                        $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                        \Storage::disk('data')->put('images/upload/' .$now . '/' . $file_original_name, base64_decode($file_data));
                        
                        $uri = 'data://application/octet-stream;base64,'  . $file_data;
                        $new_url = public_path('/data/images/upload/' .$now . '/' .$file_original_name);
                        // $size_info = getimagesize($new_url);
                        // $width = $size_info[0];
                        // $heigth = $size_info[1];
                        $image_thumb = \Image::make($file_data)->resize(400, null,function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $image_thumb2 = \Image::make($file_data)->resize(100, null,function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $image = \Image::make($file_data)->resize(800, 600,function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $command = "convert ".$new_url;
                        if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                            $command .= ' -strip ';
                        }else{
                            $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                        }
                        $command .= $file_original_name;
                        exec($command);
                        if(!empty($request->type) && $request->type == 'review'){
                            $image_thumb->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                            $image->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                        }
                        $image_thumb->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name));
                        $image_thumb2->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name_2));
                        $image->save((public_path().'/data/images/upload/' .$now . '/' . $file_name));
                        array_push($images, ['name' => $file_name, 'url' => '/data/images/upload/' .$now, 'thumb_name' => $file_thumb_name]);
                    }
                }
                return ['result' => ['images' => $images]];
            } else if ($request->hasFile('images')) {
                $image = $request->file('images');
                $file_name = $this->generateRandomString() . '.' . $image->extension();
                $image->move('/data/images/upload' .$now, $file_name);
                return ['result' => '/data/images/upload' .$now . '/' . $file_name];
            } else {
                $file_data = $request->input('file');
                if(!empty($file_data)){
                    @list($type, $file_data) = explode(';base64,', $file_data);
                    @list(, $extension) = explode('/', $type);
                    $randomName = $this->generateRandomString();
                    $randomName = $randomName.'_up2';
                    $file_name = $randomName . '.' . $extension;

                    $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                    $file_thumb_name_2 = $randomName . '_THM' . '.' . $extension;
                    $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                    \Storage::disk('data')->put('images/upload/' .$now . '/' . $file_original_name,  base64_decode($file_data));
                    
                    $uri = 'data://application/octet-stream;base64,'  . $file_data;
                    $new_url = public_path('/data/images/upload/' .$now . '/' .$file_original_name);
                    // $size_info = getimagesize($new_url);
                    // $width = $size_info[0];
                    // $heigth = $size_info[1];

                    $image_thumb = \Image::make($file_data)->resize(400, null,function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image_thumb2 = \Image::make($file_data)->resize(100, null,function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                    $image = \Image::make($file_data)->resize(800, 600,function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $command = "convert ".$new_url;
                    if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                        $command .= ' -strip ';
                    }else{
                        $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                    }
                    $command .= $file_original_name;
                    exec($command);
                    if(!empty($request->type) && $request->type == 'review'){
                        $image_thumb->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                        $image->insert(public_path('images/water.png'), 'bottom-right', 20, 20);
                    }
                    $image_thumb2->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name_2));
                    $image_thumb->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name));
                    $image->save((public_path().'/data/images/upload/' .$now . '/' . $file_name));
                    if(File::exists(public_path($file_original_name))){
                        File::delete(public_path($file_original_name));
                    }
                    return ['result' => ['name' => $file_name, 'url' => '/data/images/upload/' .$now, 'thumb_name' => 'data/images/upload/' .$now . '/' . $file_thumb_name_2]];
                }else{
                    return ['result' => [
                        'error' => true,
                        'message' => $_FILES
                    ]];
                }
                
            }

        } catch (\Exception $ex) {
            \Log::error($ex);

            return ['result' => [
                'error' => true,
                'message' => $ex->getMessage()
            ]];
        }catch(Intervention\Image\Exception\NotReadableException $e){
            \Log::error($ex);

            return ['result' => [
                'error' => true,
                'message' => $ex->getMessage()
            ]];
        }
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrsuvwxyzABCDEFGHIJKLMNOPQRSUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}
