<?php

namespace App\Http\Controllers;

use App\Actions\Users\Fimers;
use App\Actions\Users\GetUserProfileAction;
use App\Actions\Users\HotFimers;
use App\Actions\Users\GetListHotFimersAction;
use App\Actions\Users\Users;
use App\Actions\Users\GetHotFimersAction;
use App\Actions\Users\ToggleHotFimersAction;
use App\Http\Requests\CoreRequest;
use Illuminate\Http\Request;

class UsersController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('jwt.auth', ['except' => ['getFimers', 'getHotFimers', 'getList']]);
//    }

    public function getUserProfile(GetUserProfileAction $action){
        return $this->response($action->run());
    }

    public function getList(CoreRequest $request, Users $action){
        return $this->response($action->getList($request));
    }

    public function getFimers(CoreRequest $request, Fimers $action){
        $data = $action->run($request->filterType);
        return $this->response($data);
    }

    public function getHotFimers(CoreRequest $request, HotFimers $action){
        $data = $action->run($request->type);
        return $this->response($data);
    }

    public function getListHotFimers(CoreRequest $request,GetListHotFimersAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function GetAllHotFimers(CoreRequest $request,GetHotFimersAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function toggle(Request $request, ToggleHotFimersAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    
}
