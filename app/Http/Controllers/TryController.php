<?php

namespace App\Http\Controllers;

use App\Actions\TryFree\AddTryAction;
use App\Actions\TryFree\DeleteTryAction;
use App\Actions\TryFree\GetAllAction;
use App\Actions\TryFree\GetAvailableTryAction;
use App\Actions\TryFree\GetByAction;
use App\Actions\TryFree\GetByPaginationAction;
use App\Actions\TryFree\GetBySlugAction;
use App\Actions\TryFree\GetBySlugForSSRAction;
use App\Actions\TryFree\GetTriesAction;
use App\Actions\TryFree\GetTriesEndAction;
use App\Actions\TryFree\GetTriesDeliveryAction;
use App\Actions\TryFree\ToggleTryAction;
use App\Actions\TryFree\UpdateTryAction;
use App\Actions\TryFree\GetReviewByTryAction;
use App\Actions\Brand\GetTryBrandAction;
use App\Actions\Brand\GetAllProductAction;

use App\Http\Requests\TryFree\AddTryRequest;
use App\Http\Requests\TryFree\DeleteTryRequest;
use App\Http\Requests\TryFree\GetByPaginationRequest;
use App\Http\Requests\TryFree\GetByRequest;
use App\Http\Requests\TryFree\GetBySlugRequest;
use App\Http\Requests\TryFree\GetTriesRequest;
use App\Http\Requests\TryFree\ToggleTryRequest;
use App\Http\Requests\TryFree\UpdateTryRequest;
use App\Http\Requests\CoreRequest;
use App\Contracts\TipsMappingRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Actions\Constant;
use App\Models\Tips;
use App\Models\TryFree;
use App\Models\User;

class TryController extends Controller
{
    protected $tipsMappingRepository;
    protected $repository;

    public function __construct(
        TryRepositoryInterface $repository,
        TipsMappingRepositoryInterface $tipsMappingRepository
    ) {
        //$this->middleware('jwt.auth', ['except' => ['getAvailableTries', 'getAllTries', 'getBySlug']]);
        $this->tipsMappingRepository = $tipsMappingRepository;
        $this->repository = $repository;
    }

    public function index(GetAllAction $action)
    {
        $data = $action->run();

        return $this->response($data);
    }

    public function getByPagination(GetByPaginationRequest $request, GetByPaginationAction $action)
    {
        $data = $action->run($request->all());

        return $this->response($data);
    }
    public function getTriesEnd(CoreRequest $request, GetTriesEndAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }
    public function getTriesDeliveryAction(CoreRequest $request, GetTriesDeliveryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function store(AddTryRequest $request, AddTryAction $action)
    {
        $data = $action->run($request->all());

        return $this->response($data);
    }

    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/slug?slug={slug}",
     *      operationId="getBySlug",
     *      tags={"Try"},
     *      summary="get try by slug",
     *      description="Returns try",
     *      @SWG\Parameter(
     *          name="slug",
     *          description="Slug",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetBySlugRequest $request
     * @param GetBySlugAction $action
     * @return array
     */
    public function getBySlug(GetBySlugRequest $request, GetBySlugAction $action)
    {
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    public function getBySlugSSR(GetBySlugRequest $request, GetBySlugForSSRAction $action)
    {
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/getAvailableTries",
     *      operationId="getAvailableTries",
     *      tags={"Try"},
     *      summary="get Available Tries",
     *      description="Returns try",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetAvailableTryAction $action
     * @return array
     */
    public function getAvailableTries(GetAvailableTryAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/all?page={page}&period={period}",
     *      operationId="getAllTries",
     *      tags={"Try"},
     *      summary="Get tries",
     *      description="Returns tries",
     *      @SWG\Parameter(
     *          name="period",
     *          description="Period: 0 - All, 1 - Inprogress, 2 - Coming soon , 3 - Done",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="page",
     *          description="Page Index for pagination",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetTriesRequest $request
     * @param GetTriesAction $action
     * @return array
     */
    public function getAllTries(GetTriesRequest $request, GetTriesAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }


    /**
     * @param UpdateTryRequest $request
     * @param UpdateTryAction $action
     * @return array
     */
    public function update(UpdateTryRequest $request, UpdateTryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function toggle(ToggleTryRequest $request, ToggleTryAction $action)
    {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    public function deleteMulti(DeleteTryRequest $request, DeleteTryAction $action)
    {
        $data = $action->run($request->ids);
        return $this->response($data);
    }
   

    public function getTipByTryId($id)
    {
        $tipsMapping = $this->tipsMappingRepository->findByField('content_id', $id)->keyBy('ti_id');
        $tipIds = [];
        $tips = [];
        if (!empty($tipsMapping)) {
            foreach ($tipsMapping as $item) {
                if ($item->content_gb === 'T')
                    array_push($tipIds, $item->ti_id);
            }
            $tips = Tips::select('TCT_TIPS.ti_id',
            'TCT_TIPS.subject',
            'TCT_TIPS.img_url',
            'TCT_TIPS.cover_img1',
            'TCT_TIPS.cover_img2',
            'TCT_TIPS.created_at',
            'TCT_TIPS.updated_at')
            ->whereIn('ti_id', $tipIds)->where('display_yn', 'Y')->get();
        }
        return $this->response($tips);
    }
    public function getAllProducts(CoreRequest $request, GetAllProductAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    } 

    public function GetTriesByBrandId(CoreRequest $request, GetTryBrandAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function getReviewsByTry(CoreRequest $request, GetReviewByTryAction $action){
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    public function getLike() {
        $likes = TryFree::select('*')->limit(1)->get();
        dd($likes);
    }

    public function updateTry(){
        $tries = TryFree::select('cntnts_no','goods_dc','goods_dc_tmp')->where('goods_dc_tmp','')->orWhereNull('goods_dc_tmp')->limit(100)->orderBy('event_endde','desc')->get();
        if(!empty($tries) && count($tries)){
            foreach ($tries as $key) {
                $new_goods_dc = preg_replace("/<img[^>]+\>/i", "", $key->goods_dc);
                $key->goods_dc_tmp = $new_goods_dc;
                $key->save();
            }
        }
        dd('OK');
    }

    public function updateImageTry(){
        $try = TryFree::select('TCT_GOODS.cntnts_no','TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug',
                    'TOM_CNTNTS_FILE_CMMN.file_cours',
                    'TOM_CNTNTS_FILE_CMMN.orginl_file_nm',
                    'TOM_CNTNTS_FILE_CMMN.stre_file_nm')
        ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
        ->join('TOM_CNTNTS_FILE_CMMN', 'TOM_CNTNTS_FILE_CMMN.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
        ->where('TOM_CNTNTS_WDTB.delete_at', 'N')->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')->where('TOM_CNTNTS_FILE_CMMN.se_code', '=', Constant::$MAIN_SE_CODE)
        ->where('TCT_GOODS.cntnts_no',2060)->first();
        $new_url = public_path($try->file_cours. '/' .$try->orginl_file_nm);
        // dd($new_url);
        $command = "convert ".$new_url;
        $command .= ' -strip ';
        // if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
           
        // }else{
        //     $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
        // }
        $command .= $try->orginl_file_nm;
        $check = exec($command);
        dd($check);
        
    }

    public function updatePic(){
        $datas = array(
        );
        return view('checkUpdatePic',$datas);
        $users = User::select('user_no', 'reg_name', 'id', 'pic','slug')
            ->where('delete_at', 'N')
            ->where('drmncy_at', 'N')
            ->where('update_pic',NULL)->where('pic','!=',null)->orderBy('created_at', 'desc')->limit(50)->get();
        if(!empty($users) && count($users)){
            foreach ($users as $user) {
                $file_name =  $user->pic;
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $name = pathinfo($file_name, PATHINFO_FILENAME  );
                $new_url = 'https://api-np.fime.vn'.$file_name;
                try {
                   $command = "convert ".$new_url;
                   if(strtolower($extension) == 'png' || strtolower($extension) == 'gif'){
                       $command .= ' -strip ';
                   }else{
                       $command .= ' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ';
                   }
                   $command .= $file_name;
                   exec($command);
                   
                   $file_thumb_name = $name . '_TMB' . '.' . $extension;
                   $image_thumb = \Image::make($new_url)->resize(150, null,function ($constraint) {
                       $constraint->aspectRatio();
                       $constraint->upsize();
                   });
                   $image_thumb->save((public_path().'/data/images/profile/'.$file_thumb_name));
                   $user->update_pic = '/data/images/profile/'.$file_thumb_name;
                   $user->save(); 
                } catch (\Intervention \ Image \ Exception \ NotReadableException $e) {
                    $user->update_pic = $user->pic;
                    $user->save(); 
                    break;
                }catch (\Intervention \ Image \ Exception \ NotSupportedException $e) {
                    $user->update_pic = $user->pic;
                    $user->save(); 
                    break;
                }
               
            }
        }
        dd($users);
    }
}
