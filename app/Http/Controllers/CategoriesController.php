<?php

namespace App\Http\Controllers;

use App\Actions\Categories\GetAllCategoriesAction;
use App\Actions\Categories\UpdateCategoriesAction;
use App\Actions\Categories\AddCategoriesAction;
use App\Actions\Categories\DeleteCategoriesAction;

use App\Http\Requests\CodeGroup\AddCodeGroupRequest;
use App\Http\Requests\CodeGroup\UpdateCodeGroupRequest;
use App\Http\Requests\Category\DeleteCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\CodeGroup;
class CategoriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('jwt.auth', ['except' => ['index']]);
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    // public function index(Request $request,GetAllAction $action)
    // {
    //     dd(1);
    //     $data = $action->run($request->all());
    //     return $this->response($data);
    // }

    /**
     * @param AddCategoryRequest $request
     * @param AddCategoryAction $action
     * @return array
     */
    public function store(AddCodeGroupRequest $request, AddCategoriesAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param UpdateCategoryAction $action
     * @return array
     */
    public function update(UpdateCodeGroupRequest $request, UpdateCategoriesAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteCategoryRequest $request
     * @param DeleteCategoryAction $action
     * @return array
     */
    public function delete(Request $request, DeleteCategoriesAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function getAllCategories(Request $request,GetAllCategoriesAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }
    public function index()
    {
        $categories = CodeGroup::select('CODE_GROUP','CODE_GROUP_ENG_NM')
        ->where('CODE_GROUP','=','407')
        ->orWhere('CODE_GROUP','>','411')
        ->where('USE_AT','=','Y')
        ->get();
        return $this->response($categories);
    }
}
