<?php

namespace App\Http\Controllers;

use App\Actions\UserLike\GetListUserLiked;
use App\Actions\UserLike\ToggleLikeAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\UserLike\LikeRequest;

class UserLikeController extends Controller
{
    /**
     * UserFollowController constructor.
     */
    public function __construct()
    {
        //$this->middleware('jwt.auth', ['except' => ['index']]);
    }

    /**
     * @param LikeRequest $request
     * @param ToggleLikeAction $action
     * @return array
     */
    public function toggle(LikeRequest $request, ToggleLikeAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function getList(CoreRequest $request, GetListUserLiked $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }
}
