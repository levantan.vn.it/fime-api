<?php

namespace App\Http\Controllers;

use App\Actions\FAQCategory\AddCategoryAction;
use App\Actions\FAQCategory\DeleteCategoryAction;
use App\Actions\FAQCategory\GetAllFAQCategoryAction;
use App\Actions\FAQCategory\GetByAction;
use App\Actions\FAQCategory\UpdateCategoryAction;
use App\Http\Requests\FAQCategory\AddCategoryRequest;
use App\Http\Requests\FAQCategory\DeleteCategoryRequest;
use App\Http\Requests\FAQCategory\GetByRequest;
use App\Http\Requests\FAQCategory\UpdateCategoryRequest;

class FAQCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'getBy']]);
    }
    /**
     * @param GetAllFAQCategoryAction $action
     * @return array
     */
    public function index(GetAllFAQCategoryAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param AddCategoryRequest $request
     * @param AddCategoryAction $action
     * @return array
     */
    public function store(AddCategoryRequest $request, AddCategoryAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param UpdateCategoryAction $action
     * @return array
     */
    public function update(UpdateCategoryRequest $request, UpdateCategoryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteCategoryRequest $request
     * @param DeleteCategoryAction $action
     * @return array
     */
    public function delete(DeleteCategoryRequest $request, DeleteCategoryAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
}
