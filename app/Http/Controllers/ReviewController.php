<?php

namespace App\Http\Controllers;

use App\Actions\Review\CreateReviewAction;
use App\Actions\Review\DeleteReviewAction;
use App\Actions\Review\GetAllReviewAction;
use App\Actions\Review\GetPaginateAfterReviewAction;
use App\Actions\Review\GetPaginateReviewAction;
use App\Actions\Review\GetReviewByIDAction;
use App\Actions\Review\GetReviewByTypeAction;
use App\Actions\Review\GetReviewDetailAction;
use App\Actions\Review\GetReviewDetailsAction;
use App\Actions\Review\TogglePopularReviewAction;
use App\Actions\Review\ToggleReviewAction;
use App\Actions\Review\UpdateReviewAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\Review\CreateReviewRequest;
use App\Http\Requests\Review\DeleteReviewRequest;
use App\Http\Requests\Review\GetReviewByIDRequest;
use App\Http\Requests\Review\GetReviewByTypeRequest;
use App\Http\Requests\Review\GetReviewPagingRequest;
use App\Http\Requests\Review\ToggleReviewRequest;
use App\Http\Requests\Review\UpdateReviewRequest;
use App\Http\Requests\Review\ReviewStatusAction;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Models\Hashtag;
use App\Models\Review;
use Illuminate\Support\Facades\Storage;
class ReviewController extends Controller
{

    /**
     * ReviewController constructor.
     */
    protected $reviewHashTagsRepository;

    public function __construct(ReviewHashTagsRepositoryInterface $reviewHashTagsRepository)
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'get', 'getNew', 'getReviewByType', 'getDetail', 'getByHashtag', 'getNewAfter','getHashtagByReview','getDetails','remove']]);
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
    }

    public function index(CoreRequest $request, GetAllReviewAction $action) {
        $this->updateReview();
        $data = $action->run($request->all());
        return $this->response($data);
    }
    public function updateReview(){
        // return false;
        $reviews = Review::whereNull('slug')->orWhere("slug","")->get();
        if(!empty($reviews)){
            foreach ($reviews as $key) {
               $slug = str_slug($key->goods_nm,'-');
               if(empty($slug)){
                    $slug = str_random(10);
                    $check_slug = Review::where('slug',$slug)->first();
                    if(!empty($check_slug)){
                        while (true) {
                            $slug = str_random(10);
                            $check_slug = Review::where('slug',$slug)->first();
                            if(empty($check_slug)){
                                break;
                            }
                        }
                    }
                       
               }
               $key->slug = $slug;
               $key->save();
            }
        }
        return;

        $reviews = Review::select('*', \DB::raw('count(slug) as total'))->groupBy('slug')->havingRaw("COUNT(slug) > 1")->get();
        if(!empty($reviews)){
            foreach ($reviews as $value) {
                $slug = str_slug($value->goods_nm,'-');
                if(empty($slug)){
                    $slug = str_random(10);
                }
                $check_slug = Review::where('slug',$slug)->first();
                if(!empty($check_slug)){
                    while (true) {
                        $slug = str_random(10);
                        $check_slug = Review::where('slug',$slug)->first();
                        if(empty($check_slug)){
                            break;
                        }
                    }
                    
                }
                $value->slug = $slug;
                $value->save();
            }
        }
    }

    /**
     * @param CreateReviewRequest $request
     * @param CreateReviewAction $action
     * @return array
     */
    public function store(CreateReviewRequest $request, CreateReviewAction $action) {
        
        $data = $action->run($request->all());
        if(!empty($data->review_no)){
            if($request->videoFiles){  
                $seq = 7;
                foreach ($request->videoFiles as $key) {
                    $path = $key->store('review','videos');
                    $fileUniqid = uniqid() . '_' . time();
                    $media = \FFMpeg::fromDisk('videos')->open($path)->addFilter(function ($filters) {
                            $filters->resize(new \FFMpeg\Coordinate\Dimension(640, 360));
                    })->export()->inFormat(new \FFMpeg\Format\Video\X264('libmp3lame', 'libx264'))->toDisk('review')->save($fileUniqid.'.mp4');

                    $file = \FFMpeg::fromDisk('videos')->open($path)->getFrameFromSeconds(5)->export()->toDisk('thumnails')->save($fileUniqid.'.png');
                    Storage::disk('videos')->delete($path);
                    $new_path = '/data/videos/review';
                    try{
                        $results = \DB::table('TCT_REVIEW_FILE')->insert(
                            [
                            'FILE_SE_CODE' => '185002',
                              'FILE_SN'=> $seq,
                              'ORGINL_FILE_NM' => $fileUniqid.'.mp4',
                              'STRE_FILE_NM' => $fileUniqid.'.mp4',
                              'FILE_COURS' => $new_path,
                              'SE_CODE' => '209003',
                              'REGISTER_ESNTL_NO'=> '',
                              'DWLD_CO' =>'0',
                              'THUMB_FILE_NM' => 'thumnails/'.$fileUniqid.'.png',
                              'THUMB_FILE_NM2' => '',
                              'REGIST_DT' => date('Y-m-d'),
                              'REVIEW_NO' => $data->review_no,
                              'TYPE_FILE'   =>  2
                            ]
                        );
                    }catch(\Illuminate\Database\QueryException $e){ 
                    }
                }

                $seq++;
            }
        }
        return $this->response($data);
    }

    /**
     * @param ToggleReviewRequest $request
     * @param ToggleReviewAction $action
     * @return array
     */
    public function toggle(ToggleReviewRequest $request, ToggleReviewAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    public function togglePopular(ToggleReviewRequest $request, TogglePopularReviewAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    /**
     * @param DeleteReviewRequest $request
     * @param DeleteReviewAction $action
     * @return array
     */
    public function delete(DeleteReviewRequest $request, DeleteReviewAction $action) {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    /**
     * @param GetReviewByIDRequest $request
     * @param GetReviewByIDAction $action
     * @return array
     */
    public function get(GetReviewByIDRequest $request, GetReviewByIDAction $action) {
        // $this->updateReview();
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param GetReviewByTypeRequest $request
     * @param GetReviewByTypeAction $action
     * @return array
     */
    public function getReviewByType(GetReviewByTypeRequest $request, GetReviewByTypeAction $action) {
        // $this->updateReview();
        $data = $action->run($request->type);
        return $this->response($data);
    }

    /**
     * @param GetReviewPagingRequest $request
     * @param GetPaginateReviewAction $action
     * @return array
     */
    public function getNew(GetReviewPagingRequest $request, GetPaginateReviewAction $action) {
        // $this->updateReview();
        $data = $action->run($request);
        return $this->response($data);
    }

    public function getNewAfter(GetReviewPagingRequest $request, GetPaginateAfterReviewAction $action) {
        // $this->updateReview();
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetReviewByIDRequest $request
     * @param GetReviewDetailAction $action
     * @return array
     */
    public function getDetail(GetReviewByIDRequest $request, GetReviewDetailAction $action) {
        // $this->updateReview();
        $data = $action->run($request->slug);
        return $this->response($data);
    }
    public function getDetails(GetReviewByIDRequest $request, GetReviewDetailsAction $action) {
        // $this->updateReview();
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    /**
     * @param UpdateReviewRequest $request
     * @param UpdateReviewAction $action
     * @return array
     */
    public function update(UpdateReviewRequest $request, UpdateReviewAction $action) {
        if(!empty($request->videoFiles)){
            foreach ($request->videoFiles as $key) {
                $path = $key->store('tmp','review');

                $media = \FFMpeg::open($path);
                $time = $media->getDurationInSeconds();
                if($time > 60){
                    $files = Storage::disk('review')->files('tmp');
                    if(!empty($files) && count($files)){
                        foreach ($files as $key) {
                            Storage::disk('review')->delete($key);
                        }
                    }
                    return response(false);
                }
            }
        }
        $data = $action->run($request->all());
        if(!empty($data->review_no)){
            if($request->videoFiles){  
                $seq = 7;
                foreach ($request->videoFiles as $key) {
                    $path = $key->store('review','videos');
                    $fileUniqid = uniqid() . '_' . time();
                    $media = \FFMpeg::fromDisk('videos')->open($path)->addFilter(function ($filters) {
                            $filters->resize(new \FFMpeg\Coordinate\Dimension(640, 360));
                    })->export()->inFormat(new \FFMpeg\Format\Video\X264('libmp3lame', 'libx264'))->toDisk('review')->save($fileUniqid.'.mp4');

                    $file = \FFMpeg::fromDisk('videos')->open($path)->getFrameFromSeconds(5)->export()->toDisk('thumnails')->save($fileUniqid.'.png');
                    Storage::disk('videos')->delete($path);
                    $new_path = '/data/videos/review';
                    try{
                        $results = \DB::table('TCT_REVIEW_FILE')->insert(
                            [
                            'FILE_SE_CODE' => '185002',
                              'FILE_SN'=> $seq,
                              'ORGINL_FILE_NM' => $fileUniqid.'.mp4',
                              'STRE_FILE_NM' => $fileUniqid.'.mp4',
                              'FILE_COURS' => $new_path,
                              'SE_CODE' => '209003',
                              'REGISTER_ESNTL_NO'=> '',
                              'DWLD_CO' =>'0',
                              'THUMB_FILE_NM' => 'thumnails/'.$fileUniqid.'.png',
                              'THUMB_FILE_NM2' => '',
                              'REGIST_DT' => date('Y-m-d'),
                              'REVIEW_NO' => $data->review_no,
                              'TYPE_FILE'   =>  2
                            ]
                        );
                    }catch(\Illuminate\Database\QueryException $e){ 
                    }
                }

                $seq++;
            }
        }
        return $this->response($data);
    }
    public function status(CoreRequest $request, ReviewStatusAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function getHashtagByReview($id){
        
        $hashtag = $this->reviewHashTagsRepository->findByField('review_no', $id)->keyBy('hash_seq');
        $hashtag_review_id = [];
        $hashtags_review = [];
        if(!empty($hashtag)){
            foreach ($hashtag as $item) {
                    array_push($hashtag_review_id, $item->hash_seq);
            }
            $hashtags_review = Hashtag::whereIn('hash_seq',$hashtag_review_id)->get();
        }
        return $this->response($hashtags_review);
    }

    public function remove(){
        $check = Review::where('delete_at','Y')->delete();
        dd($check);
    }
}
