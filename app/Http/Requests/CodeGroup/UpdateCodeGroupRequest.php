<?php

namespace App\Http\Requests\CodeGroup;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCodeGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_group_eng_nm' => 'required'
        ];
    }
}
