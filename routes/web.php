<?php
use App\Models\TryFree;
use App\Models\Review;
use App\Models\UserLike;
use App\Models\User;
use App\Models\ReviewComment;
use App\Models\UserTry;
use App\Models\UserFollow;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

Route::get('/', function () {
    // $review_comment = ReviewComment::select('*')->limit(10)->get();
    $review = Review::select('*')
    ->where('TCT_REVIEW.delete_at', 'N')
    ->where('TCT_REVIEW.expsr_at', 'Y')
    ->orderBy('TCT_REVIEW.review_no', 'desc')->limit(10)->get();

    $like_number = UserLike::select('cntnts_no', DB::raw('count(cntnts_no) as number_of_likes'))
    ->groupBy('cntnts_no')->orderBy('cntnts_no', 'desc')->limit(10)->get();
    // dd($like_number);

    $review_comment = ReviewComment::select('review_no', DB::raw('count(review_no) as number_of_comment'))
    ->groupBy('review_no')->orderBy('review_no','desc')->limit(10)->get();
    // dd($review_comment);

    $total_apply = UserTry::select('cntnts_no',DB::raw('count(cntnts_no) as number_of_tries'))
    ->groupBy('cntnts_no')->orderBy('cntnts_no','desc')->limit(10)->get();
    // dd($total_apply);
    
    $follower = UserFollow::select('user_no', DB::raw('count(user_no) as number_of_followers'))
    ->groupBy('user_no')->orderBy('user_no','desc')->limit(10)->get();
    // dd($follower);

    $followings = DB::table('TCT_FLLW')->select('fllwr_user_no',DB::raw('count(fllwr_user_no) as number_of_following'))
        ->groupBy('fllwr_user_no')->limit(10)->get();
        dd($followings);
    return view('welcome');
});



Route::get('add_review_view', function(){
    Schema::table('TCT_REVIEW', function (Blueprint $table) {
        $table->string('likes')->nullable()->default(0);
    });
    dd('OK');
});
Route::get('/share', function () {
    return view('share');
});

Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap.xml/tries', 'SitemapController@tries');
Route::get('/sitemap.xml/reviews', 'SitemapController@reviews');
Route::get('/sitemap.xml/tips', 'SitemapController@tips');