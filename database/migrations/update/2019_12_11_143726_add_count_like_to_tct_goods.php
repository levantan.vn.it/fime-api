<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountLikeToTctGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TCT_GOODS', 'likes')) {
            Schema::table('TCT_GOODS', function (Blueprint $table) {
                $table->integer('likes')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_GOODS', function (Blueprint $table) {
            $table->dropColumn('likes');
        });
    }
}
