<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewsCodeGroupField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TCT_REVIEW', function (Blueprint $table) {
            $table->string('reviews_code_group')->default('407');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_REVIEW', function (Blueprint $table) {
            $table->dropColumn('reviews_code_group');
        });
    }
}
