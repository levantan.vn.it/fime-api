<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTipsCodeGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TCT_TIPS', function (Blueprint $table) {
            $table->string('tips_code_group')->default('413');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_TIPS', function (Blueprint $table) {
            $table->dropColumn('tips_code_group');
        });
    }
}
