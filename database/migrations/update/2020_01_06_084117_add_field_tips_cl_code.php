<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTipsClCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TCT_TIPS', function (Blueprint $table) {
            $table->string('tips_cl_code')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_TIPS', function (Blueprint $table) {
            $table->dropColumn('tips_cl_code');
        });
    }
}
