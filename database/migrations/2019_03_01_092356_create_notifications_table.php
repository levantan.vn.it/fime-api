<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('notifications')) {

            Schema::create('notifications', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title', 200);
                $table->string('content', 500);
                $table->string('url', 500);
                $table->boolean('is_disabled')->default(0);
                $table->boolean('is_system')->default(0);
                $table->string('type')->nullable();
                $table->string('user_id', 255)->default(null);
                $table->integer('object_id');
                $table->string('object_type');
                $table->string('reference_user_id', 255)->nullable();
                $table->string('guid');
                $table->boolean('is_seen')->default(0);
                $table->string('created_by')->nullable();
                $table->string('updated_by')->nullable();
                $table->string('deleted_by')->nullable();
                $table->timestamp('posted_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
