<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TSM_CODE', 'additional_inactive')) {

            Schema::table('TSM_CODE', function (Blueprint $table) {
                $table->string('additional_inactive')->nullable();
                $table->string('additional')->nullable();
                $table->string('slug')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TSM_CODE', function(Blueprint $table) {
            $table->dropColumn('additional_inactive');
        });
    }
}
