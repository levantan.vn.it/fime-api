<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHotFimerForUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TDM_USER', function(Blueprint $table) {
            $table->float('hot_fimer_7days')->change();
            $table->float('hot_fimer_30days')->change();
            $table->float('hot_fimer_90days')->change();
            $table->float('hot_fimer_1year')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TDM_USER', function(Blueprint $table) {
            $table->dropColumn('hot_fimer_7days');
            $table->dropColumn('hot_fimer_30days');
            $table->dropColumn('hot_fimer_90days');
            $table->dropColumn('hot_fimer_1year');
        });
    }
}
