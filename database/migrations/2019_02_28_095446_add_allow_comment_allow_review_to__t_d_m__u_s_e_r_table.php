<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAllowCommentAllowReviewToTDMUSERTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TDM_USER', 'allow_review')) {

            Schema::table('TDM_USER', function(Blueprint $table) {
                $table->tinyInteger('allow_review')->default(1);
                $table->tinyInteger('allow_comment')->default(1);
            });
        }

        if (!Schema::hasColumn('TCT_REVIEW', 'slug')) {

            Schema::table('TCT_REVIEW', function (Blueprint $table) {
                $table->text('slug')->nullable();
                $table->text('review_short')->nullable();
            });
        }

        if (!Schema::hasColumn('TCT_REVIEW_ANWR', 'expsr_at')) {

            Schema::table('TCT_REVIEW_ANWR', function (Blueprint $table) {
                $table->char('expsr_at')->default('Y');
            });
        }

        if (!Schema::hasColumn('TCT_GOODS_ANWR', 'expsr_at')) {

            Schema::table('TCT_GOODS_ANWR', function (Blueprint $table) {
                $table->char('expsr_at')->default('Y');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TDM_USER', function(Blueprint $table) {
            $table->dropColumn('allow_review');
            $table->dropColumn('allow_comment');
        });

        Schema::table('TCT_REVIEW', function(Blueprint $table) {
            $table->dropColumn('slug');
            $table->dropColumn('review_short');
        });
    }
}
