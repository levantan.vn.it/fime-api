<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToTctReviewAnwr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TCT_REVIEW_ANWR', 'id')) {

            Schema::table('TCT_REVIEW_ANWR', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('parent_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_REVIEW_ANWR', function($table) {
            $table->dropColumn('parent_id');
        });
    }
}
