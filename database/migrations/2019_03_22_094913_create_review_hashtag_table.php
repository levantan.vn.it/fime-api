<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewHashtagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('review_hashtag')) {

            Schema::create('review_hashtag', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('review_no');
                $table->integer('hash_seq');
                $table->foreign('hash_seq')
                    ->references('hash_seq')
                    ->on('TCT_POPHASH')
                    ->onDelete('cascade');
                $table->foreign('review_no')
                    ->references('review_no')
                    ->on('TCT_REVIEW')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_hashtag');
    }
}
