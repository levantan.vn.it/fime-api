<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ads')) {

            Schema::create('ads', function (Blueprint $table) {
                $table->increments('id');

                $table->string('name');
                $table->string('description')->nullable();
                $table->string('url');
                $table->string('target_url');
                $table->string('target_type')->default('_blank');
                $table->unsignedInteger('resource_type')->nullable();
                $table->integer('order')->default('0');
                $table->boolean('is_disabled')->nullable();
                $table->boolean('show_on_frontend')->nullable();

                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->unsignedInteger('deleted_by')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
