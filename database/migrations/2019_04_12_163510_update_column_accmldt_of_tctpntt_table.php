<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnAccmldtOfTctpnttTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TCT_PNTT', function (Blueprint $table) {
            $table->float('accml_pntt')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_PNTT', function(Blueprint $table) {
            $table->dropColumn('accml_pntt');
        });
    }
}
