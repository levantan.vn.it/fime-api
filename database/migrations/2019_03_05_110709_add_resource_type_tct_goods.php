<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddResourceTypeTctGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TCT_GOODS', 'resource_type')) {

            Schema::table('TCT_GOODS', function ($table) {
                $table->integer('resource_type')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_GOODS', function($table) {
            $table->dropColumn('resource_type');
        });
    }
}
