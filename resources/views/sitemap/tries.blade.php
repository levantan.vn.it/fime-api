<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($tries as $try)
    <url>
        <loc>http://fime.vn/tries/{{$try->slug}}</loc>
        <lastmod>{{ $try->event_bgnde}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach
</urlset>