<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($tries as $try)
    <url>
        <loc>http://fime.vn/tries/{{$try->slug}}</loc>
        <lastmod>{{ $try->event_bgnde}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach

    @foreach ($reviews as $review)
    <url>
        <loc>http://fime.vn/reviews/detail/{{$review->slug}}</loc>
        <lastmod>{{$review->writng_dt}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach

    @foreach ($tips as $tip)
    <url>
        <loc>http://fime.vn/tips/{{$tip->ti_id}}</loc>
        <lastmod>{{ $tip->created_at}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach

    @foreach ($fimers as $fimer)
        <url>
            <loc>http://fime.vn/usr/{{$fimer->slug}}/</loc>
            <lastmod>{{$fimer->reg_dt}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach
</urlset>