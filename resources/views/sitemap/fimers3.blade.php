<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($fimers as $fimer)
        <url>
            <loc>http://fime.vn/usr/{{$fimer->slug}}/</loc>
            <lastmod>{{$fimer->reg_dt}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach
</urlset>