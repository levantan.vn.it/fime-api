<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($tips as $tip)
    <url>
        <loc>http://fime.vn/tips/{{$tip->ti_id}}</loc>
        <lastmod>{{ $tip->created_at}}</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>
    @endforeach
</urlset>